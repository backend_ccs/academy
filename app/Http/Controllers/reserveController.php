<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use carbon\carbon;
use Excel;
use Illuminate\Support\Facades\Session;

class reserveController extends Controller{
    
    public $message = array();


    public function show_reservePage($id){

        $get_data = \App\Service::where('id' , $id)->get();
        
        $terms = \App\About::where('id' , 1)->value('payment_terms');
        
        return view('reserve_page', ['get_data'=> $get_data , "terms" => $terms]);
    }
    
    public function reserveService(){

        return view('reserve_service');
    }
   
   
    public function reservation($id){

        $get_data = \App\Affiliate_service::where('id' , $id)->get();

        $terms = \App\About::where('id' , 1)->value('payment_terms');

        return view('reservation', ['get_data'=> $get_data , "terms" => $terms]);
    }
    
    
    public function show_affiliate_service(Request $request){
        
        $data = array();
        $affiliate_id = $request->input('affiliate_id');
        
        $get_data= \App\Affiliate_service::where('affiliate_id' ,$affiliate_id)->get();
        
        foreach($get_data as $each){
            array_push($data , (object)array(
                "id" => $each->id,
                "service_name" => $each->service_name,
                "price" => $each->price,
                "link" => "https://qtacourse.com/reservation/".$each->id,
                
                ));
                
        }
        
        
        if(count($get_data) >0 ){
            $message['data'] = $data;
            $message['error'] = 0;
            $message['message'] = "all affiliate services";
        }else{
            $message['data'] = $data;
            $message['error'] = 1;
            $message['message'] = "there is no services";
        }
              return response()->json($message);

    }
    
    public function add_affiliate_service(Request $request){
        
         $created_at = carbon::now()->toDateTimeString();
         $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));
        
        
        $add = new \App\Affiliate_service;
        $add->affiliate_id = $request->input('affiliate_id');
        $add->service_name = $request->input('service_name');
        $add->price = $request->input('price');
        $add->created_at = $dateTime;
        $add->save();
        
        if( $add == true){
            $message['error'] = 0;
            $message['message'] = "a new service is add succesfully";
        }else{
            $message['error'] = 1;
            $message['message'] = "error, please try again";
        }
          return response()->json($message);
        
    }
    
    
    public function update_affiliate_service(Request $request){
               
        $service_id = $request->input('service_id');

        $updateData = \App\Affiliate_service::where('id' , $service_id)->update([
                                                                            "service_name" => $request->input('service_name'),
                                                                            'price' => $request->input('price'),
                                                                            ]);
                                                                            
        if($updateData == true){
            $message['error']  = 0;
            $message['message'] = "updated successfuly";
        }else{
            $message['error'] = 1;
            $message['message'] = "error, please try again";
        }
       return response()->json($message);
    }
    
    
    public function delete_service(Request $request){
        
        $service_id = $request->input('service_id');
        
        $deleted = \App\Affiliate_service::where('id' , $service_id)->delete();
        
        
        if($deleted == true){
            $message['error'] = 0;
            $message['message'] = "service is deleted sucessful";
        }else{
            $message['error'] = 1;
            $message['message'] = "error, please try again";
        }
        
       return response()->json($message);

    }
    
    
    public function show_serviceByID(Request $request){
        
        $service_id = $request->input('service_id');
        
        $get_data =\App\Affiliate_service::where('id' , $service_id)->first();
        
        if($get_data != NULL){
            $message['data'] =$get_data;
            $message['error'] = 0;
            $message['message'] = "service data";
        }else{
            $message['data'] = $get_data;
            $message['error'] = 1;
            $message['message'] = "no data";
        }
        
        return response()->json($message);
    
    }
    
    public function show_reservations(Request $request){
        
        $data = array();
        $affiliate_id = $request->input('affiliate_id');
        
        $get_data = \App\Reserve_page::where('affiliate_id' , $affiliate_id)->get();
        
        foreach($get_data as $each){
            
            array_push($data , (object)array(
                "id" => $each->id,
                "name" =>$each->name,
                "phone" => $each->phone,
                "services" =>$each->services,
                "state" => $each->state,
                "total_price" => $each->total_price,
                "type" => $each->type
                ));
        }
        
          
        if(count($get_data) > 0){
            $message['data'] =$data;
            $message['error'] = 0;
            $message['message'] = "service data";
        }else{
            $message['data'] = $data;
            $message['error'] = 1;
            $message['message'] = "no data";
        }
        
        
        return response()->json($message);

    }
    
    
    public function update_reservationPage(Request $request){
        
        $user_id = $request->input('user_id');
        
        $get_reserve = \App\affiliate::where('id' ,$user_id )->value('reserve_page');
        
        if($get_reserve == 1){
            $reserve = 0;
            $x = "تم ألغاء الصفحه بنجاح";
        }else{
            $reserve = 1;
            $x = "تم تفعيل الصفحه بنجاح";
        }
        
        $update_data = \App\affiliate::where('id' , $user_id)->update(['reserve_page' => $reserve]);
       
        if( $update_data == true){
            echo "<script>alert('.$x.');"
                    . "window.location.replace('/show_affiliators')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                    . "window.location.replace('/show_affiliators')"
                    . "</script>";
        }
    }
    
    
    
    public function show_reservationPage(){
        return view('show_reserve');
    }
    
    public function show_reservationData(){
        
        $get_data = \App\Reserve_page::select('reserve_page.id', 'reserve_page.name', 'reserve_page.phone', 'reserve_page.services', 'reserve_page.affiliate_id', 'affiliate.first_name', 'affiliate.last_name', 'reserve_page.state', 'reserve_page.total_price', 'reserve_page.type', 'reserve_page.created_at')
                                     ->leftjoin('affiliate' , 'affiliate.id' ,'=' ,'reserve_page.affiliate_id')
                                     ->get();
        
        return view('show_reserve' , ['get_data' => $get_data]);
    }
    
}