<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class web_content extends Authenticatable
{
    public $table = 'web_content';
}