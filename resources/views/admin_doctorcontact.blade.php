@extends('layouts.admin_const')

@section('content')



<div id="wrapper">

    

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                       <!--<h2>Jquery Datatable</h2>-->
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item">تواصل معنا</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                             
                            <ul class="nav nav-tabs-new">
                                 <form id="basic-form" method="get"action="{{ route('show_doctorcontact.excel')}}" novalidate enctype="multipart/form-data"  class="text-right">
                                           {{ csrf_field() }}

                                            <div class="form-group " style="float: right; margin-left: 40px;">
                                                <label style="    font-size: x-large; margin-right: -60px;">من </label>
                                                <input type="date" name='fromDate' class="form-control text-right" style="margin-top: -30px;" required>
                                            
                                                <label style="    font-size: x-large; margin-right: -60px;">الى </label>
                                                <input type="date" name='toDate' class="form-control text-right" style="margin-top: -30px;" required>
                                            </div> 
                
                                    <li style="float: right;margin-left: auto;">
                                    <button class="btn btn-outline-success " style="margin-top: 30px;"> Export to Excel</button></li>


                                </form>
                                <li style="float: right;margin-left: auto;"><h2 style="font-size: xx-large;"> : تواصل معنا</h2> </li> &nbsp;&nbsp;

                            </ul>

                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover js-basic-example dataTable table-custom text-center">
                                    
                                       <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>الاسم بالكامل</th>
                                            <th> الهاتف </th>
                                            <th>اسم الخدمة</th>
                                            <th>المسوق</th>
                                            <th>الحالة</th>
                                            <th>التكلفة</th>
                                            <th>كود الخصم</th>
                                            <th>طريقه الدفع</th>
                                            <th>وقت الانشاء</th>
                                            
                                            <!--<th>Edit</th>-->
                                            <th>مسح</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($get_data as $doctor)
                                        <tr>
                                            <td>{{$doctor->id}}</td>
                                            <td>{{$doctor->name}}</td>
                                            <td>{{$doctor->phone}}</td>

                                            <td>{{$doctor->service_id}}</td>
                                             <?php
                                                  $affiliator = \App\affiliate::select('id','first_name','last_name')->where('id',$doctor->affiliate_id)->first();


                                                    $x = $doctor->affiliate_id;
                                                    if( $affiliator != NULL){
                                                        if($x == NULL){
                                                        
                                                        ?><td></td>

                                                  <?php  }else{
                                                     ?>
                                                        <td>
                                                             <form action="/show_affiliatePage" method="GET"  id="sub_{{$affiliator->id}}" enctype="multipart/form-data">
                                   
                                                            {{ csrf_field() }}

                                                            <input type="hidden" name="user_id" value="{{$affiliator->id}}">

                                                            <a  onclick="$('#sub_{{$affiliator->id }}').submit();">

                                                                 <button class="btn  ">
                                                                
                                                           {{$affiliator->first_name}}  {{$affiliator->last_name}}
                                                                </button>
                                                                
                                                                  </a>      
                           
                                                           </form>
                                                        </td>
                                                        
                                                            <?php  } 
                                                            }else{   ?>
                                                                     <td></td>
                                                              <?php } ?>
                                                
                                                    <td>
                                               
                                                        {{$doctor->state}}
                                             
                                                
                                            </td>         
                                               
                                            <td>{{$doctor->total_price}}</td>
                                            <td>{{$doctor->code}}</td>
                                            <td>{{$doctor->type}}</td>
                                            <td>{{$doctor->created_at}}</td>
                                                                                       
                                            <td>
                                               
                                          
                                               <form action="/delete_doctorcontact" method="GET"  id="sub_{{$doctor->id}}" enctype="multipart/form-data">
                                   
                                                   {{ csrf_field() }}
                               
                                                   <input type="hidden" name="doctor_id" value="{{$doctor->id}}">
                                      
                                                   <a  onclick="$('#sub_{{$doctor->id}}').submit();">

                                                    <button class="btn btn-sm btn-icon btn-pure btn-default on-default button-remove"  data-original-title="مسح">
                                                            <i class="icon-trash" aria-hidden="true"></i>
                                                    </button>  
                                                   </a>      
                           
                                               </form>
                                                
                                                
                                            </td> 
                                        </tr>
                                      @endforeach                                    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>            
        </div>       
    </div>
</div>


         



@endsection