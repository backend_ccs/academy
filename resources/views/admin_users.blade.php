@extends('layouts.admin_const')

@section('content')



<div id="wrapper">

    

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                       <!--<h2>Jquery Datatable</h2>-->
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item">المستخدمين</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                             
                            <ul class="nav nav-tabs-new ">
                                <li class="nav-item" style="margin-top: -5px;margin-left: 7px;font-size: x-large;margin-left: auto;"><a class="nav-link"  href="/show_addUser">اضافة مستخدم</a></li>
                                <li style="float: right;margin-left: auto;"><h2 style="font-size: xx-large;">:جميع المستخدمين </h2> </li> &nbsp;&nbsp;
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover js-basic-example dataTable table-custom text-center">
                                    
                                       <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>الاسم الاول</th>
                                            <th>الاسم الثانى </th>
                                            <th>الصورة </th>
                                            <th>البريد الالكترونى</th>
                                            <th>رقم التلفون</th>
                                            <th>كلمه السر</th>
                                            <th>الحالة</th>
                                            <th>وقت الانشاء</th>
                                            
                                            <th>تعديل</th>
                                            <th>مسح</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($get_users as $user)
                                        <tr>
                                            <td>{{$user->id}}</td>
                                            <td>{{$user->first_name}}</td>
                                            <td>{{$user->last_name}}</td>
                                            
                                            <td>
                                              <?php  $image = $user->image;
                                                
                                                if( $image == ''){
                                                
                                                }else{?>
                                                <img src="/uploads/{{$user->image}}" width="80" height="80">
                                                <?php } ?>
                                            </td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->phone}}</td>
                                            <td>{{$user->password}}</td>
                                            <td>{{$user->status}}</td>
                                            <td>{{$user->created_at}}</td>
                                          
                                            <td>
                                           
                                                  <form action="/edit_User" method="get" id='sub_{{$user->id}}'  enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="user_id" value="{{$user->id}}">
                                                    <a onclick="$('#sub_{{$user->id}}').submit();">
                                                        <button class="btn btn-sm btn-icon btn-pure btn-default on-default m-r-5 button-edit" data-toggle="tooltip" data-original-title="Edit">
                                                        <i class="icon-pencil" aria-hidden="true"></i>
                                                        </button>                                      
                                                    </a>
                                                </form>

                                            </td>
                                                  
                                            <td>
                                               
                                          
                                               <form action="/delete_user" method="GET"  id="sub_{{$user->id}}" enctype="multipart/form-data">
                                   
                                                   {{ csrf_field() }}
                               
                                                   <input type="hidden" name="user_id" value="{{$user->id}}">
                                      
                                                   <a  onclick="$('#sub_{{$user->id}}').submit();">

                                                    <button class="btn btn-sm btn-icon btn-pure btn-default on-default button-remove" data-toggle="tooltip" data-original-title="مسح">
                                                            <i class="icon-trash" aria-hidden="true"></i>
                                                    </button>  
                                                   </a>      
                           
                                               </form>
                                                
                                                
                                            </td> 
                                        </tr>
                                      @endforeach                                    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>            
        </div>       
    </div>
</div>

         



@endsection