<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use carbon\carbon;

class imageController extends Controller
{
   
    // Review slider::

    public  function  show_reivewpage(){
          return view("admin_sliderReview");
    }
    
    
    public function review_data(){
        
        $get_data = DB::select("select * from review_image");
        
        return view("admin_sliderReview",['get_data'=>$get_data]);
    }

    
    public function delete_review(Request $request){
        
        $review_id = $request->input('reviews_id');
        
        $deletereview = DB::delete("DELETE from review_image WHERE id=?",[$review_id]);
        
         if( $deletereview == 1){
             echo "<script>alert('تم مسح هذا الريفيوا بنجاح');"
                    . "window.location.replace('/dashboard')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                    . "window.location.replace('/admin_sliderReview')"
                    . "</script>";
        
        }
    }
    
    public function show_addreview(){
        return view('admin_addReview');
    }


    public  function add_review(Request $request){
           
           $created_at = carbon::now()->toDateTimeString();

            $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));


           $image = $request->file('image');

           
           if( $image == ""){
               
                echo "<script>alert('يجب اختيار الصوره اولا ');"
                    . "window.location.replace('/show_addreview')"
                    . "</script>";
           }else{
              
           
                 
                
                 if(isset($image)){
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads';
                    $image->move($destinationPath_id, $savedFileName);
        
                    $images = $savedFileName;
                }else{
                   
                     $images = NULL;
                }
                
                $addreview = DB::insert("insert into review_image (image,created_at) values(?,?)",[$images,$dateTime]);
                
                if( $addreview == 1){
                     echo "<script>alert('تم اضافة اراء العملاء بنجاح');"
                            . "window.location.replace('/dashboard')"
                            . "</script>";
                }else{
                    echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                            . "window.location.replace('/show_addreview')"
                            . "</script>"; 
                }
           }

    }
    // admin slider work::
    
       public  function  show_sliderpage(){
          return view("admin_sliderwork");
    }
    
    
    public function slider_data(){
        
        $get_data = DB::select("select * from service_image");
        
        return view("admin_sliderwork",['get_data'=>$get_data]);
    }

    
    public function deleteslider(Request $request){
        
        $slider_id = $request->input('slider_id');
        
        $delete_slider = DB::delete("delete from service_image where id=?",[$slider_id]);
        
           if(  $delete_slider ){
            echo "<script>alert('تم مسح صوره الخدمة بنجاح');"
                    . "window.location.replace('/dashboard')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                    . "window.location.replace('/show_slider')"
                    . "</script>";
        }
    }
    
    
      public function show_addslider(){
        return view('admin_addslider');
    }


    public  function add_slider(Request $request){
           
         $created_at = carbon::now()->toDateTimeString();

        $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));

        $image = $request->file('image');
        
        
        if( $image == ""){
               
                echo "<script>alert('يجب اختيار الصوره اولا ');"
                    . "window.location.replace('/show_addslider')"
                    . "</script>";
           }else{
                   
                 if(isset($image)){
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads';
                    $image->move($destinationPath_id, $savedFileName);
        
                    $images = $savedFileName;
                }else{
                   
                     $images = NULL;
                }
                
                $addreview = DB::insert("insert into service_image (image,created_at) values(?,?)",[$images,$dateTime]);
                
                if( $addreview == 1){
                     echo "<script>alert('تم اضافة صور الخدمة  بنجاح');"
                            . "window.location.replace('/dashboard')"
                            . "</script>";
                }else{
                    echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                            . "window.location.replace('/show_addslider')"
                            . "</script>"; 
                }

        }
    }
    
    // // web contentwork::
    
       public  function  show_webcontentpage(){
          return view("admin_webcontent");
    }
    
    
    public function webcontent_data(){
        
        $get_data = DB::select("select * from web_content");
        
        return view("admin_webcontent",['get_data'=>$get_data]);
    }
    
    
    
    public function show_editwebcontent(){
         return view('admin_editwebcontent');
    }

    public function edit_webcontentpage(Request $request){
               
        $content_id = $request->input('content_id');
        
        $get_data = DB::select("select * from web_content where id=?",[$content_id]);

        return view('admin_editwebcontent',['get_data'=>$get_data]);
    }
    
    public function update_content(Request $request){
        
        $content_id = $request->input('content_id');
        
        $small_title = $request->input('small_title');
        $big_title = $request->input('big_title');
        $content = $request->input('content');
        
        $socialForm_title = $request->input('socialForm_title');
        $service_title = $request->input('service_title');
        
        
        $update_about= DB::update("update web_content set small_title=?,big_title=? ,content=? ,socialForm_title=?, service_title=?  where id=?",[$small_title, $big_title, $content,$socialForm_title,$service_title,$content_id]);
        
        
        if($update_about == 1){
            echo "<script>alert('تم التعديل بنجاح');"
                    . "window.location.replace('/dashboard')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                     . "window.location.replace('/admin_editwebcontent');"
                    . "</script>";
        }
    }
    
    
    // show videoo::
    
    public function show_video(){
        return view('admin_video');
    }
    
    public function video_data(Request $request){
        
        $get_data = DB::select("select *  from video_link");
        
        return view('admin_video',['get_data'=>$get_data]);
    }
    
    
    
    public function show_editvideo(){
         return view('admin_editvideo');
    }

    public function editvideo_data(Request $request){
               
        $video_id = $request->input('video_id');
        
        $get_videodata = DB::select("select * from video_link where id=?",[$video_id]);

        return view('admin_editvideo',['get_videodata'=>$get_videodata]);
    }
    
    public function update_video(Request $request){
        
        $video_id = $request->input('video_id');
        
        $get_logo = DB::select("select * from video_link where id=?",[$video_id]);
        
        $link = $request->input('link');
        
        
        $logo = $request->file('logo');
        
         if(isset($logo)){
            $new_name = $logo->getClientOriginalName();
            $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads';
            $logo->move($destinationPath_id, $savedFileName);

            $logos = $savedFileName;
        }else{
           
             $logos = $get_logo[0]->logo;
        }
     
        
        $update_videolink= DB::update("update video_link set link=? , logo=? where id=?",[$link,$logos,$video_id]);
        
        
        if($update_videolink == 1){
            echo "<script>alert('تم التعديل بنجاح');"
                    . "window.location.replace('/dashboard')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                     . "window.location.replace('/admin_editvideo');"
                    . "</script>";
        }
    }
    
    
    
    // show sponser::
    
    
    public function show_sponser(){
        return view('admin_sponser');
    }
    
    public function sponser_data(Request $request){
        
        $get_data = DB::select("select *  from sponser");
        
        return view('admin_sponser',['get_data'=>$get_data]);
    }
    
     
    public function deletesponser(Request $request){
        
        $sponser_id = $request->input('sponser_id');
        
        $delete_sponser = DB::delete("delete from sponser where id=?",[$sponser_id]);
        
           if($delete_sponser == 1){
            echo "<script>alert('تم مسح هذا الممول  بنجاح');"
                    . "window.location.replace('/dashboard')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                    . "window.location.replace('/show_sponser')"
                    . "</script>";
        }
    }
    
    
    
      public function show_addsponser(){
        return view('admin_addsponser');
    }


    public  function addsponser(Request $request){
           
        
        $image = $request->file('image');
        
         if(isset($image)){
            $new_name = $image->getClientOriginalName();
            $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads';
            $image->move($destinationPath_id, $savedFileName);

            $images = $savedFileName;
        }else{
           
             $images = NULL;
        }
        
        $addreview = DB::insert("insert into sponser (image) values(?)",[$images]);
        
        if( $addreview == 1){
             echo "<script>alert('تم اضافة صور الخدمة  بنجاح');"
                    . "window.location.replace('/dashboard')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                    . "window.location.replace('/add_sponser')"
                    . "</script>"; 
        }

    }
    
    
    public function add_msg(Request $request){
        $new_msg = $request->input('new_msg');
        
        $add = DB::insert("insert into contact_us (country,email,message) values(?,?,?) ",["","",$new_msg]);
        
        if( $add == 1){
             echo "<script>alert('تم ارسال الرساله بنجاح');"
                    . "window.location.replace('/')"
                    . "</script>";
        }
    }
    
    
    public function show_color(){
        return view('admin_colors');
    }
    
    
    public function get_colors(Request $request){
        
        $get_colors = DB::select("select * from video_link");
        
        return view('admin_colors',[ 'get_colors' => $get_colors]);
    }
    
    public function update_color(Request $request){
        
        $first_color = $request->input('first_color');
        
        $second_color = $request->input('second_color');
        
        $update_color = DB::update("update video_link set first_color=? ,second_color=? where id='1' ",[$first_color,$second_color]);
        
         
        if( $update_color == 1){
             echo "<script>alert('تم تغير اللون بنجاح ');"
                    . "window.location.replace('/show_colorpage')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                    . "window.location.replace('/show_colorpage')"
                    . "</script>"; 
        }
        
    }
    
    
    // socail mediaaa::
    
    
     
    public function show_social(){
        return view('admin_social');
    }
    
    public function social_data(Request $request){
        
        $get_data = DB::select("select *  from social_media");
        
        return view('admin_social',['get_data'=>$get_data]);
    }
    
    
    public function show_editSocail(){
        
        return view('admin_editSocial');
    }
    
    public function SocailData(Request $request){
        
        $social_id = $request->input('social_id');
        
        $Socail_data = DB::select("select * from social_media where id=?",[$social_id]);
        
        return view('admin_editSocial',['Socail_data' => $Socail_data]);
    }
    
    
    public function update_social(Request $request){
        
        $socail_id = $request->input('socail_id');
        
        $facebook = $request->input('facebook');
        $instagram = $request->input('instagram');
        $twitter = $request->input('twitter');
        $youtube = $request->input('youtube');
        $snapchat = $request->input('snapchat');
        $Latitude = $request->input('Latitude');
        $Longitude = $request->input('Longitude');
        
        $updateSocail = DB::update('update social_media set facebook=?, instagram=?, twitter=?, youtube=?, snapchat=?, Latitude=?, Longitude=? where id=?',[$facebook,$instagram,$twitter,$youtube,$snapchat,$Latitude,$Longitude,$socail_id]);
        
        
        if( $updateSocail == 1){
            echo "<script>alert('تم تغير وسائل التواصل  بنجاح ');"
                    . "window.location.replace('/show_social')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                    . "window.location.replace('/edit_social')"
                    . "</script>"; 
        }
        
        
    }
    
    
    
    // dashboard ::
    
    public function show_reviewImages(){
        try{
            
            $get_data = \App\review_image::select('id','image','created_at')->get();
            
            if( count($get_data)>0 ){
                $message['data']  = $get_data;
                $message['error'] = 0;
                $message['message'] = " this is all the review images";
            }else{
                $message['data'] = $get_data;
                $message['error']  = 1;
                $message['message'] = " there is no data, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    public function delete_reviewImage(Request $request){
        try{
            
            $image_id = $request->input('image_id');
            
            $delete = \App\review_image::where('id',$image_id)->delete();
            
            if( $delete == true){
                $message['error']  = 0;
                $message['message'] = "this image is deleted succssfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    public function add_reviewImage(Request $request){
        try{
            
            $updated_at = carbon::now()->toDateTimeString();
		    $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($updated_at)));
           
            $image = $request->file('image');
        
             if(isset($image)){
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                            $destinationPath_id = 'uploads';
                $image->move($destinationPath_id, $savedFileName);
    
                $images = $savedFileName;
            }else{
               
                 $images = NULL;
            }
            
            
            $add = new \App\review_image;
            
            $add->image = $images;
            $add->created_at = $dateTime;
            $add->updated_at = $dateTime;
            $add->save();
            
            
            if($add  == true){
                $message['error'] = 0;
                $message['message'] = "Data is inserted successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    
    
       public function show_ServiceImages(){
        try{
            
            $get_data = \App\service_image::select('id','image','created_at')->get();
            
            if( count($get_data)>0 ){
                $message['data']  = $get_data;
                $message['error'] = 0;
                $message['message'] = " this is all the service images";
            }else{
                $message['data'] = $get_data;
                $message['error']  = 1;
                $message['message'] = " there is no data, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    public function delete_serviceImage(Request $request){
        try{
            
            $image_id = $request->input('image_id');
            
            $delete = \App\service_image::where('id',$image_id)->delete();
            
            if( $delete == true){
                $message['error']  = 0;
                $message['message'] = "this image is deleted succssfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    public function add_serviceImage(Request $request){
        try{
            
            $updated_at = carbon::now()->toDateTimeString();
		    $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($updated_at)));
           
            $image = $request->file('image');
        
             if(isset($image)){
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                            $destinationPath_id = 'uploads';
                $image->move($destinationPath_id, $savedFileName);
    
                $images = $savedFileName;
            }else{
               
                 $images = NULL;
            }
            
            
            $add = new \App\service_image;
            
            $add->image = $images;
            $add->created_at = $dateTime;
            $add->updated_at = $dateTime;
            $add->save();
            
            
            if($add  == true){
                $message['error'] = 0;
                $message['message'] = "Data is inserted successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
}