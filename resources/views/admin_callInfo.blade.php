@extends('layouts.admin_const')

@section('content')



<div id="wrapper">

    

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                       <!--<h2>Jquery Datatable</h2>-->
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item">معلومات التواصل </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                             
                            <ul class="nav nav-tabs-new">
<!--                                <li class="nav-item" style="margin-top: -5px;margin-left: 7px;font-size: x-large;margin-left: auto;"><a class="nav-link"  href="/show_addservice">اضافة خدمة</a></li>-->
                                <li style="float: right;margin-left: auto;"><h2 style="font-size: xx-large;">: معلومات التواصل </li>
                        </ul> </h2> </li> &nbsp;&nbsp;

                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover js-basic-example dataTable table-custom text-center">
                                    
                                       <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>العنوان </th>
                                            <th> رقم التليفون </th>
                                            <th>رقم الواتس</th>
                                            <th> البريدالالكترونى </th>

                                            <th>تعديل</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($get_callInfo as $call)
                                        <tr>
                                            <td>{{$call->id}}</td>
                                            <td>{{$call->address}}</td>
                                            <td>{{$call->phone}}</td>
                                            <td>{{$call->whatsapp}}</td>
                                            <td>{{$call->email}}</td>
                         
                                              
                                            
                                            <td>
                                           
                                                  <form action="/edit_call" method="get" id='sub_{{$call->id}}'  enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="call_id" value="{{$call->id}}">
                                                    <a onclick="$('#sub_{{$call->id}}').submit();">
                                                        <button class="btn btn-sm btn-icon btn-pure btn-default on-default m-r-5 button-edit" data-toggle="tooltip" data-original-title="تعديل">
                                                        <i class="icon-pencil" aria-hidden="true"></i>
                                                        </button>                                      
                                                    </a>
                                                </form>

                                            </td>
                                           
                                        </tr>
                                      @endforeach                                    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>            
        </div>       
    </div>
</div>

         



@endsection