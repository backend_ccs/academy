@extends('layouts.admin_const')

@section('content')

<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12" style="float: right;margin-left: auto;">
                        <h2 style="font-size: xx-large;"> :تعديل  الوان الموقع </h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item"> الوان الموقع</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            
            
             <div class="body">
                               
                    <label style="float: right; margin-left: 15px;font-size: 22px;">:اختار لونك المفضل </label>
                    <input type="color" value="" id="favcolor" style="float: right; "><br><br>
                      
                      
                     <label style="float: right; margin-left: 15px;font-size: 22px;">:من فضلك اضف هذا الون الى الخانة </label>&nbsp; &nbsp;&nbsp;
                     <h4 id="hex" style="float: right;"></h4>

                 <br><br><br>
                 
                 
                 
                 <form id="basic-form" method="post"action="/update_color" novalidate enctype="multipart/form-data" class="text-right">
                                           {{ csrf_field() }}

                        @foreach($get_colors as $color)
                        
                    <!--    <input type="hidden" name="content_id" value="{{$color->id}}">-->

                        <div class="form-group">
                            <label style="    font-size: x-large;"> اللون الاول  </label>
                            <input type="text" name='first_color' value="{{$color->first_color}}" class="form-control text-right" required>
                        </div>
                        
                       
                        
                        <div class="form-group">
                            <label style="    font-size: x-large;"> اللون الثانى</label>
                            <input type="text" name='second_color' value="{{$color->second_color}}" class="form-control text-right" required>
                        </div>
                        
                       
                          
                        <br>    
                        <button type="submit" name="edit_color"class="btn btn-primary text-center" style="margin-right: 556px;font-size: 22px;">حفظ</button>
                          @endforeach
                    </form>
             </div>
            
            
        </div>
</div>

  <script>
                
                
                var theInput = document.getElementById("favcolor");
                var theColor = theInput.value;
                theInput.addEventListener("input", function() {
                
                document.getElementById("hex").innerHTML = theInput.value;
                }, false);
            
            </script>

@endsection