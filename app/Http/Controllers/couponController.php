<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use carbon\Carbon;

class couponController extends Controller
{

    public $message = array();

    public function show_addCoupon(){
        return view('add_coupon');
    }

    public function addCoupons(Request $request){

        $created_at = carbon::now()->toDateTimeString();
        $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));
        
        if( $request->has('service_id') && $request->has('affiliate_id') && $request->has('code') && $request->has('value')){
            
            $add = new \App\AffiliateCoupon;
            $add->service_id = $request->service_id;
            $add->affiliate_id = $request->affiliate_id;
            $add->code = $request->code;
            $add->value = $request->value;
            $add->created_at = $dateTime;
            $add->save();

            if( $add == true){
                echo "<script>alert('تمت اضافه الكوبون بنجاح ');"
                            . "window.location.replace('/home')"
                            . "</script>";
            }
        }else{
            echo "<script>alert('منفضلك ادخل جميع البيانات');"
                            . "window.location.replace('/show_affiliators')"
                            . "</script>";

        }
    }

    public function view_affiliateCoupon(Request $request){
        return view('show_affiliateCoupon');
    }

    public function show_all_affiliateCoupons(Request $request){
        
        $get_data = array();

        $get_data = \App\AffiliateCoupon::select('affiliate_coupon.id as coupon_id' , 'affiliate_coupon.service_id', 'services.name', 'affiliate_coupon.code', 'affiliate_coupon.value', 'affiliate_coupon.created_at')
                                        ->leftJOIN('services' , 'affiliate_coupon.service_id' ,'=', 'services.id')
                                        ->where('affiliate_id' , $request->affiliate_id)->get();

        foreach($data as $each){

            if( $each->service_id == 0){
                $service = "جميع الدورات";
            }else{
                $service = $each->name;
            }

            array_push($get_data , (object)array(
                    "coupon_id" => $each->coupon_id,
                    "service_id" =>$each->service_id,
                    "name" => $service,
                    "code" => $each->code,
                    "value" => $each->value,
                    "created_at" => $each->date,
                ));
        }

        return view('show_affiliateCoupon', array('get_data'=> $get_data ));
    }


    public function delete_coupon(Request $request){
        $coupon_id = $request->coupon_id;

        $deleteData = \App\AffiliateCoupon::where('id' , $coupon_id)->delete();

        if( $deleteData == true){
            echo "<script>alert('تمت مسح الكوبون بنجاح ');"
            . "window.location.replace('/home')"
            . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ حاول مره اخرى ');"
            . "window.location.replace('/show_affiliators')"
            . "</script>";
        }
    }


    public function show_affiliate_coupons(Request $request){

        $data = \App\AffiliateCoupon::select('affiliate_coupon.id', 'affiliate_coupon.service_id', 'services.name as service_name', 'affiliate_coupon.code', 'affiliate_coupon.value', 'affiliate_coupon.created_at  as date')
                                    ->JOIN('services' , 'affiliate_coupon.service_id' ,'=' ,'services.id')
                                    ->where('affiliate_coupon.affiliate_id', $request->affiliate_id)->get();

        if( count($data) >0){
            $message['data'] = $data;
            $message['error'] = 0;
            $message['message'] = "all affiliate coupons";
        }else{
            $message['data'] = $data;
            $message['error'] = 1;
            $message['message'] = "No coupons for that affiliate";
        }

        return response()->json($message);

    }


    /******************show llgeneral coupons ************************ */

    public function show_allcoupons(){
        return view('all_coupons');
    }

    public function show_general_Coupons(Request $request){
        
        $get_data = array();

        $data = \App\AffiliateCoupon::select('affiliate_coupon.id as coupon_id' , 'affiliate_coupon.service_id', 'services.name', 'affiliate_coupon.code', 'affiliate_coupon.value', 'affiliate_coupon.created_at as date')
                                        ->leftJOIN('services' , 'affiliate_coupon.service_id' ,'=', 'services.id')
                                        ->where('affiliate_id' , 0)->get();

        foreach($data as $each){

            if( $each->service_id == 0){
                $service = "جميع الدورات";
            }else{
                $service = $each->name;
            }
            array_push($get_data , (object)array(
                    "coupon_id" => $each->coupon_id,
                    "service_id" =>$each->service_id,
                    "name" => $service,
                    "code" => $each->code,
                    "value" => $each->value,
                    "created_at" => $each->date,
                ));
        }

        return view('all_coupons', array('get_data'=> $get_data ));
    }
}
