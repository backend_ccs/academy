@extends('layouts.admin_const')

@section('content')



<div id="wrapper">


    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                       <!--<h2>Jquery Datatable</h2>-->
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item">  تقارير الزيارات </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                             
                            <ul class="nav nav-tabs-new">
                                 <form id="basic-form" method="get"action="{{ route('affiliate_visits.excel')}}" novalidate enctype="multipart/form-data"  class="text-right">
                                           {{ csrf_field() }}

                                            <div class="form-group " style="float: right; margin-left: 40px;">
                                                <label style="    font-size: x-large; margin-right: -60px;">من </label>
                                                <input type="date" name='fromDate' class="form-control text-right" style="margin-top: -30px;" required>
                                            
                                                <label style="    font-size: x-large; margin-right: -60px;">الى </label>
                                                <input type="date" name='toDate' class="form-control text-right" style="margin-top: -30px;" required>
                                            </div> 
                
                                    <li style="float: right;margin-left: auto;">
                                    <button class="btn btn-outline-success " style="margin-top: 30px;"> Export to Excel</button></li>


                                </form>
                                <li style="float: right;margin-left: auto;"><h2 style="font-size: xx-large;">: تقارير الزيارات التى تمت من خلال هذا المسوق</h2> </li> &nbsp;&nbsp;
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover js-basic-example dataTable table-custom text-center">
                                    
                                       <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th> عدد الزيارات</th>
                                            <th>التاريخ </th>
                                          
                                            
                                            <!--<th>Edit</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($get_data as $visit)
                                        <tr>
                                            <td>{{$visit->id}}</td>
                                            <td>{{$visit->number}}</td>
                                            <td>{{$visit->date}}</td>
                                             
                                        </tr>
                                      @endforeach                                    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>            
        </div>       
    </div>
</div>

         



@endsection