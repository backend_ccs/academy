<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class doctor_contact extends Authenticatable
{
    public $table = 'doctor_contact';
}