<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use carbon\carbon;
use App\About;
use App\contact_us;
use App\User;
use App\Service;
use App\stage;


class AdminCOntroller extends Controller
{
    
    public $message=array();
    
    
    //admin
    
    
     public function admin_login(Request $request)
     {
     try{
         $email=$request->input('email');
         $pass=$request->input('password');
              $updated_at = carbon::now()->toDateTimeString();
              $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($updated_at)));
         
        
      $userexist=User::select('users.id','users.first_name','users.last_name','users.email','users.password','users.phone','users.status','users.image')
              ->where([['users.email',$email],['users.password',$pass],['users.status','=','admin']])->first();
              
              
              
            
		                  
		          
              
              
               $userexist=User::select('users.id','users.first_name','users.last_name','users.email','users.password','users.phone','users.status','users.image')
              ->where([['users.email',$email],['users.password',$pass],['users.status','=','admin']])->first();
              

              if($userexist !=null){
               
                   $message['data']=$userexist;
                   $message['error']=0;
                   $message['message']='login success';

             }else{
                    $message['data']=$userexist;
                    $message['error']=1;
                     $message['message']='data is wrong';
             }
            
            }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
            }
      
            return response()->json($message);
     }
     
         public function show_adminById(Request $request)
    {
       try{
           $id=$request->input('user_id');
          
          
        
        
        

         $user_data=User::select('users.id','users.first_name','users.last_name','users.email','users.password','users.phone','users.status','users.image')
             
            ->where('id',$id)->first();
          

                  if($user_data !=null){
        
                    $message['data']=$user_data;
                    $message['error']=0;
                    $message['message']='user data';
        
                  }else{
                    $message['data']=$user_data;
                    $message['error']=1;
                    $message['message']='no data for user ';
                  }
       

       }catch(Exception $ex){
         
            $message['error']=2;
            $message['message']='error'.$ex->getMessage();

       }
       return response()->json($message);
    }
    
          public function edit_adminprofile(Request $request)
     {
        try{
            
           $id=$request->input('user_id');
		       
		        $fname=$request->input('first_name');
		         $lname=$request->input('last_name');
		        $phone=$request->input('phone');
		         $image=$request->file('image');
		        $email=$request->input('email');
		        $pass=$request->input('password');
		       
		      

		          $updated_at = carbon::now()->toDateTimeString();
		          $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($updated_at)));
		             if(isset($image)) {
		                    $new_name = $image->getClientOriginalName();
		                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
		                    $destinationPath_id = 'uploads/users';
		                    $image->move($destinationPath_id, $savedFileName);
		        
		                    $images = $savedFileName;
		                  
		           }else{
		              $images = User::where('id',$id)->value('image');       
		          }
		        
		 
		            $userupdate=User::where('id',$id)
		              ->update(
		               ['first_name'=>$fname,
		               'last_name'=>$lname,
		                'email'=>$email,
		                 'image'=>$images,
		                'phone'=>$phone,
		                'password'=>$pass,
		                'updated_at'=>$dateTime]);

		              

		        $user_data=User::select('users.id','users.first_name','users.last_name','users.email','users.password','users.phone','users.status','users.image')
              ->where('users.id','=',$id)->first();

		                  if($userupdate==true){
		                        $message['data']=$user_data;
		                         $message['error']=0;
		                        $message['message']='update data successfully';
		                    }else{
		                         $message['data']=$user_data;
		                         $message['error']=1;
		                         $message['message']='error in update data';
		                    }
	       
			            }catch(Exception $ex){
			                 $message['error']=2;
			                 $message['message']='error'.$ex->getMessage();
			            }  

			         return response()->json($message);
     }

    
    
    //users
    
        public function show_allusers( ){
        
        try{
        
       $user=User::select('id', 'first_name', 'last_name', 'image', 'email', 'phone', 'password', 'status', 'created_at')->get();
        
        if( count($user)>0){
             $message['data']=$user;
                      $message['error']=0;
                       $message['message']='show users success';
                
        }else{
             $message['data']=$user;
             $message['error']=0;
             $message['message']='no users';
                
            
        }
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    }
    
         public function show_userByid( Request $request){
        
        try{
        $id=$request->input('user_id');
        
       $user=User::select('id', 'first_name', 'last_name', 'image', 'email', 'phone', 'password', 'status', 'created_at')
       ->where('id',$id)->first();
        
        if( $user!=null){
             $message['data']=$user;
                      $message['error']=0;
                       $message['message']='show users success';
                
        }else{
             $message['data']=$user;
             $message['error']=0;
             $message['message']='no users';
                
            
        }
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    }
    
      public function delete_user( Request $request){
        
        try{
        $id=$request->input('user_id');
        
       $user=User::where('id',$id)->delete();
        
        if( $user ==true){
            // $message['data']=$user;
                      $message['error']=0;
                       $message['message']='delete success';
                
        }else{
           //  $message['data']=$user;
             $message['error']=0;
             $message['message']='error in delete';
                
            
        }
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    }
    
      public function add_user( Request $request){
        
        try{
            
       $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $image = $request->file('image');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $pass = $request->input('password');

        $updated_at = carbon::now()->toDateTimeString();
		          $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($updated_at)));
        
        
            if(isset($image)) {
		                    $new_name = $image->getClientOriginalName();
		                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
		                    $destinationPath_id = 'uploads/users';
		                    $image->move($destinationPath_id, $savedFileName);
		        
		                    $images = $savedFileName;
		                  
		           }else{
		              $images =NULL;       
		          }
		          
		          
		          $insert=new User;
		          $insert->first_name=$first_name;
		           $insert->last_name=$last_name;
		            $insert->image=$images;
		             $insert->email=$email;
		              $insert->phone=$phone;
		               $insert->password=$pass;
		                $insert->status= "admin";
		                 $insert->created_at=$dateTime;
		                  $insert->save();
		        
        
       $user=User::select('id', 'first_name', 'last_name', 'image', 'email', 'phone', 'password', 'status', 'created_at')
       ->where('id',$insert->id)->first();
        
        if( $user ==true){
             $message['data']=$user;
                      $message['error']=0;
                       $message['message']='insert success';
                
        }else{
             $message['data']=$user;
             $message['error']=0;
             $message['message']='error in insert';
                
            
        }
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    }
    
    
    
       public function update_user( Request $request){
        
        try{
           $id= $request->input('user_id'); 
       $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $image = $request->file('image');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $pass = $request->input('password');

                  $updated_at = carbon::now()->toDateTimeString();
		          $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($updated_at)));
        
        
            if(isset($image)) {
		                    $new_name = $image->getClientOriginalName();
		                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
		                    $destinationPath_id = 'uploads/users';
		                    $image->move($destinationPath_id, $savedFileName);
		        
		                    $images = $savedFileName;
		                  
		           }else{
		              $images =User::where('id',$id)->value('image');       
		          }
		          
		          
		          $insert=User::where('id',$id)->update([
		              'first_name'=>$first_name,
		             'last_name'=>$last_name,
		             'image'=>$images,
		              'email'=>$email,
		               'phone'=>$phone,
		                'password'=>$pass,
		                 'updated_at'=>$dateTime
		              
		          
		              ]);
		        
		        
        
       $user=User::select('id', 'first_name', 'last_name', 'image', 'email', 'phone', 'password as passwords', 'status', 'created_at')
       ->where('id',$id)->first();
        
        if( $user ==true){
             $message['data']=$user;
                      $message['error']=0;
                       $message['message']='update success';
                
        }else{
             $message['data']=$user;
             $message['error']=0;
             $message['message']='error in update';
                
            
        }
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    }
    
    
    
    
    //about
      public function show_about( ){
        
        try{
        
       $user=About::select('id', 'content', 'created_at')->first();
        
        if( $user !=null){
             $message['data']=$user;
                      $message['error']=0;
                       $message['message']='show  success';
                
        }else{
             $message['data']=$user;
             $message['error']=0;
             $message['message']='no data';
                
            
        }
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    }
    
     public function update_about(Request $request ){
        
        try{
            $content=$request->input('content');
               
            $updated_at = carbon::now()->toDateTimeString();
		    $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($updated_at)));
            
        
       
           $update=About::where('id',1)->update([
               'content'=>$content,
               'updated_at'=>$dateTime
    
               ]);
          
           $user=About::select('id', 'content', 'created_at')->where('id',1)->first();
        
        if( $update ==true){
             $message['data']=$user;
                      $message['error']=0;
                       $message['message']='update  success';
                
        }else{
             $message['data']=$user;
             $message['error']=0;
             $message['message']='error in update';
                
            
        }
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    }
    
    
    //contact us
       public function show_contact( ){
        
        try{
        
       $user=contact_us::select('id', 'country','email', 'message', 'created_at' )->get();
        
        if( count($user)>0){
             $message['data']=$user;
             $message['error']=0;
             $message['message']='show  success';
                
        }else{
             $message['data']=$user;
             $message['error']=0;
             $message['message']='no data';
                
            
        }
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    }
    
     public function delete_contact(Request $request ){
        
        try{
            $id=$request->input('contact_id');
        
       $user=contact_us::where('id',$id)->delete();
        
        if( $user==true){
             
                      $message['error']=0;
                       $message['message']='delete  success';
                
        }else{
             
             $message['error']=0;
             $message['message']='error in delete';
                
            
        }
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    } 
    
    ///stages
    
    
    //id`, `name`, `from_num`, `to_num`, `created_at`, `updated_
    
    
    
      public function show_stages( ){
        
        try{
        
       $user=stage::select('id', 'name','from_num', 'to_num', 'created_at','updated_at' )->get();
        
        if( count($user)>0){
             $message['data']=$user;
                      $message['error']=0;
                       $message['message']='show  success';
                
        }else{
             $message['data']=$user;
             $message['error']=0;
             $message['message']='no data';
                
            
        }
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    }
      public function show_stagesbyid(Request $request ){
        
        try{
        
        $id=$request->input('stage_id');
       $user=stage::select('id', 'name','from_num', 'to_num', 'created_at','updated_at' )
       ->where('id',$id)
       ->first();
        
        if( $user !=null){
             $message['data']=$user;
                      $message['error']=0;
                       $message['message']='show  success';
                
        }else{
             $message['data']=$user;
             $message['error']=0;
             $message['message']='no data';
                
            
        }
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    }
    
          public function update_stages(Request $request ){
        
        try{
             $id=$request->input('stage_id');
            $name=$request->input('name');
            $from=$request->input('from_number');
            $to=$request->input('to_number');
            
                           
        $updated_at = carbon::now()->toDateTimeString();
		          $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($updated_at)));
            
        
       $user=stage::where('id',$id)->update([
           
            'name'=>$name,
            'from_num'=>$from,
            'to_num'=>$to,
            'updated_at'=>$dateTime
           ]);
           
           $select=stage::select('id', 'name','from_num', 'to_num', 'created_at','updated_at' )->where('id',$id)->first();
        
        if($user == true){
             $message['data']=$select;
                      $message['error']=0;
                       $message['message']='update  success';
                
        }else{
             $message['data']=$select;
             $message['error']=0;
             $message['message']='error in update';
                
            
        }
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    }
    
    
    //services
        public function show_services( ){
        
        try{
        
       $user=Service::select('id', 'name', 'description', 'price', 'level1','level2', 'level3', 'created_at','updated_at' )->get();
        
        if( count($user)>0){
             $message['data']=$user;
                      $message['error']=0;
                       $message['message']='show  success';
                
        }else{
             $message['data']=$user;
             $message['error']=0;
             $message['message']='no data';
                
            
        }
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    }
    
       public function show_servicebyid(Request $request){
        
        try{
            $id=$request->input('service_id');
        
       $user=Service::select('id', 'name', 'description', 'price', 'level1','level2', 'level3', 'created_at','updated_at' )
                   ->where('id',$id)
                   ->first();
        
        if( $user !=null){
             $message['data']=$user;
                      $message['error']=0;
                       $message['message']='show  success';
                
        }else{
             $message['data']=$user;
             $message['error']=0;
             $message['message']='no data';
                
            
        }
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    }
           public function delete_service(Request $request){
        
        try{
            $id=$request->input('service_id');
        
       $user=Service::where('id',$id)
       ->delete();
        
        if( $user ==true){
          
                      $message['error']=0;
                       $message['message']='delete  success';
                
        }else{
          
             $message['error']=0;
             $message['message']='error in delete';
                
            
        }
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    }
    
    
    
    
    
    
    public function update_service(Request $request){
        try{
            
            $service_id = $request->input('service_id');
           
            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($updated_at)));
        
            
            $update = \App\Service::where('id',$service_id)
                                  ->update([
                                      "name" => $request->input('name'),
                                      "description" => $request->input('description'),
                                      "price" => $request->input('price'),
                                      "level1" => $request->input('level1'),
                                      "level2" => $request->input('level2'),
                                      "level3" => $request->input('level3'),
                                      "updated_at" => $dateTime
                                      ]);    
                                      
                                      
           if( $update == true){
               $message['error'] = 0;
               $message['message'] = "this service is updated successfully";
           }else{
               $message['error'] = 1;
               $message['message'] = "there is an error, please try again";
           }
            
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    }
    
    
    public function add_services(Request $request){
        try{
            
            
            $updated_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($updated_at)));
        
            $add = new \App\Service;
            
            $add->name = $request->input('name');
            $add->description = $request->input('description');
            $add->price = $request->input('price');
            $add->level1 = $request->input('level1');
            $add->level2 = $request->input('level2');
            $add->level3 = $request->input('level3');
            $add->save();
            
            if($add == true){
                $message['error'] = 0;
                $message['message'] = " a new service is add successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
        }catch(Exception $ex){
                   $message['error']=2;
                   $message['message']='error'.$ex->getMessage();
            }
        
         return response()->json($message);
    }
    
    
    
    
    public function delete_serviceImages(Request $request){
        try{
            
            $image_id = $request->input('image_id');
            
            
            $delete_data = \App\image_services::where('id',$image_id )->delete();
            
            if( $delete_data == true){
                $message['error'] = 0;
                $message['message'] = "this image is deleted sucessfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
            
            
            
        }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
        }
        
         return response()->json($message);
    }
    
    
    public function add_serviceImages(Request $request){
        try{
            $updated_at = carbon::now()->toDateTimeString();
		    $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($updated_at)));
        
            $image = $request->file('image');
        
            if(isset($image)) {
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                $destinationPath_id = 'uploads';
                $image->move($destinationPath_id, $savedFileName);
    
                $images = $savedFileName;
                  
            }else{
                $images = NULL;       
            }
            
            
            $add =  new \App\image_services;
            
            $add->image = $images;
            $add->service_id = $request->input('service_id');
            $add->created_at = $dateTime;
            $add->save();
            
            if($add == true){
                $message['error'] = 0;
                $message['message'] = " A new service image is inserted successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
        }
        
         return response()->json($message);
    }
    
    
    
    // show_ all visits days
    
    
    public function show_all_visits(){
        try{
            
            $get_data = \App\Visits::select('id', 'number','date')->get();
            
            if( count($get_data) >0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is the visits in each day";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
        }
        
         return response()->json($message);
    }
    
    
    
}
