@extends('layouts.admin_const')

@section('content')

<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12" style="float: right;margin-left: auto;">
                        <h2 style="font-size: xx-large;"> :تعديل الخدمات </h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">تعديل الخدمات</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            
            
             <div class="body">
                 <form id="basic-form" method="post"action="/update_service" novalidate enctype="multipart/form-data" class="text-right">
                                           {{ csrf_field() }}

                        @foreach($get_servicedata as $service)
                        
                        <input type="hidden" name="service_id" value="{{$service->id}}">

                        <div class="form-group">
                            <label style="    font-size: x-large;">اسم الخدمة</label>
                            <input type="text" name='name' value="{{$service->name}}" class="form-control text-right" required>
                        </div>
                        
                       
                        
                        <div class="form-group">
                            <label style="    font-size: x-large;">التفاصيل</label>
                            <textarea type="text" name='description' class="form-control text-right" required>  {{$service->description}}</textarea>
                        </div>
                        
                         <div class="form-group">
                            <label style="    font-size: x-large;">الصورة</label><br>
                           <?php
                           $image = $service->image;
                                                
                                if( $image == ''){

                                }else{?>
                            &nbsp;  &nbsp;<img src="uploads/{{$service->image}}" width="100" height="80" style="margin-right: 90px;"><br><br><br>
                                <?php } ?>
                            <input type="file" name="image"  class="form-control text-right" required>
                        </div>
                        
                  
                         <div class="form-group">
                            <label style="    font-size: x-large;">السعر</label>
                            <input type="text" name='price' value="{{$service->price}}" class="form-control text-right" required>
                        </div>
                        
                        <div class="form-group">
                            <label style="    font-size: x-large;"> المرحلة الاولى </label>
                            <input type="text" name='service_level1' value="{{$service->level1}}" class="form-control text-right" required>
                        </div>
                       
                        <div class="form-group">
                            <label style="    font-size: x-large;"> المرحلة الثانية</label>
                            <input type="text" name='service_level2' value="{{$service->level2}}" class="form-control text-right" required>
                        </div>
                        <div class="form-group">
                            <label style="    font-size: x-large;"> المرحلة الثالثة</label>
                            <input type="text" name='service_level3' value="{{$service->level3}}" class="form-control text-right" required>
                        </div>
                        
                        
                        
                          
                        <br>    
                        <button type="submit" name="edit_category"class="btn btn-primary" style="margin-right: 556px;font-size: 22px;">حفظ</button>
                          @endforeach
                    </form>
             </div>
            
            
        </div>
</div>

@endsection