@extends('layouts.admin_const')

@section('content')



<div id="wrapper">

    

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                       <!--<h2>Jquery Datatable</h2>-->
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item">وسائل التواصل الاجنماعى   </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                             
                            <ul class="nav nav-tabs-new">
                                <li style="float: right;margin-left: auto;"><h2 style="font-size: xx-large;">:وسائل التواصل الاجتماعى    </li>
                        </ul> </h2> </li> &nbsp;&nbsp;

                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover js-basic-example dataTable table-custom text-center">
                                    
                                       <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Facebook </th>
                                            <th>Instagram</th>
                                            <th>Twitter</th>
                                            <th>Youtube</th>
                                            <th>Snapchat</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                            
                                            <th>تعديل</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($get_data as $social)
                                        <tr>
                                            <td>{{$social->id}}</td>
                                            <td>{{$social->facebook}}</td>  
                                            <td>{{$social->instagram}}</td>
                                            <td>{{$social->twitter}}</td>
                                            <td>{{$social->youtube}}</td>
                                            <td>{{$social->snapchat}}</td>
                                            <td>{{$social->Latitude}}</td>
                                            <td>{{$social->Longitude}}</td>
                                           <td>
                                               
                                          
                                               <form action="/edit_social" method="get" id='sub_{{$social->id}}'  enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="social_id" value="{{$social->id}}">
                                                    <a onclick="$('#sub_{{$social->id}}').submit();">
                                                        <button class="btn btn-sm btn-icon btn-pure btn-default on-default m-r-5 button-edit" data-toggle="tooltip" data-original-title="تعديل">
                                                        <i class="icon-pencil" aria-hidden="true"></i>
                                                        </button>                                      
                                                    </a>
                                                </form>
                                                
                                                
                                            </td> 
                                           
                                        </tr>
                                      @endforeach                                    

                                    </tbody>
                                  
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>            
        </div>       
    </div>
</div>

         



@endsection