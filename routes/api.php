<?php

use Illuminate\Http\Request;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: *');

header('Content-Type: application/json; charset=UTF-8', true); 
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post("affiliate_login","affiliateController@affiliate_login");

Route::post("affiliate_register","affiliateController@affiliate_register");

Route::get("show_user_ById","affiliateController@show_user_ById");

Route::get("show_followUser_ByID","affiliateController@show_followUser_ByID");

Route::post("update_profile","affiliateController@update_profile");

Route::get("show_contacts_Byaffiliator","affiliateController@show_contacts_Byaffiliator");

Route::get("show_affiliateVisits","affiliateController@show_affiliateVisits");

Route::get("show_terms_link","contactController@show_terms_link");



Route::get("version_Read","importDocController@version_Read");

Route::get("show_documents","importDocController@show_documents");



// web apis ::

Route::get("visits","AllWebController@visits");
Route::get("all_info","AllWebController@all_info");

Route::post('send_message','AllWebController@send_message');

Route::post('academy_contact','AllWebController@academy_contact');

Route::get("show_service","AllWebController@show_service");

Route::get("show_service_image","AllWebController@show_service_image");

Route::get("show_serviceData","AllWebController@show_serviceData");

Route::get("show_sliders","AllWebController@show_sliders");



//dashboard

// admin


Route::get("show_adminById","AdminCOntroller@show_adminById");

Route::post("admin_login","AdminCOntroller@admin_login");

Route::post("edit_adminprofile","AdminCOntroller@edit_adminprofile");

//users:

Route::get("show_allusers","AdminCOntroller@show_allusers");

Route::get("show_userByid","AdminCOntroller@show_userByid");

Route::get("delete_user","AdminCOntroller@delete_user");

Route::post("add_user","AdminCOntroller@add_user");

Route::post("update_user","AdminCOntroller@update_user");

//about

Route::get("show_about","AdminCOntroller@show_about");

Route::post("update_about","AdminCOntroller@update_about");


//contact

Route::get("show_contact","AdminCOntroller@show_contact");

Route::get("delete_contact","AdminCOntroller@delete_contact");

//stages

Route::get("show_stages","AdminCOntroller@show_stages");

Route::get("show_stagesbyid","AdminCOntroller@show_stagesbyid");

Route::post("update_stages","AdminCOntroller@update_stages");

//service
Route::get("show_servicebyid","AdminCOntroller@show_servicebyid");

Route::get("show_services","AdminCOntroller@show_services");

Route::get("delete_service","AdminCOntroller@delete_service");

Route::post("update_service","AdminCOntroller@update_service");

Route::post("add_services","AdminCOntroller@add_services");

Route::get("delete_serviceImages","AdminCOntroller@delete_serviceImages");

Route::post("add_serviceImages","AdminCOntroller@add_serviceImages");


//all users visits ::

Route::get("show_all_visits","AdminCOntroller@show_all_visits");

//important documents ::

Route::get("show_document_data","importDocController@show_document_data");

Route::post("update_importantDocuments","importDocController@update_importantDocuments");


//call info ::

Route::get("show_callInfo","contactController@show_callInfo");

Route::post("update_callInfo","contactController@update_callInfo");

//review_images ::

Route::get("show_reviewImages","imageController@show_reviewImages");

Route::get("delete_reviewImage","imageController@delete_reviewImage");

Route::post("add_reviewImage","imageController@add_reviewImage");


// service _images ::

Route::get("show_ServiceImages","imageController@show_ServiceImages");

Route::get("delete_serviceImage","imageController@delete_serviceImage");

Route::post("add_serviceImage","imageController@add_serviceImage");


Route::get("show_webContent","contactController@show_webContent");

Route::post("update_webContent","contactController@update_webContent");

// links 

Route::get("show_links","contactController@show_links");

Route::post("update_links","contactController@update_links");

// colors ::

Route::get("show_colors","contactController@show_colors");

Route::post("update_colors","contactController@update_colors");


// academy contact  ::

Route::get("show_academyContact","contactController@show_academyContact");

Route::get("update_contactStatus","contactController@update_contactStatus");

Route::get("show_affiliate_Data","contactController@show_affiliate_Data");


// affiliators ::

Route::get("show_all_affilitors","affiliateController@show_all_affilitors");

Route::get("show_affiliator_ByID","affiliateController@show_affiliator_ByID");

Route::post("update_affiliators","affiliateController@update_affiliators");

Route::post("insert_affiliators","affiliateController@insert_affiliators");

Route::get("deleted_affiliators","affiliateController@deleted_affiliators");

Route::get("show_affiliate_visits","affiliateController@show_affiliate_visits");

Route::get("show_affiliate_follow","affiliateController@show_affiliate_follow");


//social mediaa::

Route::get("show_socialContact","AllWebController@show_socialContact");

Route::post("update_socailContacts","AllWebController@update_socailContacts");

// home

Route::get("count_visits","AllWebController@count_visits");

Route::get("todays_visit","AllWebController@todays_visit");

Route::get("show_visitsDate_charts","AllWebController@show_visitsDate_charts");

Route::get("show_visitsNumber_charts","AllWebController@show_visitsNumber_charts");

Route::get("show_registerDate_chart","AllWebController@show_registerDate_chart");

Route::get("show_registerNumber_chart","AllWebController@show_registerNumber_chart");

Route::get("show_payiedDate_chart","AllWebController@show_payiedDate_chart");

Route::get("show_payiedNumber_chart","AllWebController@show_payiedNumber_chart");

Route::get("all_counts","AllWebController@all_counts");


//whatsapp contact ::
Route::get("show_whatsaffiliate_data","contactController@show_whatsaffiliate_data");






/*************************************************************** Apis for academy mariam ****************************************************/

Route::get("show_AcademyContacts" , "newController@show_AcademyContacts");

Route::get("show_AcademyContacts_byDate" , "newController@show_AcademyContacts_byDate");

Route::get("show_whatscontact" , "newController@show_whatscontact");

Route::get("show_contactUs" , "newController@show_contactUs");

Route::get("show_affiliate_data" , "newController@show_affiliate_data");

Route::get("show_service_data" , "newController@show_service_data");

Route::get("show_affiliate_ByID" , "newController@show_affiliate_ByID");

Route::get("show_affiliate_withFollowData" , "newController@show_affiliate_withFollowData");


Route::get("show_academy_visits" , "newController@show_academy_visits");

Route::get("show_academy_visits_byDate" , "newController@show_academy_visits_byDate");

Route::get("show_affiliate_visits_ByDate" , "newController@show_affiliate_visits_ByDate");

Route::get("show_affiliate_visits" , "newController@show_affiliate_visits");



Route::get("show_AcademyContacts_fromID" , "newController@show_AcademyContacts_fromID");

Route::get("show_whatscontact_fromID" , "newController@show_whatscontact_fromID");

Route::get("show_contactUs_fromID" , "newController@show_contactUs_fromID");



/****************************reservations ***************************/


Route::get("show_affiliate_service" , "reserveController@show_affiliate_service");

Route::post("add_affiliate_service" , "reserveController@add_affiliate_service");

Route::post("update_affiliate_service" , "reserveController@update_affiliate_service");

Route::get("delete_service" , "reserveController@delete_service");

Route::get("show_serviceByID" , "reserveController@show_serviceByID");

Route::get("show_reservations" , "reserveController@show_reservations");

/******************** affiliate coupons***************/

Route::get("show_affiliate_coupons" , "couponController@show_affiliate_coupons");

