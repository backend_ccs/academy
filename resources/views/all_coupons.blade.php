@extends('layouts.admin_const')

@section('content')



<div id="wrapper">


    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                       <!--<h2>Jquery Datatable</h2>-->
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item">  كوبونات العامة </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                             
                            <ul class="nav nav-tabs-new">
                                
                                <li style="float: right;margin-left: auto;"><h2 style="font-size: xx-large;">:جميع كوبونات العامة </h2> </li> &nbsp;&nbsp;
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover js-basic-example dataTable table-custom text-center">
                                    
                                       <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th> أسم الدورة</th>
                                            <th>الكود  </th>
                                            <th>النسبه </th>
                                            <th>وقت الانشاء</th>
                                            
                                            
                                            <th>مسح</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($get_data as $data)
                                        <tr>
                                            <td>{{$data->coupon_id}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->code}}</td>
                                            <td>{{$data->value}}</td>
                                            <td>{{$data->created_at}}</td>
                                             
                                            <td>
                                              <form action="/delete_coupon" method="GET"  id="sub_{{$data->coupon_id}}" enctype="multipart/form-data">
                                   
                                                   {{ csrf_field() }}
                               
                                                   <input type="hidden" name="coupon_id" value="{{$data->coupon_id}}">
                                      
                                                   <a  onclick="$('#sub_{{$data->coupon_id}}').submit();">

                                                    <button class="btn btn-sm btn-icon btn-pure btn-default on-default button-remove" data-toggle="tooltip" data-original-title="مسح">
                                                            <i class="icon-trash" aria-hidden="true"></i>
                                                    </button>  
                                                   </a>      
                           
                                               </form>
                                            </td>
                                            
                                        </tr>
                                      @endforeach                                    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>            
        </div>       
    </div>
</div>

         



@endsection