<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Input;
use carbon\carbon;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    private  $_api_context;
    
    public function __construct(){
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
                $paypal_conf['client_id'],
                $paypal_conf['secret']
                ));
                
                $this->_api_context->setConfig($paypal_conf['settings']);
    }
    
    
    public function  payWithpaypal(Request $request){
        

        $fullname = $request->input('fullname');
        $phone = $request->input('phone');  
        $service_id = $request->input('service');
        
        if($request->has('fullname') != '' || $request->has('fullname') != NULL || $request->has('phone') != '' || $request->has('phone') != NULL){

        if( $request->has('service') != '' || $request->has('service') != NULL){ 

                $x= implode(" , ", $service_id);
                
                if (isset($_COOKIE["affiliate"])){   
                
                    $affiliate_id = $_COOKIE['affiliate'];
                }else{
                       
                    $affiliate_id = NULL;
                }
        
                
                $created_at = carbon::now()->toDateTimeString();
        
                $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));
        
                if($request->has('code')){
                    $code_id = \App\AffiliateCoupon::where('code' ,$request->code)->value('id');
        
                }
                $insert_contact = DB::insert("insert into doctor_contact (name, phone,service_id,affiliate_id,state,copoun_id,created_at ) values (?,?,?,?,?,?,?)",
                                            [$fullname,$phone,$x,$affiliate_id,"wait",$code_id,$dateTime]);
                  
                
                
                
                $select_service = \App\doctor_contact::select('service_id')->where([['name',$fullname],['phone',$phone],['created_at',$dateTime]])->get();
                
    
            $get_price = array();
            $g = 0;$total=0;
            $change_coin = 0;
            $total_price=0;
            $get_code_value = 0;
            
            if($request->has('code')){
                $get_code_value = \App\AffiliateCoupon::where('code' ,$request->code)->value('value');
    
            }
            
            foreach($select_service as $serve){
                
                foreach (explode(', ', $serve->service_id) as $client_website){
                    array_push($get_price, DB::select("SELECT  price FROM `services` where name= ?",[$client_website]));
                    $discound = 0;
                    $discound = $get_code_value == 0 ? 0 : ( $get_price[$g][0]->price * $get_code_value ) / 100;
                    
                    $after_discound = $get_price[$g][0]->price - $discound;
                    
                    $total_price += $after_discound ;
                    $g++;
                }
            }
            
            $total = $total_price/3.75;                                             // change from dollar to ryal.
            
            $payer = new Payer();
            $payer->setPaymentMethod('paypal');
     
            $item_1 = new Item();
     
            $item_1->setName('Item 1') /** item name **/
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setPrice($total); /** unit price **/
     
            $item_list = new ItemList();
            $item_list->setItems(array($item_1));
     
            $amount = new Amount();
            $amount->setCurrency('USD')
                ->setTotal($total);
     
            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription('Your transaction description');
     
            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(URL::route('paypal.status')) /** Specify return URL **/
                ->setCancelUrl(URL::route('paypal.status'));
     
            $payment = new Payment();
            $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));
            /** dd($payment->create($this->_api_context));exit; **/
            try {
     
                $payment->create($this->_api_context);
                
                $update_contact = \App\doctor_contact::where([['name',$fullname],['phone',$phone],['created_at',$dateTime]])
                                                    ->update([
                                                       'total_price' =>$total,
                                                       'type' => 'paypal',
                                                    ]);
            
            
           
     
            } catch (\PayPal\Exception\PPConnectionException $ex) {
     
                if (\Config::get('app.debug')) {
     
                    \Session::put('error', 'Connection timeout');
                    return Redirect::route('paywithpaypal');
     
                } else {
     
                    \Session::put('error', 'Some error occur, sorry for inconvenient');
                    return Redirect::route('paywithpaypal');
     
                }
     
            }
     
            foreach ($payment->getLinks() as $link) {
     
                if ($link->getRel() == 'approval_url') {
     
                    $redirect_url = $link->getHref();
                    break;
     
                }
     
            }
     
            /** add payment ID to session **/
            Session::put('paypal_payment_id', $payment->getId());
     
            if (isset($redirect_url)) {
     
                /** redirect to paypal **/
                return Redirect::away($redirect_url);
     
            }
     
            
           
            
            
            
            \Session::put('error', 'Unknown error occurred');
            return Redirect::route('paywithpaypal');
     
        }else{
            echo "<script>alert('منفضلك اختار خدمة');"
                        . "window.location.replace('/')"
                        . "</script>";
        }
    }else{
            echo "<script>alert('منفضلك ادخل جميع البيانات');"
                        . "window.location.replace('/')"
                        . "</script>";
        }
    }
    
    
    public function getPaymentStatus(Request $request){
      
        
      
                
        
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
 
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
 
            \Session::put('error', 'Payment failed');
            return Redirect('/');
 
        }
 
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
 
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
 
        if ($result->getState() == 'approved') {
 
            \Session::put('success', 'Payment success');
            return Redirect('/');
 
        }

        \Session::put('error', 'Payment failed');
        return Redirect('/');
    }
    
    
    
    

}
