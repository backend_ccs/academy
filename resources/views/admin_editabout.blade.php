@extends('layouts.admin_const')

@section('content')

<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12" style="float: right;margin-left: auto;">
                        <h2 style="font-size: xx-large;"> :تعديل من نحن </h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">تعديل من نحن</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            
            
             <div class="body">
                 <form id="basic-form" method="post"action="/update_about" novalidate enctype="multipart/form-data"  class="text-right">
                                           {{ csrf_field() }}

                        @foreach($get_data as $about)
                        
                        <input type="hidden" name="about_id" value="{{$about->id}}">

                        <div class="form-group">
                            <label style="    font-size: x-large;">المحتوى </label>
                            <textarea class="form-control text-right" rows="5" cols="30" name="content" required>{{$about->content}}</textarea>

                        </div>
                        
                       <div class="form-group">
                            <label style="    font-size: x-large;"> لينك الشروط و الاحكام </label>
                            <input type="text" name='terms_condition'  class="form-control text-right"  value="{{$about->terms_condition}}" required>

                        </div>
                        
                        <div class="form-group">
                            <label style="    font-size: x-large;"> لينك شروط الدفع </label>
                            <input type="text" name='payment_terms'  class="form-control text-right" value="{{$about->payment_terms}}" required>

                        </div>
                       
                      
                          
                        <br>    
                        <button type="submit" name="edit_about"class="btn btn-primary" style="margin-right: 556px;font-size: 22px;">حفظ</button>
                          @endforeach
                    </form>
             </div>
            
            
        </div>
</div>

@endsection