@extends('layouts.admin_const')

@section('content')

<div class="wrapper">
    
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12"style="float: right;margin-left: auto;">
                        <h2 style="font-size: xx-large;" > اضافة مستخدم</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">اضافة مستخدم </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            
             <div class="body">
                 <form id="basic-form" method="post"action="/add_user" novalidate enctype="multipart/form-data"  class="text-right">
                                           {{ csrf_field() }}

                        
                        <div class="form-group">
                            <label style="    font-size: x-large;">الاسم الاول</label>
                            <input type="text" name='first_name'  class="form-control text-right" required>
                        </div>
                        
                        <div class="form-group">
                            <label style="    font-size: x-large;">الاسم الاخير</label>
                            <input type="text" name='last_name' class="form-control text-right" required>
                        </div>
                        
                         <div class="form-group">
                            <label style="    font-size: x-large;">الصورة </label>
                           
                            <input type="file" name="image"  class="form-control" required>
                        </div>
                        
                        
                         <div class="form-group">
                            <label style="    font-size: x-large;">البريد الالكترونى</label>
                            <input type="text" name='email' class="form-control text-right" required>
                        </div>
                         <div class="form-group">
                            <label style="    font-size: x-large;">رقم التليفون</label>
                            <input type="text" name='phone' class="form-control text-right" required>
                        </div>
                        
                          <div class="form-group">
                            <label style="    font-size: x-large;">كلمة السر</label>
                            <input type="text" name='password'  class="form-control text-right" required>
                        </div>
                          <div class="form-group">
                            <label style="    font-size: x-large;">الحالة</label>
                            
                            <select class="form-control text-right "  name="status" style="direction: rtl" required>
                              <option>اختار الحالة</option>
                             
                              
                              <option value="admin">admin</option>

                          </select>
                        </div>
                            
                        <br>    
                        <button type="submit" name="Add_review"class="btn btn-primary" style="margin-right: 556px;font-size: 22px;">حفظ</button>
                          
                    </form>
             </div>
            
            
        </div>
</div>
</div>

@endsection