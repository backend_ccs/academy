@extends('layouts.admin_const')

@section('content')

<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12" style="float: right;margin-left: auto;">
                        <h2 style="font-size: xx-large;"> :تعديل معلومات الاتصال </h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">تعديل معلومات الاتصال</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            
            
             <div class="body">
                 <form id="basic-form" method="post"action="/update_call" novalidate enctype="multipart/form-data" class="text-right">
                                           {{ csrf_field() }}

                        @foreach($get_callInfo as $call)
                        
                        <input type="hidden" name="call_id" value="{{$call->id}}">

                        <div class="form-group">
                            <label style="    font-size: x-large;">العنوان </label>
                            <input type="text" name='address' value="{{$call->address}}" class="form-control text-right" required>

                        </div>
                        
                         <div class="form-group">
                            <label style="    font-size: x-large;">رقم التليفون </label>
                            <input type="text" name='phone' value="{{$call->phone}}" class="form-control text-right" required>

                        </div>
                        
                         <div class="form-group">
                            <label style="    font-size: x-large;">رقم الواتس </label>
                            <input type="text" name='whatsapp' value="{{$call->whatsapp}}" class="form-control text-right" required>

                        </div>
                        <div class="form-group">
                            <label style="    font-size: x-large;">البريد الالكترونى </label>
                            <input type="email" name='email' value="{{$call->email}}" class="form-control text-right" required>

                        </div>
                       
                      
                          
                        <br>    
                        <button type="submit" name="edit_about"class="btn btn-primary" style="margin-right: 556px;font-size: 22px;">حفظ</button>
                          @endforeach
                    </form>
             </div>
            
            
        </div>
</div>

@endsection