<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use carbon\carbon;
use Cookie;
use response;
use Session;
use Excel;
use \Cache;

class contactController extends Controller
{
    public function ajax_get_imgs(Request $request){
         $id=$request->input('id');
        
       $get_serviceName = \App\Service::where('id',$id)->value('name');
        $get_listimage = DB::select("select id, image from image_services where service_id=?",[$id]);
            
        $response=' <h4 style=" text-align: center; ">'.$get_serviceName.'</h4>';
        $i = 0;
        foreach( $get_listimage as $photo ){
                       $i++;

            if($i == 1){
                $temp = 'active';
            }else{
                $temp = '';
            }
            $response.='<div class="carousel-item '.$temp.'">
                    
                              <img class="d-block img-fluid" src="/uploads/'.$photo->image.'" width="100%" height="100%" >
                         </div>  ';
            
            
            
              
        }
        return response()->json(['data'=>$response]);
       // retrun $res;
        
        
     }
    
     public function show_home(){
        // \Cookie::queue(\Cookie::forget('affiliate'));
  
            if( Cookie::get('affiliate') != NULL){
              $get_cokie = Cookie::get('affiliate');
  
         
  
          return redirect('/id='.$get_cokie);
          }else{
              $now_date = date("Y/m/d");
  
          $get_num = DB::select("select number from visits where date=?",[$now_date]);
  
  
  
          
          if( count($get_num)>0 ){
              $visits = $get_num[0]->number + 1;
          
              $get_sameDate = DB::update("update  visits set number=? where date=?",[$visits,$now_date]);
          
          }else{
              $insert_visit = DB::insert("insert into visits (number,date) values(?,?)",['1',$now_date]);
  
          }  
          return view('index');
      }  
      }
      
       public function index($affiliate){
  
          //setcookie('affiliate',$affiliate );
        
              $get_cokie = -1;
  
          if( Cookie::has('affiliate') != NULL){
              $get_cokie = Cookie::get('affiliate');
          }else{
              $cookie = Cookie::queue('affiliate', $affiliate, 21600);
              $get_cokie = $affiliate;
          }
          $now_date = date("Y/m/d");
  
          $get_num = DB::select("select number from visits where date=?",[$now_date]);
           
           
          $get_affiliateVisit = DB::select("select number from affiliate_visits where date=? and affiliate_id=? ",[$now_date,$get_cokie ]);
          
          if( count($get_num)>0 ){
              $visits = $get_num[0]->number + 1;
  
             $get_sameDate = DB::update("update  visits set number=? where date=?",[$visits,$now_date]);
  
          }else{
             $insert_visit = DB::insert("insert into visits (number,date) values(?,?)",['1',$now_date]);
  
          }
          
          if( count($get_affiliateVisit)>0 ){
              $affiliate_visit = $get_affiliateVisit[0]->number + 1;
              
              $update_visit = DB::update("update  affiliate_visits set number=? where date=? and affiliate_id=?",[$affiliate_visit,$now_date , $get_cokie]);
  
          }else{
              $insert_affiliatevisit = DB::insert("insert into affiliate_visits (affiliate_id,number,date) values(?,?,?)",[$get_cokie , '1',$now_date]);
  
          }
          
          
           
     // $x =$_COOKIE['affiliate'];
      return view('index');
  
      }
  
    public function doctorContact(Request $request){
        
        
        
        $fullname = $request->input('fullname');
        $phone = $request->input('phone');  
        $service_id = $request->input('service');
        
        if($request->has('fullname') != '' || $request->has('fullname') != NULL || $request->has('phone') != '' || $request->has('phone') != NULL){
        if( $request->has('service') != '' || $request->has('service') != NULL){ 
            $x= implode(" , ", $service_id);
           
            $created_at = carbon::now()->toDateTimeString();
    
            $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));
    
            if (Cookie::get('affiliate') != NULL){   
            
                $affiliate_id = Cookie::get('affiliate');                  
                
            }else{
                   
                $affiliate_id = NULL;
            }
            
           
            
           
            $insert_contact = DB::insert("insert into doctor_contact (name, phone,service_id,affiliate_id,state,created_at ) values (?,?,?,?,?,?)",
                                        [$fullname,$phone,$x,$affiliate_id,"wait",$dateTime]);
            
           
            
            $select_service = \App\doctor_contact::select('service_id')->where([['name',$fullname],['phone',$phone],['created_at',$dateTime]])->get();
            
    
            $get_price = array();
            $g = 0; 
            $total=0;
            $affiliate_money=0;
            foreach($select_service as $serve){
                
                foreach (explode(', ', $serve->service_id) as $client_website){
                    array_push($get_price, DB::select("SELECT  price FROM `services` where name= ?",[$client_website]));
    
                    $total += $get_price[$g][0]->price ;
                    
                    
                 /*   $calculate_present = ($get_price[$g][0]->present * $get_price[$g][0]->price)/100;    
                    $affiliate_money += $calculate_present ;*/
                       
                    $g++;
                }
            }
            
         /*   if(isset($_COOKIE["affiliate"])){   
            
                $affiliate_id = $_COOKIE['affiliate'];  
                
                
                $update_affiliate = \App\affiliate::where('id',$affiliate_id)->update(['balance'=>$affiliate_money]);
                
            }*/
            
            $update_contact = \App\doctor_contact::where([['name',$fullname],['phone',$phone],['created_at',$dateTime]])
                                                    ->update([
                                                        'total_price' =>$total,
                                                        'type' => 'cash',
                                                    ]);
            
    
           
            
            
            if($insert_contact == 1){
                echo "<script>alert('تم ارسال بياناتك و سيتم التواصل معك قريبا');"
                        . "window.location.replace('/')"
                        . "</script>";
            }else{
                echo "<script>alert('there is an error, please try again');"
                        . "window.location.replace('/')"
                        . "</script>";
            }
        }else{
            echo "<script>alert('منفضلك اختار خدمة');"
                        . "window.location.replace('/')"
                        . "</script>";
        }
        }else{
            echo "<script>alert('منفضلك ادخل جميع البيانات');"
                        . "window.location.replace('/')"
                        . "</script>";
        }
    }
    
    public function show_services(){
          $created_at = carbon::now()->toDateTimeString();

          $date = date('Y-m-d',strtotime('+3 hours',strtotime($created_at)));
          $time = date('H:i:s',strtotime('+3 hours',strtotime($created_at)));

        $get_num = DB::select("select number from visits where date=?",[$date]);
         
        
         
         
         if( count($get_num)>0 ){
             $visits = $get_num[0]->number + 1;
         
            $get_sameDate = DB::update("update  visits set number=? where date=?",[$visits,$date]);
          
         }else{
            $insert_visit = DB::insert("insert into visits (number,date,time) values(?,?,?)",['1',$date,$time]);

         }
        $allservices =\App\Service::all();
        
        return view('/index',['allservices'=>$allservices]);
    }
    
    
    public function sendmessage(Request $request){
        
        $country = $request->input('country');
        $phone = $request->input('phone');
        $message = $request->input('message');
        
        $created_at = carbon::now()->toDateTimeString();

        $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));

        $insert_message = DB::insert("insert into contact_us (country,email,message,created_at) values(?,?,?,?)",
                            [$country,$phone,$message,$dateTime]);
        
          if($insert_message == 1){
            echo "<script>alert('Your message is inserted successfully');"
                    . "window.location.replace('/')"
                    . "</script>";
        }else{
            echo "<script>alert('there is an error, please try again');"
                    . "window.location.replace('/')"
                    . "</script>";
        }
    }
    
    
    
    //****** web contactpage *******
    
    public function show_contactPage(){
        return view("admin_contactus");
    }
    
    public function contact_data(){
        
        $get_data = DB::select("select * from contact_us");
        
        return view("admin_contactus",['get_data'=>$get_data]);
    }
    
    public function delete_contactus(Request $request){
        
        $contact_id = $request->input('contact_id');
        
        $delete_contact = DB::delete("delete from contact_us where id=? ",[$contact_id]);
        
        
          if( $delete_contact == 1){
            echo "<script>alert('تم مسح هذا التواصل بنجاح');"
                    . "window.location.replace('/dashboard')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                    . "window.location.replace('/admin_contactus')"
                    . "</script>";
        }
    }
    
    
    
    //********** doctor contact **************
    
    
    public function show_doctorcontact(){
        return view('admin_doctorcontact');
    }
    
    public function doctorcontact_data(){
        
        $get_data = \App\doctor_contact::select('doctor_contact.id','doctor_contact.name', 'doctor_contact.phone', 'doctor_contact.address', 'doctor_contact.nationality', 'doctor_contact.service_id', 'doctor_contact.affiliate_id', 'doctor_contact.state',
                                                'doctor_contact.total_price', 'doctor_contact.type', 'doctor_contact.copoun_id', 'doctor_contact.created_at' , 'affiliate_coupon.code')
                                        ->leftjoin('affiliate_coupon' , 'doctor_contact.copoun_id', '=' ,'affiliate_coupon.id')->get();
        
        
        return view('admin_doctorcontact',['get_data'=>$get_data]);
    }
    
    public function delete_doctorcontact(Request $request){
        
        $doctor_id = $request->input('doctor_id');
        
        $delete_docctorcontact = DB::delete("delete from doctor_contact where id=?",[$doctor_id]);
         
        if( $delete_docctorcontact == 1){
            echo "<script>alert('تم مسح هذا التواصل بنجاح');"
                    . "window.location.replace('/dashboard')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                    . "window.location.replace('/admin_doctorcontact')"
                    . "</script>";
        }
        
       
    }
    
    
    
    //  about us ::
    
    public  function show_about(Request $request){
        return view('admin_aboutus');
    }
    
    public function about_data(){
        
        $get_data = DB::select("select * from about_us");
        
        return view('admin_aboutus',["get_data"=>$get_data]);
    }
    
    public function show_editabout(){
         return view('admin_editabout');
    }

    public function edit_about(Request $request){
               
        $about_id = $request->input('about_id');
        
        $get_data = DB::select("select * from about_us where id=?",[$about_id]);

        return view('admin_editabout',['get_data'=>$get_data]);
    }
    
    public function update_about(Request $request){
        
        $about_id = $request->input('about_id');
        
        $content = $request->input('content');
        $terms_condition = $request->input('terms_condition');
        $payment_terms = $request->input('payment_terms');

        
        $update_about= DB::update("update about_us set content=? , 
                                                terms_condition=? ,
                                                payment_terms =? where id=?",[$content, $terms_condition , $payment_terms, $about_id]);
        
        
        if($update_about == 1){
            echo "<script>alert('تم التعديل بنجاح');"
                    . "window.location.replace('/dashboard')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                     . "window.location.replace('/show_about');"
                    . "</script>";
        }
    }
    
    public function  show_call(){
        return view('admin_callInfo');
    }

    public function call_Data(){
        $get_callInfo =  DB::select("select *  from call_info");
        
        return view("admin_callInfo",['get_callInfo'=>$get_callInfo]);
       
    }
    
    public  function edit_call(){
        return view('admin_editcallInfo');
    }
    
    public function edit_callData (Request $request){
        $call_id = $request->input('call_id');
        
        $get_callInfo =  DB::select("select *  from call_info where id=?",[$call_id]);
        
        return view("admin_editcallInfo",['get_callInfo'=>$get_callInfo]);

    }
    
    public function  upload_call(Request $request){
        
        $call_id = $request->input('call_id');
        
        $address = $request->input('address');
        $phone = $request->input('phone');
        $whatsapp = $request->input('whatsapp');
        $email = $request->input('email');
        
        $upload_call = DB::update("update call_info set address=?, phone=?,whatsapp=?, email=? where id=?",
                        [$address,$phone,$whatsapp, $email, $call_id]);
        
        
        if($upload_call == 1){
            echo "<script>alert('تم التعديل بنجاح');"
                    . "window.location.replace('/show_call')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                     . "window.location.replace('/edit_call');"
                    . "</script>";
        }
                
    }
    
    
    
    public function homeDate(Request $request){
        
         $fromDate = $request->input('fromDate');
         $toDate  = $request->input('toDate');
        
        $get_visitDay = \App\Visits::whereBetween('date',[$fromDate,$toDate])->sum('number');
        
        return view('/home',['get_visitDay' =>$get_visitDay]);
    }
    
    
    
    public static function count_visit(){
                              
           $created_at = carbon::now()->toDateTimeString();

          $date = date('Y-m-d',strtotime('+3 hours',strtotime($created_at)));
          $time = date('H:i:s',strtotime('+3 hours',strtotime($created_at)));

        $get_num = DB::select("select number from visits where date=?",[$date]);
         
        
         
         
         if( count($get_num)>0 ){
             $visits = $get_num[0]->number + 1;
         
            $get_sameDate = DB::update("update  visits set number=? where date=?",[$visits,$date]);
          
         }else{
            $insert_visit = DB::insert("insert into visits (number,date,time) values(?,?,?)",['1',$date,$time]);

         }
         return view('index');
    }
    
    
    public function excel(Request $request){
      $count = 0;
      
      
        $fromDate = $request->input('fromDate');
        $toDate = $request->input('toDate');


        if( $request->has('fromDate')  &&  $request->has('toDate')){
            $get_data = \App\doctor_contact::select('doctor_contact.id', 'doctor_contact.name','doctor_contact.phone','doctor_contact.service_id',DB::raw('(concat(affiliate.first_name," ", affiliate.last_name)) as affiliate_name'),'doctor_contact.affiliate_id', 'doctor_contact.state','doctor_contact.total_price','doctor_contact.type','doctor_contact.created_at')
                                               ->leftjoin('affiliate' , 'doctor_contact.affiliate_id', '=', 'affiliate.id')
                                               ->whereDate('doctor_contact.created_at' ,'>=' , $fromDate)
                                               ->whereDate('doctor_contact.created_at' ,'<=' , $toDate)->get();
               
            
            $doctor_array[] = array('id','name','phone','services','affiliate_name','state','total_price','type','created_at');
            
            foreach ($get_data as $doctorss){
                $doctor_array[] = array(
                    'id' =>  $doctorss->id,
                    'name' => $doctorss->name,
                    'phone' => $doctorss->phone,  
                    'services'=>  $doctorss->service_id,
                    'affiliate_name' => $doctorss->affiliate_name,
                    'state'=>  $doctorss->state,
                    'total_price' => $doctorss->total_price,
                    'type' => $doctorss->type,
                    'created_at' =>  $doctorss->created_at,
                );
                $count++;
            }
            
            Excel::create('Academy data',function($excel) use($doctor_array){
                $excel->setTitle('Academy data');
                $excel->sheet('Academy data',function($sheet) use($doctor_array){
                    $sheet->fromArray($doctor_array , null, 'A1', false, false);
                });
            })->download('xls');
        }else{
            echo "<script>alert('من فضلك اختار التاريخ من و الى ');"
                              . "window.location.replace('/show_visits');"
                              . "</script>";
        }
    }
    
    
    
    public function contact_excel(Request $request){
        $count = 0;
        $fromDate = $request->input('fromDate');
        $toDate = $request->input('toDate');

        if( $request->has('fromDate')  &&  $request->has('toDate')){

            $get_data = \App\contact_us::select('id','country','email','message','created_at')
                                        ->whereDate('created_at' ,'>=' , $fromDate)
                                        ->whereDate('created_at' ,'<=' , $toDate)->get();
     
            $contacts[] = array('id','country','email','message','created_at');
            
            foreach ($get_data as $contactData){
                $contacts[] = array(
                    'id' =>  $contactData->id,
                    'country' => $contactData->country,
                    'email' => $contactData->email,
                    'message' => $contactData->message,
                    'created_at' =>  $contactData->created_at,
                );
                $count++;
            }
            
            Excel::create('Academy contact data',function($excel) use($contacts){
                $excel->setTitle('Academy contact data');
                $excel->sheet('Academy contact data',function($sheet) use($contacts){
                    $sheet->fromArray($contacts , null, 'A1', false, false);
                });
            })->download('xls');
        }else{
            echo "<script>alert('من فضلك اختار التاريخ من و الى ');"
                         . "window.location.replace('/show_contactPage');"
                        . "</script>";
        }
        
    }
    
    
    public function show_terms_link(){
        try{
            
            $get_terms = \App\About::where('id','1')->value('terms_condition');
            
            if( $get_terms != NULL ){
                $message['data'] = $get_terms;
                $message['error'] = 0;
                $message['message'] = "this is the data";
            }else{
                $message['data'] = $get_terms;
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";    
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    
    
    // dashboard ::
    
    
    public function show_callInfo(){
        try{
            
            $get_data = \App\call_info::select('id','address','phone','whatsapp','email','created_at')->first();
            
            if($get_data != NULL){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is the data of call info";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }




    public function update_callInfo(Request $request){
        try{
            
            $updated_at = carbon::now()->toDateTimeString();
		    $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($updated_at)));
           
            
            $update_data = \App\call_info::where('id','1')
                                        ->update([
                                            "address" => $request->input('address'),
                                            "phone" => $request->input('phone'),
                                            "whatsapp" => $request->input('whatsapp'),
                                            "email" => $request->input('email'),
                                            "updated_at" => $dateTime
                                            ]);
                                            
                                            
            if( $update_data == true){
                $message['error'] = 0;
                $message['message'] = "the data is updated successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    // webcontent ::
    
    
    public function show_webContent(Request $request){
        try{
            
            $get_data = \App\web_content::select('id', 'small_title','big_title','content','socialForm_title','service_title')->first();
            
            if($get_data != NULL){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is the web content data";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no data, please try again";
            }
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    public function update_webContent(Request $request){
        try{
            $updated_at = carbon::now()->toDateTimeString();
		    $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($updated_at)));
           
            
            $update = \App\web_content::where('id','1')
                                    ->update([
                                        "small_title" => $request->input('small_title'),
                                        "big_title" => $request->input('big_title'),
                                        "content" => $request->input('content'),
                                        "socialForm_title" => $request->input('socialForm_title'),
                                        "service_title" => $request->input('service_title'),
                                        "updated_at" => $dateTime

                
                                       ]);
                                       
                                       
            if($update == true){
                $message['error'] = 0;
                $message['message'] = "the data is updated successfully";
            }else{
                $message['error']= 1;
                $message['message']  = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    // vedio link :: 
    
    public function show_links(){
        try{
            
            $get_data = \App\video_link::select('id','link','logo')->first();
            
            if( $get_data  != NULL){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is the links data";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
      public function update_links(Request $request){
        try{
             
            $updated_at = carbon::now()->toDateTimeString();
		    $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($updated_at)));
           
            $get_image = \App\video_link::where('id','1')->value('logo');
           
              $logo = $request->file('logo');
        
              if(isset($logo)){
                    $new_name = $logo->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads';
                    $logo->move($destinationPath_id, $savedFileName);
    
                    $logos = $savedFileName;
                }else{
                    if($get_image != NULL){
                        $logos = $get_image;
                    }else{
                       $logos = NULL;
                    }
                }
            $get_data = \App\video_link::where('id','1')
                                        ->update([
                                            "link" => $request->input('link'),
                                            "logo" => $logos,
                                            "updated_at" => $dateTime
                                            ]);
            
            if( $get_data  == true){
                $message['error'] = 0;
                $message['message'] = "data is updated successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    
    
    
    public function show_colors(){
        try{
            
            $get_data = \App\video_link::select('id','first_color','second_color')->first();
            
            if( $get_data  != NULL){
                $message['first_color'] = $get_data->first_color;      
                $message['second_color'] = $get_data->second_color;
                $message['error'] = 0;
                $message['message'] = "this is the colors data";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
      public function update_colors(Request $request){
        try{
             
            $updated_at = carbon::now()->toDateTimeString();
		    $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($updated_at)));
           
            
            $get_data = \App\video_link::where('id','1')
                                        ->update([
                                            "first_color" => $request->input('first_color'),
                                            "second_color" => $request->input('second_color'),
                                            "updated_at" => $dateTime
                                            ]);
            
            if( $get_data  == true){
                $message['error'] = 0;
                $message['message'] = "data is updated successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    
    // academy contact ::
    
    
    public function show_academyContact(Request $request){
        try{
            
            $get_data = \App\doctor_contact::select('doctor_contact.id', 'doctor_contact.name','doctor_contact.phone','doctor_contact.service_id','doctor_contact.affiliate_id', 'affiliate.first_name','affiliate.last_name', 'doctor_contact.state','doctor_contact.total_price','doctor_contact.type','doctor_contact.created_at')
                                           ->leftjoin('affiliate' , 'doctor_contact.affiliate_id', '=', 'affiliate.id')->get();
           
           
           if( count($get_data) >0){
               $message['data'] = $get_data;
               $message['error'] = 0;
               $message['message'] = "this is all academy contact";
           }else{
               $message['data'] = $get_data;
               $message['error'] = 1;
               $message['message'] = "there is an error, please try again";
           }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    public function update_contactStatus(Request $request){
        try{
            
            $contact_id = $request->input('contact_id');
            
            
            
              $select_service = \App\doctor_contact::select('service_id','affiliate_id')->where('id' , $contact_id)->get();

         $get_price = array();
        $g = 0; 
        $total=0;
        $affiliate_money=0;
        
        if( $select_service  != NULL){
            $affiliateID = $select_service[0]->affiliate_id;
              
            if($affiliateID == NULL){
                
            }else{
                
                $get_level = \App\affiliate::where('id', $select_service[0]->affiliate_id)->value('stage');

            
            
            
                foreach($select_service as $serve){
    
                    foreach (explode(', ', $serve->service_id) as $serv){
    
                        array_push($get_price, \App\Service::select('price', 'level1','level2', 'level3')
                                                              ->where('name' , $serv)->get());
    
                        $total += $get_price[$g][0]->price ;
    
                        if( $get_level == 1){
                            $calculate_present = $get_price[$g][0]->level1 ;    
                            $affiliate_money += $calculate_present ;
    
                        }else if($get_level == 2){
                            $calculate_present = $get_price[$g][0]->level2 ;    
                            $affiliate_money += $calculate_present ;
    
                        }else{
                            $calculate_present = $get_price[$g][0]->level3 ;    
                            $affiliate_money += $calculate_present ;
    
                        $g++;
                    }
                    }
                }
            
    
          
                
                $get_balance = \App\affiliate::where('id', $affiliateID)->value('balance');
                
                $all_balance = $get_balance + $affiliate_money;
                
                 $update_affiliate = \App\affiliate::where('id',$affiliateID)->update(['balance'=>$all_balance]);
      
            }
        }
            
            $update = \App\doctor_contact::where('id',$contact_id)->update(['state' => "done"]);
            
            if( $update == true){
                $message['error']  = 0;
                $message['message'] = "the status is accepted successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    public function show_affiliate_Data(Request $request){
        try{
            
            $affiliate_id = $request->input('affiliate_id');
            
            
            $affiliateData= \App\affiliate::select('id','first_name','last_name','image','email','phone','balance', DB::raw('(select count(id) from affiliate where affiliate_id= '.$affiliate_id.') as  affiliate_num'))
                                        ->where('id', $affiliate_id)->first();
                                        
                                        
            $get_affiliate_contacts = \App\doctor_contact::select('doctor_contact.id', 'doctor_contact.name','doctor_contact.phone','doctor_contact.service_id', 'doctor_contact.state','doctor_contact.total_price','doctor_contact.type','doctor_contact.created_at')
                                                        ->leftjoin('affiliate' , 'doctor_contact.affiliate_id', '=', 'affiliate.id')
                                                        ->where('doctor_contact.affiliate_id' , $affiliate_id)->get();
                                        
            
            if( $affiliateData  != NULL){
                $message['data'] = $affiliateData;
                $message['affiliate_contacts'] =$get_affiliate_contacts;
                $message['error'] = 0;
                $message['message'] = "this is the data of that affiliate";
            }else{
                $message['data'] = $affiliateData;
                $message['affiliate_contacts'] =$get_affiliate_contacts;
                $message['error'] = 1;
                $message['message'] = "there is no data, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    public function whatsContact_excel(Request $request){
            
        $count = 0;
      
        $fromDate = $request->input('fromDate');
        $toDate = $request->input('toDate');
        
        if( $request->has('fromDate') && $request->input('toDate')){
        
            $get_data = \App\Whatsapp_user::select('whatsapp_user.id','name',DB::raw('(concat(affiliate.first_name," ",affiliate.last_name)) as affiliate_name'),'whatsapp_user.created_at')
                                          ->leftjoin("affiliate" , "whatsapp_user.affiliate_id", "=", "affiliate.id")
                                          ->whereDate('whatsapp_user.created_at' ,'>=' , $fromDate)
                                          ->whereDate('whatsapp_user.created_at' ,'<=' , $toDate)->get();
    
            
            $contacts[] = array('id','name','affiliate_name','created_at');
            
            foreach ($get_data as $whatscontacts){
                $contacts[] = array(
                    'id' =>  $whatscontacts->id,
                    'country' => $whatscontacts->name,
                    'email' => $whatscontacts->affiliate_name,
                    'created_at' =>  $whatscontacts->created_at,
                );
                $count++;
            }
            
            Excel::create('whatsapp contact data',function($excel) use($contacts){
                $excel->setTitle('whatsapp contact data');
                $excel->sheet('whatsapp contact data',function($sheet) use($contacts){
                    $sheet->fromArray($contacts , null, 'A1', false, false);
                });
            })->download('xls');
            
         }else{
            echo "<script>alert('من فضلك اختار التاريخ من و الى ');"
                              . "window.location.replace('/show_whatsContact');"
                              . "</script>";
        }
    }
    
    
    
    
    
    
    public function add_whatsappUser(Request $request ){
        
        if (Cookie::get('affiliate') != NULL){   
        
            $affiliate_id = Cookie::get('affiliate');                     
            
        }else{
               $affiliate_id = NULL;
        }


        $username = $request->input('username');

        $updated_at = carbon::now()->toDateTimeString();
        $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($updated_at)));
       
        
        $add_user = new \App\Whatsapp_user;

        $add_user->name = $username;
        $add_user->affiliate_id = $affiliate_id;
        $add_user->created_at = $dateTime;
        $add_user->updated_at = $dateTime;
        $add_user->save();


        $whats_phone = \App\call_info::where('id','1')->value('whatsapp');

        if( $add_user == true){
         
               $url="https://api.whatsapp.com/send?phone=".$whats_phone."&&text=%d9%85%d8%b1%d8%ad%d8%a8%d8%a7%2c%20%d9%86%d8%b1%d9%8a%d8%af%20%d9%85%d8%b9%d8%b1%d9%81%d8%a9%20%d8%a7%d9%84%d8%ae%d8%af%d9%85%d8%a7%d8%aa%20%d9%88%d8%a7%d9%84%d8%aa%d9%83%d8%a7%d9%84%d9%8a%d9%81%20%d8%a7%d9%84%d8%ad%d8%a7%d9%84%d9%8a%d8%a9";
            
                 //  "https://api.whatsapp.com/send?phone={{$whats_phone}}&amp;&amp;text=%d9%85%d8%b1%d8%ad%d8%a8%d8%a7%2c%20%d9%86%d8%b1%d9%8a%d8%af%20%d9%85%d8%b9%d8%b1%d9%81%d8%a9%20%d8%a7%d9%84%d8%ae%d8%af%d9%85%d8%a7%d8%aa%20%d9%88%d8%a7%d9%84%d8%aa%d9%83%d8%a7%d9%84%d9%8a%d9%81%20%d8%a7%d9%84%d8%ad%d8%a7%d9%84%d9%8a%d8%a9";
            
            echo '<script>'
                    . 'window.location.href = "'.$url.'";'
                    . '</script>';
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                     . "window.location.replace('/');"
                    . "</script>";
        }
    }



    public function show_whatsUser_page(){
        return view('show_whatUser');
    }

    public function show_whatsUser_data(){

        $get_data = \App\Whatsapp_user::select('whatsapp_user.id','name','affiliate.first_name','affiliate.last_name','whatsapp_user.created_at')
                                      ->leftjoin("affiliate" , "whatsapp_user.affiliate_id", "=", "affiliate.id")
                                      ->get();

        return view("show_whatUser",["get_data" => $get_data]);
    }
    
    
    
    public function show_whatsaffiliate_data(Request $request){
        try{
            $affiliate_id = $request->input('affiliate_id');
            
            $get_data = \App\Whatsapp_user::select('id','name','created_at')
                                      ->where('affiliate_id', $affiliate_id)->get();
                                      
          
            if( count($get_data )>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is all the whats app contacts";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no data";
            }

            
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }
               return response()->json($message);

    }

    
}