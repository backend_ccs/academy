
<html lang="en"  > 
    
 <head>
     
         
    <meta charset="utf-8">
    <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, shrink-to-fit=no">
     <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Qta.edu.sa">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <!--<link rel="stylesheet" href="css/fontawesome.min.css"/>-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

     <link href="https://fonts.googleapis.com/css?family=Amiri" rel="stylesheet">
      <link rel="stylesheet" href="bower_components/aos/dist/aos.css" />
     <link href="css/aos.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/style_index1.css"/>  

    

     <style>
         
             :root
         {
             --main:<?php
                    
                        $get_logo = DB::select("SELECT * FROM `video_link`");
                    echo $get_logo[0]->first_color;
                    ?>;
                    
             --second:<?php
            
            echo $get_logo[0]->second_color;
            ?>;
            
           
         }
        
        
         
     
         
         
     </style>
  </head>
    
                                <?php 
                                
                                $get_phone = DB::select("select *  from call_info");
                                   
                                  
                                
                                ?>
    <body>
      <!-- logo section -->
       <section class="logo-section hidden-xs roboto">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-3">
                <a href="">
                    <?php
                    
                        $get_logo = DB::select("SELECT logo FROM `video_link`");
                        $socail = DB::select("select * from social_media where id=1");
                        $get_map = DB::select("select * from social_media where id=1");

                    ?>
                    <img src="/uploads/{{$get_logo[0]->logo}}" alt="" class="img-responsive logo " style=" height: 150px;width: 150px;">
                </a>
            </div>
            <div class="col-md-8 col-sm-9" style="text-align:right;position: relative; margin-top:40px;" >
                <div class="contact-list">
                     <div class="icon">
                            <a href="http://maps.google.com/maps?q={{$get_map[0]->Latitude}},{{$get_map[0]->Longitude}}&amp;ll={{$get_map[0]->Latitude}},{{$get_map[0]->Longitude}}&amp;z=14" target="_blank">
                                <i class="fas fa-map-marker-alt"></i>
                            </a>
                    </div>
                    <div class="icon">
                         <a href="{{$socail[0]->snapchat}}" target="_blank">
                                <i class="fab fa-snapchat-ghost"></i>
                          </a>
                    </div>
                    
                    <div class="icon">
                     <a href=""  data-toggle="modal" data-target="#whatsUser_model" >
                            <i class="fab fa-whatsapp" style="font-size: 29px"></i>
                        </a>
                    </div>
                    
<!--                    <div class="icon">
                        <a data-toggle="modal" data-target="#exampleModal" href="#">
                            <i class="fas fa-edit sicon" style="font-size: 24px"></i>
                        </a>
                    </div>-->
                    <div class="icon">
                        <a href="mailto:{{$getphone[0]->email}}" target="_blank">
                            <i class="fas fa-envelope" style="font-size: 27px"></i>
                        </a>
                    </div>
                    <div class="call">
                        <p>Reach Us By Call !</p>
                        <a href="#">
                            <p>  
                                {{$get_phone[0]->phone}}
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
        
        
        
        <!--end logo section -->
        <!-- small contact bar -->
        <div class="small-bar ">
    <div class="container">
        <table class="table table-borderless ">
              <?php
                    $getphone = DB::select("select *  from call_info");
                ?>
            <tbody>
                <tr class="text-center">
                    <td>
                         <a href="http://maps.google.com/maps?q=41.061634,28.989238&amp;ll=41.061634,28.989238&amp;z=14" target="_blank">
                                <i class="fas fa-map-marker-alt"></i>
                        </a>
                    </td>
                    <td>
                       <a href="https://www.snapchat.com/add/qta_edu" target="_blank">
                            <i class="fab fa-snapchat-ghost"></i>
                       </a>
                    </td>
                    <td>
                       <a href="tel://{{$getphone[0]->phone}}" target="_blank" onclick="return gtag_report_conversion('tel://{{$getphone[0]->whatsapp}}');">
                            <i class="fas fa-phone" style="font-size: 17px;"></i>
                        </a>
                    </td>
                    <td>
                         <a href="" data-toggle="modal" data-target="#whatsUser_model" >
                            <i class="fab fa-whatsapp"></i>
                        </a>
                    </td>
                    
<!--                    <td>
                        <a data-toggle="modal" data-target="#exampleModal" href="#">
                            <i class="far fa-edit" style="font-size: 17px;"></i>
                        </a>
                    </td>-->
                    <td>
                        <a href="mailto:{{$getphone[0]->email}}" target="_blank">
                            <i class="fas fa-envelope" style="font-size: 17px"></i>
                        </a>

                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
        <div class="logo-div ">
    <a href="" style="display: contents;"><img src="/uploads/{{$get_logo[0]->logo}}" alt="clinicexpert" class="slogo"></a>
</div>
        
        @yield('content')
        
        
    <!--responsive-->
        
        <!--end of responsive-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
          
       <script src="https://www.paypal.com/sdk/js?client-id=sb"></script>

        <script src="js/jquery-3.3.1.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
       <script src="dist/js/BsMultiSelect.js"></script>
       <script src="js/aos.js"></script>
        <script src="bower_components/aos/dist/aos.js"></script>
        <script src="js/mine1.js"></script>
        <script>
  AOS.init();
</script>
    </body>
</html>