@extends('layouts.admin_const')

@section('content')

<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12" style="float: right;margin-left: auto;">
                        <h2 style="font-size: xx-large;"> :تعديل وسائل التواصل  </h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">تعديل وسائل التواصل</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            
            
             <div class="body">
                 <form id="basic-form" method="post"action="/update_social" novalidate enctype="multipart/form-data" class="text-right">
                                           {{ csrf_field() }}

                        @foreach($Socail_data as $socail)
                        
                        <input type="hidden" name="socail_id" value="{{$socail->id}}">

                        <div class="form-group">
                            <label style="    font-size: x-large;">Facebook</label>
                            <input type="text" name='facebook' value="{{$socail->facebook}}" class="form-control text-right" required>
                        </div>
                        
                        <div class="form-group">
                            <label style="    font-size: x-large;">Instagram</label>
                            <input type="text" name='instagram' value="{{$socail->instagram}}" class="form-control text-right" required>
                        </div>
                        
                         <div class="form-group">
                            <label style="    font-size: x-large;">Twitter</label>
                            <input type="text" name='twitter' value="{{$socail->twitter}}" class="form-control text-right" required>
                        </div>
                         <div class="form-group">
                            <label style="    font-size: x-large;">Youtube</label>
                            <input type="text" name='youtube' value="{{$socail->youtube}}" class="form-control text-right" required>
                        </div>
                         <div class="form-group">
                            <label style="    font-size: x-large;">Snapchat</label>
                            <input type="text" name='snapchat' value="{{$socail->snapchat}}" class="form-control text-right" required>
                        </div>
                         <div class="form-group">
                            <label style="    font-size: x-large;">خط العرض </label>
                            <input type="text" name='Latitude' value="{{$socail->Latitude}}" class="form-control text-right" required>
                        </div>
                         <div class="form-group">
                            <label style="    font-size: x-large;">خط الطول</label>
                            <input type="text" name='Longitude' value="{{$socail->Longitude}}" class="form-control text-right" required>
                        </div>
                  
                        
                        
                        
                          
                        <br>    
                        <button type="submit" name="edit_socail"class="btn btn-primary" style="margin-right: 556px;font-size: 22px;">حفظ</button>
                          @endforeach
                    </form>
             </div>
            
            
        </div>
</div>

@endsection