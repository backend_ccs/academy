@extends('layouts.admin_const')

@section('content')

<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12" style="float: right;margin-left: auto;">
                        <h2 style="font-size: xx-large;"> :تعديل لينك الفديوا </h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">تعديل  لينك الفديوا </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            
            
             <div class="body ">
                 <form id="basic-form" method="post"action="/update_video" novalidate enctype="multipart/form-data" class="text-right">
                                           {{ csrf_field() }}

                        @foreach($get_videodata as $link)
                        
                        <input type="hidden" name="video_id" value="{{$link->id}}">

                        <div class="form-group text-right">
                            <label  style="    font-size: x-large;">لينك الفديوا </label>
                            <input type="text" name='link' value="{{$link->link}}" class="form-control text-right" required>

                        </div>
                        
                          <div class="form-group  text-right ">
                            <label  style="    font-size: x-large;">لوجو</label><br>
                           <?php
                           $logo = $link->logo;
                                                
                                if( $logo == ''){

                                }else{?>
                                         &nbsp;  &nbsp;<img src="uploads/{{$link->logo}}" width="280" height="100"><br><br><br>
                                <?php } ?>
                           
                            <input type="file" name="logo"  class="form-control text-right" required>
                        </div>
                                           
                          
                        <br>    
                        <button type="submit" name="edit_about"class="btn btn-primary" style="margin-right: 556px;font-size: 22px;">حفظ</button>
                          @endforeach
                    </form>
             </div>
            
            
        </div>
</div>

@endsection