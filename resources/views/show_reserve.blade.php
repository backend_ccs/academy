@extends('layouts.admin_const')

@section('content')



<div id="wrapper">


    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                       <!--<h2>Jquery Datatable</h2>-->
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item">  الحجوزات الخارجية </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                             
                            <ul class="nav nav-tabs-new">
                                <!-- <form id="basic-form" method="get"action="{{ route('affiliate_visits.excel')}}" novalidate enctype="multipart/form-data"  class="text-right">-->
                                <!--           {{ csrf_field() }}-->

                                <!--            <div class="form-group " style="float: right; margin-left: 40px;">-->
                                <!--                <label style="    font-size: x-large; margin-right: -60px;">من </label>-->
                                <!--                <input type="date" name='fromDate' class="form-control text-right" style="margin-top: -30px;" required>-->
                                            
                                <!--                <label style="    font-size: x-large; margin-right: -60px;">الى </label>-->
                                <!--                <input type="date" name='toDate' class="form-control text-right" style="margin-top: -30px;" required>-->
                                <!--            </div> -->
                
                                <!--    <li style="float: right;margin-left: auto;">-->
                                <!--    <button class="btn btn-outline-success " style="margin-top: 30px;"> Export to Excel</button></li>-->


                                <!--</form>-->
                                <li style="float: right;margin-left: auto;"><h2 style="font-size: xx-large;">: جميع الزيارات الخارجية</h2> </li> &nbsp;&nbsp;
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover js-basic-example dataTable table-custom text-center">
                                    
                                       <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th> الاسم</th>
                                            <th>رقم الهاف </th>
                                            <th>الخدمة</th>
                                            <th>السعر</th>
                                            <th>تابع الى مسوق</th> 
                                            <th>الحاله</th>
                                            <th>طريقه الدفع</th>
                                            <th>وقت التسجيل</th>
                                            
                                            
                                            <!--<th>Edit</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($get_data as $data)
                                        <tr>
                                            <td>{{$data->id}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->phone}}</td>
                                            <td>{{$data->services}}</td>
                                            <td>{{$data->total_price}}</td>
                                            
                                             <?php

                                                    if($data->affiliate_id == NULL){
                                                        ?><td></td>

                                                  <?php  }else{
                                                     ?>
                                                        <td>
                                                             <form action="/show_affiliatePage" method="GET"  id="sub_{{$data->affiliate_id}}" enctype="multipart/form-data">
                                   
                                                            {{ csrf_field() }}

                                                            <input type="hidden" name="user_id" value="{{$data->affiliate_id}}">

                                                            <a  onclick="$('#sub_{{$data->affiliate_id }}').submit();">

                                                                 <button class="btn  ">
                                                                
                                                           {{$data->first_name}}  {{$data->last_name}}
                                                                </button>
                                                                
                                                                  </a>      
                           
                                                            </form>
                                                     </td>
                                                        
                                               <?php  }
                                                ?>
                                            <td>{{$data->state}}</td>
                                            <td>{{$data->type}}</td>
                                            <td>{{$data->created_at}}</td>
                                             
                                        </tr>
                                      @endforeach                                    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>            
        </div>       
    </div>
</div>

         



@endsection