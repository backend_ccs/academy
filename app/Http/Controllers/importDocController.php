<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use carbon\carbon;

class importDocController extends Controller
{
    public $message = array();
    
    
    public function show_docs(){
        return view("admin_document");
    }
    
    public function show_doc_data(){
        
        $get_data = \App\import_doc::select('id','content','version','created_at')->where('id',1)->get();
        
        return view("admin_document" , ['get_data' => $get_data]);
    }
    
    
    public function show_edit_page(){
   
        return view('admin_editDoc');
    }
    
     public function show_docData(Request $request){
        
        $doc_id = $request->input('doc_id');
        
        $get_data = \App\import_doc::select('id','content','version','created_at')->where('id',$doc_id)->get();
        
        return view("admin_editDoc" , ['get_data' => $get_data]);
    }
    
    public function update_doc(Request $request){
        
        $get_version = \App\import_doc::where('id','1')->value('version');
        
        $update = \App\import_doc::where('id','1')
                                 ->update([
                                        "content" => $request->input('content'),
                                        "version" =>$get_version + 1
                                          ]);
                                          
        if($update == true){
            echo "<script>alert('تم تعديل المستندات بنجاح');"
                    . "window.location.replace('show_docs')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ ,حاول مرة اخرى');"
                    . "window.location.replace('show_edit_page')"
                    . "</script>";
        }
        
                                          
                                          
    }
    
    
    //********************* apis ::
    
    public function version_Read(Request $request){
        try{

            $user_id = $request->input('user_id');
            
            $get_version = \App\import_doc::where('id','1')->value('version');
            
            $update = \App\affiliate::where('id', $user_id)->update(['version_seen' =>$get_version]);
            
            
            if( $update == true){
                $message['error'] = 0;
                $message['message'] = "this user is read the last version corectly";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }
          return response()->json($message);

    }
    
    
    public function show_documents(){
        try{
            $get_data = \App\import_doc::where('id','1')->value('content');
            
            if( $get_data != NULL){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is the important document";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no data, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }
          return response()->json($message);

    }
    
    
    // dashboard ::
    
    
    public function show_document_data(Request $request){
        try{
            
            $get_data = \App\import_doc::select('id','content','version','updated_at')->first();
            
            
            if( count($get_data ) > 0){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is the important documents";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no important documents";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }
          return response()->json($message);
    }
    
    public function update_importantDocuments(Request $request){
        try{
             
             $get_version = \App\import_doc::where('id','1')->value('version');
        
            $update = \App\import_doc::where('id','1')
                                     ->update([
                                            "content" => $request->input('content'),
                                            "version" =>$get_version + 1
                                              ]);
                                              
            if($update == true){
                $message['error'] = 0;
                $message['message'] = "the documnt is updated successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
        
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }
          return response()->json($message);
    }
    
}