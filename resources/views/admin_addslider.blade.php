@extends('layouts.admin_const')

@section('content')

<div class="wrapper">
    
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12"style="float: right;margin-left: auto;">
                        <h2 style="font-size: xx-large;" > اضافة صور الخدمات</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">اضافة  صور الخدمات </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            
             <div class="body">
                 <form id="basic-form" method="post"action="/add_slider" novalidate enctype="multipart/form-data"  class="text-right">
                                           {{ csrf_field() }}

                        

                        
                                           
                        <div class="form-group">
                            <label style="    font-size: x-large;"> صورة الخدمات </label>
                           <input type="file" name="image"  class="form-control" required>

                        </div>
                        
                        
                            
                        <br>    
                        <button type="submit" name="Add_review"class="btn btn-primary" style="margin-right: 556px;font-size: 22px;">حفظ</button>
                          
                    </form>
             </div>
            
            
        </div>
</div>
</div>

@endsection