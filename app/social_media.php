<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class social_media extends Authenticatable
{
    public $table = 'social_media';
}