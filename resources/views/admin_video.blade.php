@extends('layouts.admin_const')

@section('content')



<div id="wrapper">

    

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                       <!--<h2>Jquery Datatable</h2>-->
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item">لينك الفيديوا   </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                             
                            <ul class="nav nav-tabs-new">
<!--                                <li class="nav-item" style="margin-top: -5px;margin-left: 7px;font-size: x-large;margin-left: auto;"><a class="nav-link"  href="/show_addreview">اضافة اراء العملاء</a></li>-->
                                <li style="float: right;margin-left: auto;"><h2 style="font-size: xx-large;">:    لينك الفديوا </li>
                        </ul> </h2> </li> &nbsp;&nbsp;

                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover js-basic-example dataTable table-custom text-center text-center">
                                    
                                       <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>لينك الفديو</th>
                                            <th> اللوجو</th>

                                            <th>تعديل</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($get_data as $link)
                                        <tr>
                                            <td>{{$link->id}}</td>
                                            <td>{{$link->link}}</td> 
                                            <td>
                                              <?php  $logo = $link->logo;
                                                
                                                if( $logo == ''){
                                                
                                                }else{?>
                                                <img src="/uploads/{{$link->logo}}" width="290" height="85">
                                                <?php } ?>
                                            </td>  
                                            
                                             
                                            <td>
                                           
                                                  <form action="/edit_video" method="get" id='sub_{{$link->id}}'  enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="video_id" value="{{$link->id}}">
                                                    <a onclick="$('#sub_{{$link->id}}').submit();">
                                                        <button class="btn btn-sm btn-icon btn-pure btn-default on-default m-r-5 button-edit" data-toggle="tooltip" data-original-title="تعديل">
                                                        <i class="icon-pencil" aria-hidden="true"></i>
                                                        </button>                                      
                                                    </a>
                                                </form>

                                            </td>
                                           
                                        </tr>
                                      @endforeach                                    

                                    </tbody>
                                  
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>            
        </div>       
    </div>
</div>

         



@endsection