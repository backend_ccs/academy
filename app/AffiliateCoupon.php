<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AffiliateCoupon extends Model
{
    public $table = 'affiliate_coupon';
}
