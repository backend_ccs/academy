@extends('layouts.admin_const')

@section('content')


<div id="wrapper">
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>بيانات المسوق</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item active">بيانات المسوق</li>
                        </ul>
                    </div>
                </div>
            </div>

            
            
            
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="body testimonial-single">
                            <div class="text-center">
                                <div class="testimonial4">
                                    <div class="item">
                                        
                                        @foreach($get_data as $data)
                                        
                                       <?php $count_affiliators = \App\affiliate::where('affiliate_id',$data->id)->count(); ?>
                                     
                                        <img class="rounded-circle" src="uploads\affiliates\{{$data->image}}" alt="">
                                        <div class="name">
                                            <h6>{{$data->first_name}} {{$data->last_name}} <label>:الاسم   </label></h6> 
                                            <span>{{$data->email}} <label>:البريد الالكترونى   </label></span>  <br>
                                            <span>{{$data->phone}} <label>:تليفون   </label></span> <br>
                                            <span>{{$data->balance}} <label>: الرصيد</label></span><br>
                                        
                                                <form action="/follow_affiliate" method="GET"  id="sub_{{$data->id}}" enctype="multipart/form-data">
                                   
                                                        {{ csrf_field() }}

                                                        <input type="hidden" name="follow_id" value="{{$data->id}}">

                                                        <a  onclick="$('#sub_{{$data->id}}').submit();" >

                                                        <span style="cursor: pointer;     border: #2eb7d5ad; border-style: groove; padding: 3px;">
                                                            {{$count_affiliators}} <label style="cursor: pointer;">:  عدد المسوقين التابعين له</label>
                                                        </span>

                                                        </a>      

                                               </form>

                                        </div>
                                        
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
          
            
            
            
            
            
            
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                             
                            <ul class="nav nav-tabs-new">
                                 <form id="basic-form" method="get"action="{{ route('show_affiliateContact.excel')}}" novalidate enctype="multipart/form-data"  class="text-right">
                                           {{ csrf_field() }}

                                            <div class="form-group " style="float: right; margin-left: 40px;">
                                                <label style="    font-size: x-large; margin-right: -60px;">من </label>
                                                <input type="date" name='fromDate' class="form-control text-right" style="margin-top: -30px;" required>
                                            
                                                <label style="    font-size: x-large; margin-right: -60px;">الى </label>
                                                <input type="date" name='toDate' class="form-control text-right" style="margin-top: -30px;" required>
                                            </div> 
                
                                    <li style="float: right;margin-left: auto;">
                                    <button class="btn btn-outline-success " style="margin-top: 30px;"> Export to Excel</button></li>


                                </form>
                                <li style="float: right;margin-left: auto;"><h2 style="font-size: xx-large;"> : تواصل مع المسوق</h2> </li> &nbsp;&nbsp;

                            </ul>

                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover js-basic-example dataTable table-custom text-center">
                                    
                                       <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>الاسم بالكامل</th>
                                            <th> الهاتف</th>
                                            <th>اسم الخدمة</th>
                                            <th>الحالة</th>
                                            <th>التكلفة</th>
                                            <th>طريقه الدفع</th>
                                            <th>وقت الانشاء</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($get_contact as $doctor)
                                       
                                     
                                        <tr>
                                            <td>{{$doctor->id}}</td>
                                            <td>{{$doctor->name}}</td>
                                            <td>{{$doctor->phone}}</td>
                                                                                        
                                            <td>{{$doctor->service_id}}</td>
                                            <td>
                                               
                                          
                                          
                                             
                                                       <?php
                                                        if($doctor->state == "wait"){
                                                          ?>
                                                        <form action="/update_state" method="GET"  id="sub_{{$doctor->id}}" enctype="multipart/form-data">
                                   
                                                            {{ csrf_field() }}

                                                            <input type="hidden" name="doctor_id" value="{{$doctor->id}}">

                                                            <a  onclick="$('#sub_{{$doctor->id}}').submit();">

                                                           <button class="btn btn-sm btn-icon btn-pure btn-default " data-toggle="tooltip" data-original-title="تاكيد الحجز">
                                                                    تاكيد الحجز
                                                            </button>   
                                                                
                                                                  </a>      
                           
                                                   </form>
                                                

                                                    <?php }else{
                                                           ?>
                                                       <span>تم التاكيد</span>
                                                               <?php
                                                       }
                                                       ?>
                                                    
                                                 
                                                
                                            </td>  
                                             <td>{{$doctor->total_price}}</td>
                                            <td>{{$doctor->type}}</td>
                                            
                                            <td>{{$doctor->created_at}}</td>
                                                                                       
                                           
                                        </tr>
                                      @endforeach                                    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
            
            
            
        </div>
 </div>
     
     
@endsection