@extends('layouts.admin_const')

@section('content')



<div id="wrapper">

    

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                       <!--<h2>Jquery Datatable</h2>-->
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item">  المراحل</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                             
                            <ul class="nav nav-tabs-new">
                                <li style="float: right;margin-left: auto;"><h2 style="font-size: xx-large;">: المرحلة</li>
                        </ul> </h2> </li> &nbsp;&nbsp;

                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover js-basic-example dataTable table-custom text-center">
                                    
                                       <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>المرحلة </th>
                                            <th>من </th>
                                            <th>الى </th>
                                            <th>وقت الانشاء </th>
                                           
                                            <th>تعديل</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($get_data as $stage)
                                        <tr>
                                            <td>{{$stage->id}}</td>
                                            <td>{{$stage->name}}</td>  
                                            <td>{{$stage->from_num}}</td>
                                            <td>{{$stage->to_num}}</td>
                                            <td>{{$stage->created_at}}</td>
                                           
                                           <td>
                                               
                                          
                                               <form action="/show_editStage" method="get" id='sub_{{$stage->id}}'  enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="stage_id" value="{{$stage->id}}">
                                                    <a onclick="$('#sub_{{$stage->id}}').submit();">
                                                        <button class="btn btn-sm btn-icon btn-pure btn-default on-default m-r-5 button-edit" data-toggle="tooltip" data-original-title="تعديل">
                                                        <i class="icon-pencil" aria-hidden="true"></i>
                                                        </button>                                      
                                                    </a>
                                                </form>
                                                
                                                
                                            </td> 
                                           
                                        </tr>
                                      @endforeach                                    

                                    </tbody>
                                  
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>            
        </div>       
    </div>
</div>

         



@endsection