<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>اشترك في الدورة</title>
    

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<!-- <link rel="stylesheet" type="text/css" href="style.css"> -->


<style>
    
 @import url('https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;700;900&family=Tajawal:wght@200;300;400;500;700;800;900&display=swap');
 
 
 body{
    font-family: 'Cairo', sans-serif;
}

*, ::after, ::before {
    box-sizing: unset;
    font-family: 'Cairo', sans-serif;
}

.con{
    margin: 0 -15px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    -ms-flex-direction: row;
    flex-direction: row;
    -ms-flex-flow: row wrap;
    flex-flow: row wrap;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    
}





.payment{
    justify-content: center;
    width: 80px;
    height: 80px;
    margin: 0 15px 30px 15px;
    border: 1px #cccccc solid;
    border-radius: 3px;
    padding: 10px;

    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-pack: center;
    -ms-flex-pack: center;

}

img{
    width: 100%;
    height: auto;
}


  @media (min-width: 768px){
  .container, .container-md, .container-sm {
      max-width: 670px;
  }}



  
@media screen and (max-width: 497px)  {
   .payment {
    width: 65px;
    height: 65px;
    }
  } 

</style>

</head>
<body>
    


<div class="container" style="width: 94%;">

              <div class="card-header" style="width: 90%;  background-color:unset; border-bottom: unset; ">

                <div class="col-md-12 text-center">
                <form dir="rtl" action="/doctorContact" method="post" >
                                                {{ csrf_field() }}

                    <div style=" text-align: center;">
                        <h1  id="head" class="text-center padding-bottom-2x" style="border-bottom: 1px solid rgb(238, 238, 238); padding-top: 48px !important;     padding-bottom: 48px !important; ">الاشتراك في الدورة</h1>
                   </div>
                    <div class="form-body" style="margin-top: 30px;">


                        <div class="row ">

                        
                        
                
                            <div class="form-group  col-md-5 mb-2   ml-2 mr-2">
                                <label for="exampleInputEmail1" style="float: right;">الاسم</label>
                                <input type="text" class="form-control" name= "fullname" id="exampleInputname1" aria-describedby="emailHelp" placeholder="ادخل الاسم">
                            </div>

                            <div class="form-group col-md-5 mb-2 ml-2 mr-2">
                                <label for="exampleInputPassword1" style="float: right;" >رقم التليفون</label>
                                <input type="text" class="form-control"  name="phone" id="exampleInputphone1" placeholder=" ادخل رقم التليفون">
                            </div>
                             @foreach($get_data as $x)
                            <div class="form-group  col-md-5 mb-2 ml-2 mr-2">
                                <label for="exampleInputEmail1" style="float: right;">أسم الدورة</label>
                                <input type="text" class="form-control" name="service[]" value="{{$x->name}}" readonly id="exampleInputNumber2" aria-describedby="emailHelp" placeholder="{{$x->name}}">
                            </div>
                   
                            <div class="form-group col-md-5 mb-2 ml-2 mr-2">
                                <label for="exampleInputPassword1" style="float: right;" >سعر الدورة</label>
                                <input type="text" class="form-control" readonly id="exampleInputprice2" placeholder="{{$x->price}}">
                            </div>
                           
                            <div class="form-group col-md-5 mb-2 ml-2 mr-2">
                                <label for="exampleInputPassword1" style="float: right;" >كود الخصم </label>
                                <input type="text" class="form-control" name="code" id="exampleInputprice2" placeholder="كود">
                            </div>
                           
                            @endforeach
                            
                        </div>
                        <br><br>
                      
                    </div>

                        <input type="checkbox" name="checkbox" id="option" value="{{$terms}}" required/> &nbsp;<label for="option"> <a href="{{$terms}}">الشروط و الاحكام </a></label>

                </div>
                    <div style=" text-align: center;">
                          <h3  id="head" class="text-center padding-bottom-2x" style="border-bottom: 1px solid rgb(238, 238, 238); padding-top: 0px !important;     padding-bottom: 28px !important; ">اختر طريقة الدفع</h3>
                     </div>
             </div>
             <div class="card-body">
                 <div class="con">
                    <div class="payment">
                          <button type="submit" name="submit" id="btnSubmit" formaction="{!! URL::to('payProcess') !!}" >
                            <img src="https://media.zid.store/static/bankTransfer.png">
                        </button>
                    </div>
                    
                    <div class="payment">
                          <button type="submit" name="submit" id="btnSubmit" formaction="{!! URL::to('payProcess') !!}" >
                            <img src="https://media.zid.store/static/mastercard-circle.png">
                        </button>
                    </div>
                    <div class="payment">
                          <button type="submit" name="submit" id="btnSubmit" formaction="{!! URL::to('payProcess') !!}" >
                            <img src="https://media.zid.store/static/visa-circle.png">
                        </button>
                    </div>
                    <div class="payment">
                          <button type="submit" name="submit" id="btnSubmit"  formaction="{!! URL::to('payProcess') !!}" >
                            <img src="https://media.zid.store/static/mada-circle.png">
                        </button>
                    </div>
                    <div class="payment">
                          <a >
                            <img src="https://media.zid.store/static/apple_pay.svg">
                        </a>
                    </div>
                    
                    <div class="payment">
                          <button type="submit" name="submit"   id="btnSubmit" formaction="{!! URL::to('paypal') !!}" >
                            <img src="/uploads/paypal.png">
                        </button>
                    </div>
                    
                 </div>
               
             </div>
</form>

           
</div>

<script>
    var elements = document.getElementsByClassName("payment");


    
    var myFunction = function() {
        // var attribute = this.getAttribute("data-myattribute");


        var checkBox = document.getElementById("option");
        if (checkBox.checked == true){
            for (var i = 0; i < elements.length; i++) {
            elements[i].style.display = 'none';
        }
        } else {
        }

        
    };

    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('click', myFunction, false);
    }
</script>
 <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
 <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
 <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>


</body>
</html>









