@extends('layouts.admin_const')

@section('content')



<div id="wrapper">

    

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                       <!--<h2>Jquery Datatable</h2>-->
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item"> المسوقين</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                             
                            <ul class="nav nav-tabs-new">
                                 <form id="basic-form" method="get"action="{{ route('affiliators.excel')}}" novalidate enctype="multipart/form-data"  class="text-right">
                                           {{ csrf_field() }}

                                            <div class="form-group " style="float: right; margin-left: 40px;">
                                                <label style="    font-size: x-large; margin-right: -60px;">من </label>
                                                <input type="date" name='fromDate' class="form-control text-right" style="margin-top: -30px;" required>
                                            
                                                <label style="    font-size: x-large; margin-right: -60px;">الى </label>
                                                <input type="date" name='toDate' class="form-control text-right" style="margin-top: -30px;" required>
                                            </div> 
                
                                    <li style="float: right;margin-left: auto;">
                                    <button class="btn btn-outline-success " style="margin-top: 30px;"> Export to Excel</button></li>


                                </form>
                                <li style="float: right;margin-left: auto;"><a href="/show_addAffilitaor" class="btn btn-success"style="font-size: x-large;"> أضافة مسوقين</a></li>
                                <li style="float: right;margin-left: auto;"><h2 style="font-size: xx-large;"> : المسوقين</h2> </li> &nbsp;&nbsp;

                            </ul>

                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover js-basic-example dataTable table-custom text-center">
                                    
                                       <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>الاسم كامل  </th>
                                            <th> الصورة</th>
                                            <th>البريد الالكترونى</th>
                                            <th>رقم الهاتف</th>
                                            <th>تابع الى مسوق</th>
                                            <th>الصفحه الخارجية</th>
                                            <th> كوبون المسوق</th>
                                            <th>الرصيد  </th>
                                            <th>تقارير الزيارات</th>
                                            <th>المرحلة </th>
                                            <th>رقم المستند </th>
                                            <th>رقم الهوية</th>
                                            <th>وقت الانشاء</th>
                                            
                                            <th>تعديل</th>
                                            <th>مسح</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($get_affiliators as $affiliate)
                                       
                                     
                                        <tr>
                                            <td>{{$affiliate->id}}</td>
                                            <td>
                                               <form action="/show_affiliatePage" method="GET"  id="sub_{{$affiliate->id}}" enctype="multipart/form-data">
                                   
                                                    {{ csrf_field() }}

                                                    <input type="hidden" name="user_id" value="{{$affiliate->id}}">

                                                    <a  onclick="$('#sub_{{$affiliate->id }}').submit();">

                                                        <button class="btn  ">  {{$affiliate->first_name}}  {{$affiliate->last_name}} </button>
                                                    </a>
                                               </form>
                                                                 
                                            </td>
                                            
                                            <td>
                                             <?php  $image = $affiliate->image;
                                                
                                                if( $image == ''){
                                                
                                                }else{?>
                                                 <a href="/uploads/affiliates/{{$affiliate->image}}"><img src="/uploads/affiliates/{{$affiliate->image}}" width="250" height="100" target="_blank"></a>
                                                <?php } ?>
                                            </td>
                                            <td>{{$affiliate->email}}</td>
                                            
                                            
                                            <td>{{$affiliate->phone}}</td>
                                              <?php
                                                  $affiliator = \App\affiliate::select('id','first_name','last_name')->where('id',$affiliate->affiliate_id)->first();

                                                    if($affiliate->affiliate_id == NULL){
                                                        ?><td></td>

                                                  <?php  }else{
                                                     ?>
                                                        <td>
                                                             <form action="/show_affiliatePage" method="GET"  id="sub_{{$affiliator->id}}" enctype="multipart/form-data">
                                   
                                                            {{ csrf_field() }}

                                                            <input type="hidden" name="user_id" value="{{$affiliator->id}}">

                                                            <a  onclick="$('#sub_{{$affiliator->id }}').submit();">

                                                                 <button class="btn  ">
                                                                
                                                           {{$affiliator->first_name}}  {{$affiliator->last_name}}
                                                                </button>
                                                                
                                                                  </a>      
                           
                                                            </form>
                                                     </td>
                                                        
                                               <?php  }
                                                ?>
                                                 
                                                 
                                                     <?php $reserve = $affiliate->reserve_page ;
                                                     
                                                     if($reserve == 1){
                                                         
                                                     ?>
                                                 <td>
                                                      <form action="/update_reservationPage" method="GET"  id="sub_{{$affiliate->id}}" enctype="multipart/form-data">
                               
                                                        {{ csrf_field() }}

                                                        <input type="hidden" name="user_id" value="{{$affiliate->id}}">

                                                        <a  onclick="$('#sub_{{$affiliate->id }}').submit();">

                                                             <button class="btn btn-outline-info">ألغاء الصفحة </button>
                                                            
                                                              </a>      
                       
                                                      </form>  
                                                 </td>
                                                     <?php }else{ ?>
                                                      
                                                <td>
                                                      <form action="/update_reservationPage" method="GET"  id="sub_{{$affiliate->id}}" enctype="multipart/form-data">
                               
                                                        {{ csrf_field() }}

                                                        <input type="hidden" name="user_id" value="{{$affiliate->id}}">

                                                        <a  onclick="$('#sub_{{$affiliate->id }}').submit();">

                                                             <button class="btn btn-outline-info">تفعيل الصفحة</button>
                                                            
                                                              </a>      
                       
                                                      </form>  
                                                 </td>   
                                                     <?php }  ?>
                                                
                                                <td>
                                                    <form action="view_affiliateCoupon" method="GET"  id="sub_{{$affiliate->id}}" enctype="multipart/form-data">
                               
                                                        {{ csrf_field() }}

                                                        <input type="hidden" name="affiliate_id" value="{{$affiliate->id}}">

                                                        <a  onclick="$('#sub_{{$affiliate->id }}').submit();">

                                                             <button class="btn  btn-secondary">عرض الكوبون </button>
                                                            
                                                              </a>      
                       
                                                        </form>  
                                                </td>
                                                 <td>{{$affiliate->balance}}</td> 
                                                 <td>
                                                      <form action="/show_affiliateVists" method="GET"  id="sub_{{$affiliate->id}}" enctype="multipart/form-data">
                               
                                                        {{ csrf_field() }}

                                                        <input type="hidden" name="user_id" value="{{$affiliate->id}}">

                                                        <a  onclick="$('#sub_{{$affiliate->id }}').submit();">

                                                             <button class="btn  btn-secondary">عدد الزيارات </button>
                                                            
                                                              </a>      
                       
                                                        </form>  
                                                 </td>
                                                        
                                            <?php
                                            
                                            if( $affiliate->stage == 1){ ?>
                                                        
                                                     <td style=" background-color: graytext ">{{$affiliate->stage}}</td>
                                           
                                            <?php } elseif( $affiliate->stage == 2){ ?>
                                                               
                                                     <td style=" background-color: gold ">{{$affiliate->stage}}</td>

                                             <?php } elseif( $affiliate->stage == 3){ ?>
                                                     
                                                     <td style=" background-color: #aa1e1e ">{{$affiliate->stage}}</td>

                                             <?php } ?>
                                                
                                            <td>{{$affiliate->version_seen}}</td>    
                                            <td>{{$affiliate->card_no}}</td>
                                            <td>{{$affiliate->created_at}}</td>
                                              
                                            <td>
                                           
                                                  <form action="/show_editAffiliator" method="get" id='sub_{{$affiliate->id}}'  enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="affiliate_id" value="{{$affiliate->id}}">
                                                    <a onclick="$('#sub_{{$affiliate->id}}').submit();">
                                                        <button class="btn btn-sm btn-icon btn-pure btn-default on-default m-r-5 button-edit" data-toggle="tooltip" data-original-title="تعديل">
                                                        <i class="icon-pencil" aria-hidden="true"></i>
                                                        </button>                                      
                                                    </a>
                                                </form>

                                            </td>                                          
                                            
                                            <td>
                                               
                                          
                                               <form action="/delete_affiliator" method="GET"  id="sub_{{$affiliate->id}}" enctype="multipart/form-data">
                                   
                                                   {{ csrf_field() }}
                               
                                                   <input type="hidden" name="affiliator_id" value="{{$affiliate->id}}">
                                      
                                                   <a  onclick="$('#sub_{{$affiliate->id}}').submit();">

                                                    <button class="btn btn-sm btn-icon btn-pure btn-default on-default button-remove"  onclick="return myFunction();" data-original-title="مسح">
                                                            <i class="icon-trash" aria-hidden="true"></i>
                                                    </button>  
                                                   </a>      
                           
                                               </form>
                                                
                                                
                                            </td> 
                                            
                                        </tr>
                                      @endforeach                                    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>            
        </div>       
    </div>
</div>

         


<script>
  function myFunction() {
      if(!confirm("هل تريد مسح هذا المسوق"))
      event.preventDefault();
  }
 </script>

@endsection