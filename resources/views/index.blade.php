@extends('layouts.const')

@section('content')


<?php 
         
            ?>                    
            
 <!-- small logo section -->
        <!-- main -->
        <section class="main">
        <div class="container">
            
               @if($message= Session::get('Success'))
            
            <div class="w3-panel w3-green w3-display-container">
                <span onclick="this.parentElement.style.display='none' "
                      class="w3-button w3-green w3-large w3-display-topright">&times;</span>
                
                      <p>{!! $message !!}</p>
            </div>
            
            <?php Session::forget('success'); ?>
            @endif
            
            
            @if($message = Session::get('error'))
             <div class="w3-panel w3-red w3-display-container">
                <span onclick="this.parentElement.style.display='none'"
                        class="w3-button w3-red w3-large w3-display-topright">&times;</span>
                <p>{!! $message !!}</p>
            </div>
            <?php Session::forget('error');?>
            @endif
            
            <div class="row" id="register_form">
                <div class=" col-lg-6 col-md-12 hidden-sm hidden-xs">
                    <div class="video">
                        <div class="video-container">
                            <?php
                                $get_video = DB::select("select *  from video_link");
                            ?>
                            
                            <iframe src="{{$get_video[0]->link}}" allow="autoplay; encrypted-media" allowfullscreen="" width="560" height="315" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
                <?php
                    $content = DB::select("select * from web_content");
                ?>
                <div class=" col-lg-6 col-md-12">
                    <div class="text text-center" data-aos="zoom-in">
                        <h2 >{{$content[0]->small_title}}</h2>
                        <h1 style='font-size:20px'>{{$content[0]->big_title}}</h1>
                        <p>{{$content[0]->content}}</p>
                    </div>
                <div class="tabs visible-sm visible-xs">
                        <div class="video">
                            <div class="video-container"> 
                            
                            <iframe src="{{$get_video[0]->link}}" allow="autoplay; encrypted-media" allowfullscreen="" width="560" height="315" frameborder="0"></iframe>
                        </div>
                        </div>
                    </div>
                     
                    <?php
                        $get_titles  = DB::select("select *  from web_content where id=1");
                     ?>
                    
               <div class="form" id="register_form">
                        <h2 > {{$get_titles[0]->socialForm_title}}</h2>
             
                        <form action="/doctorContact" method="post" >
                                                {{ csrf_field() }}

                            <input type="hidden" value="gs_aesthetic" id="offer_no" name="offer_no">
                            <div class="row">
                                <!--<div class="col-sm-6">
                                    <div class="input-group">
                                        <input class="form-control" autocomplete="off" name="address" id="country1" placeholder="بلد الإقامة" required="" type="text">
                                        <span class="input-group-addon">
                                            <i class="fa fa-globe" aria-hidden="true" style="font-size:17px;"></i>
                                        </span>
                                    </div>
                                </div>-->
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <input class="form-control" autocomplete="off" name="fullname" placeholder="الإسم الكامل" id="name1" required="" type="text">
                                        <span class="input-group-addon">
                                            <i class="fa fa-user" aria-hidden="true" style="font-size:17px;"></i>
                                        </span>
                                    </div>
                                </div>
                               <!-- <div class="col-sm-6">
                                    <div class="input-group">
                                        <input class="form-control modalform" autocomplete="off" name="nationality" id="nationality1" placeholder="الجنسية" required="" type="text">
                                        <span class="input-group-addon">
                                            <i class="fa fa-id-card" aria-hidden="true" style="font-size:14px;"></i>
                                        </span>
                                    </div>
                                </div>-->
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <input class="form-control" autocomplete="off" name="phone" id="phone1" placeholder="الهاتف مع رمز البلد" required="" type="text">
                                        <span class="input-group-addon">
                                            <i class="fas fa-mobile-alt " aria-hidden="true" style="font-size:22px;"></i>
                                        </span>
                                    </div>
                                </div> 
                                <div class="col-lg-12">
                                       <h4 class=" text-right" style="color: cornsilk">: اختار الخدمة</h4>
                                    <select name="service[]" id="example" class="form-control"  multiple="multiple" style="display: none;">
       
        
                                        <?php 
                                        $get_service = \App\Service::select('id','name')->orderBy('id','ASC')->get();
                                        ?>
       
                                            @foreach($get_service as $service)

                                                 <option   style="float: right"  value="{{$service->name}}">&nbsp;{{$service->name}}</option>
                                            @endForeach

                                    </select><br>
                               </div><br><br>
                               <div class="col-lg-6">
                                    <button type="submit" name="submit" class="form-control btn" >
                                        <span>
                                            إرسال
                                            <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                        </span>
                                    </button>
                                </div>

                               <div class="col-lg-6">
                                    <button type="submit" name="submit" class="form-control btn" formaction="{!! URL::to('payProcess') !!}" style="background: #aa1e1e;" >
                                       <!--<button type="reset" onclick="location.href='/show_reservePage/1'">-->
                                         <span>
                                            Pay Online
                                            <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                        </span>
                                                                                        
                                        <input type="hidden" id="amount" name="amount">

                                    </button>
                                </div>
                             
<!--                                <form class=" w3-container w3-display-middle w3-card-4 w3-padding-16" method="post" id="payment-form" 
                                        action="{!! URL::to('paypal') !!}">
                                      
                                      {{csrf_field()}}
                                        <input type="hidden" id="amount" name="amount" value="25">
                                      <button class="form-control btn col-lg-6">pay with paypal</button>
                                  </form>-->
                            </div>
                            

                        </form>
           
                    </div>
                </div>
            </div>
        </div>
    </section>
            <!--end of main -->
    
        <!-- january-offer -->
        
        <section class="j-offer text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-6  text-center">
                     <?php
                    $getphone = DB::select("select *  from call_info");
                ?>
                    <a href="" data-toggle="modal" data-target="#whatsUser_model" >
                        <span>
                            <i class="fab fa-whatsapp" aria-hidden="true"></i>
                        </span>
                        التواصل عبر الواتساب
                    </a>
                </div>
                <div class="col-md-6" >
                   <h2 class="bold" style="">!! عروض الشهر الحالي</h2>
                    <h3>احصلوا على الأسعار خلال دقيقة واحدة</h3>
                </div>
            
            </div>
        </div>
    </section>
  <?php
  
   $get_logo = DB::select("SELECT * FROM `video_link`");
                ?>
          <!--end of january-offer -->
        <!-- services-->
        <div class="services text-center pt-62 pb-62">
        <div class="container">
            <!--<h4>خدماتنا</h4>-->
            <h3>{{$get_titles[0]->service_title}}</h3>
            <hr class="text-center" style="width:20%; border:solid {{$get_logo[0]->second_color}} ;margin-left: 40%;">
            <div class="row" style=" /*margin-right: -378px;margin-left: 35px;*/">
                <?php
                    $allservices = \App\Service::select('id','name','description','image','price','level1','level2','level3','created_at','updated_at')->orderBy('id','ASC')->get();
                ?>
                @foreach($allservices as $services)
                
                                    <?php
                        $get_images = DB::select("select id, image from image_services where service_id=?",[$services->id]);
                        $i = 0;
                        
                      ?>
                       <!--c-sin c-sin-sp /// bta3 al 5aat aly byfsal fe service-->
                       <div class="col-md-3" data-aos="zoom-in-up">
                   
                 <div id="carouselControlsService" class="carousel slide" data-ride="carousel"  >
                    <div class="carousel-inner"  data-toggle="modal" data-target="#exampleModal"  onclick="img('{{$services->id}}')">

                        <?php if( $get_images == NULL ){
                                ?>
                          <div class="carousel-item  {{ $temp }}">
                                   <img class="d-block img-fluid" src="./uploads/123_logo.png" width="250" height="250" style="margin: auto;height:260px ">

                                      <div class="carousel-caption d-none d-md-block">

                                      </div>
                               </div>
                                <?php
                            }else{
                        ?>
                        
                        @foreach( $get_images as $photo )
                        
                        <?php
                        $i++;
                            if($i == 1){
                                $temp = 'active';
                            }else{
                                $temp = '';
                            }
                            
                            
                           ?>
                        
                       
                               <div class="carousel-item  {{ $temp }}">
                                   <img class="d-block img-fluid" src="./uploads/{{$photo->image}}" width="250" height="250" style="margin: auto">

                                      <div class="carousel-caption d-none d-md-block">

                                      </div>
                               </div>
                           
                        @endforeach
                
                <?php }  ?>


                    </div>
                    <a class=" carousel-control-prev" href="#carouselControlsService" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class=" carousel-control-next" href="#carouselControlsService" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
                 
                 
                  
                 <?php

                        $paragraph = $services->description;
                        
                        $count = strlen($paragraph);
                        $get_first = mb_substr($paragraph ,0, 141);
                        $get_last = mb_substr($paragraph , 141, $count);
                 ?>
                 
                 <h4 style="margin-top:20px;">{{$services->name}}</h4><br>
                 <p style="white-space: pre-line; margin-right: 20px; text-align: justify;  direction: rtl;"  > {{$get_first}} 
                 <span id="more{{$services->id}}" style="display: none">{{$get_last}}</span></p>
                 <?php 
                 if($get_last == NULL){
                     ?><br><br> <?php
                 }else{
                     ?> <a href="#" onclick="toggleText(this, 'more{{$services->id}}'); return false;">Read more</a>

                 <?php } ?>

                 
                 <?php 
                 
                    if( $services->price == NULL){
                   
                    }else{
                            ?>
                 <a class="btn servb" style=" background-color: #aa1e1e; color: white; font-size: 16px;"  href="show_reservePage/{{$services->id}}">
                      <span style=" background-color: #9d9d9d;">
                            <img src="uploads/coin.png" style="width: 30px; margin-top: 7px; ">
                        </span>
                        {{$services->price}}
                    </a>
                <?php } ?>
                     <a class="btn servb" href="" data-toggle="modal" data-target="#whatsUser_model" >
                        <span>
                            <i class="fab fa-whatsapp" aria-hidde="true"></i>
                        </span>
                        التواصل عبر الواتساب
                    </a>
                </div>
                
                
                   
   
                @endforeach
               
            </div>
        </div>
    </div>
        
        <!--end services-->
       <?php
                  use Illuminate\Support\Facades\DB;
                    $get_allimages = DB::select('select * from service_image');
                    
                    $i = 0;
                  ?>
<!-- before-->
<section class="before text-center pt-62 pb-62">
<div class="container">
    <div class="row text-center">
         <div class="col-lg-6 col-md-6  cars">

        
          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                   
                @foreach( $get_allimages as $photo )
                
                <?php
                 $i++;
                    if($i == 1){
                        $temp = 'active';
                    }else{
                        $temp = '';
                    }
                ?>
                       <div class="carousel-item {{ $temp }}">
                           <img class="d-block img-fluid" src="/uploads/{{ $photo->image }}" >
                              <div class="carousel-caption d-none d-md-block">

                              </div>
                       </div>
                @endforeach`
                    

            </div>
            <a class=" carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class=" carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
                  
         </div>              

          <div class="col-lg-6 col-md-6 ">
              <!-- reviews carousel -->
                 <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <?php
                        $get_review = DB::select("select *  from review_image");
                        $i = 0;
                        ?>
                     @foreach( $get_review as $review )
                     
                      <?php
                         $i++;
                            if($i == 1){
                                $temp = 'active';
                            }else{
                                $temp = '';
                            }
                        ?>
                        <div class="carousel-item {{ $temp }}">
                            <img class="d-block img-fluid" src="/uploads/{{ $review->image }}" >
                               <div class="carousel-caption d-none d-md-block">

                               </div>
                        </div>
                     @endforeach
                     
                    </div>
                   <a class="automatic carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="automatic carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
               <!--end of before-->
        
        <!-- channels -->
     <!--   <section class="channels">
        <div class="container">
            <div class="row">
                
                <?php
                
                    $get_sponser = DB::select("select *  from sponser");
                ?>
                
                @foreach($get_sponser as  $sponser)
                <div class="col col-sm-2 col-xs-4">
                    <div class="c-container">
                        <img src="/uploads/{{$sponser->image}}" alt="" class="img-responsive">
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>-->
        <!-- end channels -->
        <!--footer-->
        <footer>
    <div class="container">
        <div class="row" >
                 
             <div class="col-md-4  col-sm-12 text-center" >
                <hr class=" send1">
                <h6 class="send1 text-right">ارسال رسالة</h6>
                <form action="/sendmessage" method="post">
                    {{csrf_field()}}
                    <input type="text" class="form-control new" autocomplete="off" name="country" placeholder="بلد الإقامة" id="name" required="">
                    <input type="text" class="form-control new" autocomplete="off" name="phone" placeholder="الهاتف او الايميل" id="name1" required="">
                    <textarea id="" rows="2" autocomplete="off" style="width:100%;" name="message" class="new" placeholder="الرسالة" ></textarea>
                    <button type="submit">
                        <span class="aljazeera">

                            ارسال <i class="fa fa-paper-plane" aria-hidden="true"></i>
                        </span>
                    </button>
                </form>
            </div>
            <div class="col-md-4  col-sm-6 hidden-xss" >
                <h6 class="text-right">معلومات الاتصال</h6>
                <ul class="addresss-info">
                    <li>
                        <table>
                            <tbody>
                                
                            <?php
                            
                                $callData = DB::select("select *  from call_info");
                                
                                $get_map = DB::select("select * from social_media where id=1");
                            ?>
                                
                                @foreach($callData as $call)
                                <tr>
                                <td> <i class="fa fa-map-marker"></i></td>
                                <td>
                                    <a href="http://maps.google.com/maps?q={{$get_map[0]->Latitude}},{{$get_map[0]->Longitude}}&amp;ll={{$get_map[0]->Latitude}},{{$get_map[0]->Longitude}}&amp;z=14" target="_blank">
                                        <p>Address:{{$call->address}}</p>
                                    </a>
                                </td>
                            </tr>
                        </tbody></table>
                    </li>
                    <li>
                        <table>
                            <tbody><tr>
                                <td> <i class="fa fa-phone"></i></td>
                                <td style="direction: ltr;">
                                    <a href="tel://{{$call->phone}}" onclick="return gtag_report_conversion('tel://{{$call->phone}}');"> <span>{{$call->phone}}</span> </a>
                                </td>
                            </tr>
                        </tbody></table>
                    </li>
                    <li>
                        <table>
                            <tbody><tr>
                                <td> <i class="far fa-envelope"></i></td>
                                <td>
                                    <a href="mailto:{{$call->email}}" target="_blank">Email:{{$call->email}}</a>
                                </td>
                            </tr>
                        </tbody></table>
                    </li>
                </ul>
                @endforeach
            </div>
                   <div class="col-md-4 col-sm-6 hidden-xss" >
                <h6 class="text-right">من نحن</h6>
                    <?php
                     $get_aboutus = DB::select("select * from about_us");
                    ?>
                    @foreach($get_aboutus as $aboutus)
                    
                <p class="logob text-right"> {{$aboutus->content}}</p>
                
                @endforeach
                
                 <?php
                    
                        $get_logo = DB::select("SELECT logo FROM `video_link`");
                    ?>
                <img src="/uploads/{{$get_logo[0]->logo}}" alt="" class="img-responsive logow  ">
            </div>
           
        </div>
        
        <?php
        
        $socail = DB::select("select * from social_media where id=1");
                                     

        
        ?>
        
        
        
        <!-- copy for lg -->
        <div class="copy ">
            <div class="row">
                
                <div class="col-md-6 col-sm-6" style="text-align:right">
                    <ul class="list-unstyled list-inline social">
                        <li>
                            <a href="{{$socail[0]->facebook}}" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{$socail[0]->instagram}}" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{$socail[0]->twitter}}" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{$socail[0]->youtube}}" target="_blank">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{$socail[0]->snapchat}}" target="_blank">
                                <i class="fab fa-snapchat-ghost"></i>
                            </a>
                        </li>
                        <li>
                            <a href="http://maps.google.com/maps?q={{$socail[0]->Latitude}},{{$socail[0]->Longitude}}&amp;ll={{$socail[0]->Latitude}},{{$socail[0]->Longitude}}&amp;z=14" target="_blank">
                                <i class="fas fa-map-marker-alt"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-6">
                    <p class="copyp">
                       Copyright 2019 <span>Code Caique Solution </span>All Rights Reserved | <a href="http://www.codecaique.com/" target="_blank">Private Policy</a> ©
                    </p>
                </div>
            </div>
        </div>
      
    </div>
</footer>
        <!--end of footer-->
        
       
                <!--fixed bar -->
<div class="fixed-bar">
        <ul class="lg-bar2 list-unstyled hidden-xs show" >
        <li>
             <a href="http://maps.google.com/maps?q=41.061634,28.989238&amp;ll=41.061634,28.989238&amp;z=14" target="_blank">
                    <i class="fas fa-map-marker-alt"></i>
             </a>
        </li>
        <li>                           
           <a href="https://www.snapchat.com/add/qta_edu" target="_blank">
                <i class="fab fa-snapchat-ghost"></i>
           </a>
        </li>
        <li>
            <a href="tel://{{$getphone[0]->phone}}" target="_blank" onclick="return gtag_report_conversion('tel://{{$getphone[0]->phone}}');">
                <i class="fas fa-phone" style="font-size: 17px;"></i>
            </a>
        </li>
        <li>
            <a  href="" data-toggle="modal" data-target="#whatsUser_model"  >
                <i class="fab fa-whatsapp"></i>
            </a>
        </li>
       
     <!--    <li>
           <a data-toggle="modal" data-target="#myModal" href="#">
                <i class="far fa-edit" style="font-size: 17px;"></i>
            </a>
        </li>-->
        <li>
            <a href="mailto:{{$getphone[0]->email}}" target="_blank" onclick="return goog_report_conversion('mailto:{{$getphone[0]->email}}');">
                <i class="far fa-envelope"></i>
            </a>
        </li>
    </ul>
    </div>
        <!--end fixed bar -->

        <!--chat
         <a  class="pic-chat">
   <img src="./images/chat.jpg" class="img-fluid img-chat">
       </a> 
        <div class="container">
   <div class="row">
       <div class="col-sm-12"></div>
              <div class="col-sm-12"></div>

            <div class="col-lg-3 col-md-12 col-sm-12" >  
           <div id="root" >     
   <div style="width:100%;height:100%">   
   <div dir="rtl">
    <div class="chat">
       
       
       
    <div class=" sec-chat ">

       <a class="ellipsis " style="display: inline-block">
           <i class="fas fa-ellipsis-h" style="margin-top: 10px"></i>        
       </a>
   
  
    
       <p class="thanks text-center" style="display: inline-block">شكراً لزيارتكم موقع كلينك إكسبرت</p>

       <a class="minus" style="display: inline-block">
               <i class="fas fa-minus float-right" ></i>
       </a>
                         
    </div>

       
       
       
        <div class="point-chat">
            <ul>
                <li class="msg">
                   <i class="fas fa-envelope"></i>
                    <span class="send">  ارسال رسالة</span>
                   
                </li>
                <li class="msg mute">
                 <i class="fas fa-volume-mute"></i>   
                    <span class="send">mute</span>
                   
                </li>
            </ul>
        </div>
        <div class=" ">
            <div class=" " style="bottom: 0px; height: auto;">
                
                <div class="sec2">
                    <div class="part-sec2 rpw" style="opacity: 1;">
                        <div class="part-img col-sm-12">
                            <div class="chatt-img ">
                                <img src="./images/chat.jpg">
                            </div>
                            <div class="active1">
                            </div>
                             <div class="">
                                <div class="name-chat ">
                                    Leyla</div>
                                <div class="desc ">
                                    Support Agent</div>
                            </div>
                        </div>
                
                    </div>
                </div>
            </div>
        </div>
        <div class="">
            <div class="">
                <div class="conversation">
                    <div class="">
                        <div class="image-chat">
                            <img src="./images/chat.jpg">
                            <span class="name-conv">
                                           12:29     Leyla
                                            </span>
                        </div>
                    </div>
        <div class="start-con">
            <div class="words">
                <span class="Linkify">
                    أو التواصل عبر الواتساب عبر الرابط
                    <a href="https://api.whatsapp.com/send?phone={{$getphone[0]->whatsapp}}&amp;&amp;text=%d9%85%d8%b1%d8%ad%d8%a8%d8%a7%2c%20%d9%86%d8%b1%d9%8a%d8%af%20%d9%85%d8%b9%d8%b1%d9%81%d8%a9%20%d8%a7%d9%84%d8%ae%d8%af%d9%85%d8%a7%d8%aa%20%d9%88%d8%a7%d9%84%d8%aa%d9%83%d8%a7%d9%84%d9%8a%d9%81%20%d8%a7%d9%84%d8%ad%d8%a7%d9%84%d9%8a%d8%a9" target="_blank" onclick="return gtag_report_conversion('https://api.whatsapp.com/send?phone={{$getphone[0]->whatsapp}}&amp;&amp;text=%d9%85%d8%b1%d8%ad%d8%a8%d8%a7%2c%20%d9%86%d8%b1%d9%8a%d8%af%20%d9%85%d8%b9%d8%b1%d9%81%d8%a9%20%d8%a7%d9%84%d8%ae%d8%af%d9%85%d8%a7%d8%aa%20%d9%88%d8%a7%d9%84%d8%aa%d9%83%d8%a7%d9%84%d9%8a%d9%81%20%d8%a7%d9%84%d8%ad%d8%a7%d9%84%d9%8a%d8%a9');">
                        https://goo.gl/9HUaVy
                    </a>
                </span>
                                               
             </div> 
            </div>
            
            
            
                
             <div class="start-con">
                 <div class="words">
                    <span class="Linkify">
                    </span>
                 </div>
             </div>
            
            <div class="visitor">
            <span class="name-conv">
                                   12:29  زائر
                                    </span>
            
            </div>
            <div class="visitor-con">
                    <div class="words">
                        <span class="Linkify">a</span>
                    </div>
            </div>
                    
                    
                    
                </div>
            </div>
        </div>
<div class="send-msgg ">
                     <form id="basic-form" method="post"action="/add_msg" novalidate enctype="multipart/form-data">
                                           {{ csrf_field() }}

                        <div class="padd row">
                            <div class="col-sm-10">
                                <textarea class="form-control" id="exampleFormControlTextarea1" name ="new_msg" placeholder="اكتب رسالتك هنا واضغط زر الإدخال لإرسالها" ></textarea>
                                </div>
                            <div class="col-sm-2 send-words">

                               
                                <button type="submit" name="edit_about"> <i class="fas fa-2x fa-chevron-circle-right"></i></button>

                                    
                              

                            </div>
                            
                        </div>
   
                     </form>


</div>
        <div class="footer-chat">
            <a href="#" >
                <span>Powered by </span>
                <strong>Live Chat</strong>
            </a></div>
     </div>
       </div>
            
         </div>
               
        </div>
         
</div>
            </div>
        </div>
-->
<div class="last-sec">
  <button class="btn btn-secondary btnup" [routerLink]="'/'">
    <a style="color:white " >

    <i class="fab fa-whatsapp fa-3x"></i></a>

  </button>

</div>

<div class="whatsappme__box" style="background:url(../uploads/background.webp);display: none">
  <div class="box-header">
    
      <div class="text-right  whts-app">
                  <a class="ay7aga text-right"><i class="fa fa-times-circle  " style="font-size:30px; "></i></a> 

           <a  class=" text-right" style="color:white ">
        <i class="fab fa-whatsapp"></i></a>
      <p class="text-right whts">WhatsApp</p>
     

        
       
      </div>

     
  

  </div>
  <div class="box-message">
    
      <div class="whatsappme__message__wrap">
        <div class="whatsappme__message__content">
        :) مرحبا<br>كيف يمكننى مساعدتك؟
          <br>
        </div>
      </div>

  </div>
  <div class="whatsappme__button">
      
      <div class="whatsappme__button__sendtext">
       <a style="color: white; text-decoration:none" data-toggle="modal" data-target="#whatsUser_model" > تحدث معنا</a>
             <i class="fa fa-comment comment"></i>
 </div>  </div>
  <div class="whatsappme__copy">
      Powered by 
      <a href="www.codecaique.com">Code Caique Solution </a>
  </div>


 </div>

        <!--modal form-->
   
        
        <!-- Modal -->
<!--<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">-->
<!--  <div class="modal-dialog" role="document">-->
<!--    <div class="modal-content">-->
<!--      <div class="modal-header">-->
<!--        <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--          <span aria-hidden="true">&times;</span>-->
<!--        </button>-->
<!--      </div>-->
<!--      <div class="modal-body">-->
<!--               <form action="process.php" method="post">-->
<!--                        <h2 class="text-center" style="color: white">الإتصال بالفريق الطبي</h2>-->
<!--                        <hr style="width:12%;border: 2px solid #dd2c2c;margin: 20px auto 40px">-->
<!--                        <input type="hidden" value="gs_aesthetic" id="offer_no" name="offer_no">-->
<!--                        <div class="input-group mb-3">-->
<!--                            <input type="text" class="form-control" autocomplete="off" name="name" placeholder=" الإسم الكامل" id="name1" required="">-->
<!--                            <span class="input-group-addon">-->
<!--                                <i class="fa fa-user" aria-hidden="true" style="font-size:17px;"></i>-->
<!--                            </span>-->
<!--                        </div>-->
<!--                        <div class="input-group mb-3">-->
<!--                            <input class="form-control" type="text" autocomplete="off" name="phone" id="phone1" placeholder=" الهاتف مع رمز البلد" required="">-->
<!--                            <span class="input-group-addon">-->
<!--                                <i class="fas fa-mobile-alt " aria-hidden="true" style="font-size:20px;"></i>-->
<!--                            </span>-->
<!--                        </div>-->
<!--                        <div class="input-group mb-3">-->
<!--                            <input class="form-control" type="text" autocomplete="off" name="country" id="country1" placeholder=" بلد الإقامة" required="">-->
<!--                            <span class="input-group-addon">-->
<!--                                <i class="fa fa-globe" aria-hidden="true" style="font-size:17px;"></i>-->
<!--                            </span>-->
<!--                        </div>-->
<!--                        <div class="input-group mb-3">-->
<!--                            <input type="text" class="form-control modalform" autocomplete="off" name="nationality" id="nationality1" placeholder="الجنسية" required="">-->
<!--                            <span class="input-group-addon">-->
<!--                                <i class="fa fa-id-card" aria-hidden="true" style="font-size:14px;"></i>-->
<!--                            </span>-->
<!--                        </div>-->
<!--                        <button type="submit" class="form-control btn bk-red">-->
<!--                            <span class="aljazeera">-->
<!--                                <i class="fa fa-paper-plane" aria-hidden="true"></i>-->
<!--                                إرسال-->
<!--                            </span>-->
<!--                        </button>-->
<!--                    </form>-->
<!--      </div>-->
     
<!--    </div>-->
<!--  </div>-->
<!--</div>-->
        <!--end modal form-->
     
       
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
            <div class="modal-dialog" role="document" >
    <div class="modal-content" style=" background: white" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: black">
                    <span aria-hidden="true">&times;</span>
            </button>
        </div>
      <div class="modal-body">
         <div id="carouselExampleControlss" class="carousel slide" data-ride="carousel"   >
                    <div class="carousel-inner-x">
                     
                              
                                  
                               
                       

                    </div>
                    <a class=" carousel-control-prev" href="#carouselExampleControlss" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class=" carousel-control-next" href="#carouselExampleControlss" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
                 
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>




<!-- Modal whatsapp users :: -->
<div class="modal fade" id="whatsUser_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="direction: rtl; height: auto;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> من فضلك ادخل اسمك..  </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
    
        <form  action="add_whatsappUser" method="post" >
            {{ csrf_field() }}
            <input type="text" class="form-control" id="exampleInputPassword1" name="username" placeholder=" أسمك ثنائي"  required>
     
     
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">
               ارسال
            
        </button>
        </div>
</div>
      </form>
    </div>
  </div>
</div>                 
  



        
        
        
        
<script type="text/javascript">

     function img(id){
         
        $.ajax({
          method: "get",
          url: "/ajax_get_imgs",
          data: { id: id }
        })
          .done(function( msg ) {
            $('.carousel').carousel({interval: 2000});
            $('.carousel-inner-x').empty();
            $('.carousel-inner-x').html(msg.data);
           // alert( "Data Saved: " + msg.data );
           
           
          // $('.modal-content').html(msg.view);
          });
    }   

setTimeout(img, 10);



      function toggleText(btn,id){
         var evv = document.getElementById(id);
         if(evv.style.display == 'block'){
             evv.style.display = 'none';
             btn.innerHTML = "Read more";
         }else{
             evv.style.display = "block";
             btn.innerHTML = "Read less";
         }
     } 




</script>      
       
       
       
    
@endsection