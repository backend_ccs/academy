@extends('layouts.admin_const')

@section('content')





<div id="wrapper">

    

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                       <!--<h2>Jquery Datatable</h2>-->
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item"> الخدمات</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                             
                            <ul class="nav nav-tabs-new">
                                <li class="nav-item" style="margin-top: -5px;margin-left: 7px;font-size: x-large;margin-left: auto;"><a class="nav-link"  href="/show_addservice">اضافة خدمة</a></li>
                                <li style="float: right;margin-left: auto;"><h2 style="font-size: xx-large;"> :جميع الخدمات </h2> </li> &nbsp;&nbsp;

                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover js-basic-example dataTable table-custom text-center">
                                    
                                       <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>اسم الخدمة </th>
                                            <th class="col-md-4"> التفاصيل</th>
                                            <th>سعر الخدمة</th>
                                            <th> الصور </th>
                                            <th> المرحلة الاولى </th>
                                            <th>المرحلة الثانية </th>
                                            <th> المرحلة الثالثة</th>
                                            <th>وقت الانشاء</th>
                                            <th> وقت التعديل</th>
                                            <th>تعديل</th>
                                            <th>مسح</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($get_servicedata as $service)
                                        <tr>
                                            <td>{{$service->id}}</td>
                                            <td>{{$service->name}}</td>
                                            <td>{{$service->description}}</td>    
                                            <td>{{$service->price}}</td>
                                            <td>
                                                <form action="/show_images" method="get" id='sub_{{$service->id}}'  enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="service_id" value="{{$service->id}}">
                                                    <a onclick="$('#sub_{{$service->id}}').submit();">
                                                        <button class="btn btn-sm btn-icon btn-pure btn-default on-default m-r-5 button-edit" data-toggle="tooltip" data-original-title="عرض الصور">
                                                        عرض الصور
                                                        </button>                                      
                                                    </a>
                                                </form>
                                            </td> 
                                            <td>{{$service->level1}}</td>
                                            <td>{{$service->level2}}</td>
                                            <td>{{$service->level3}}</td>
                                            <td>{{$service->created_at}}</td>
                                            <td>{{$service->updated_at}}</td>
                                              
                                            
                                            <td>
                                           
                                                  <form action="/edit_servicedata" method="get" id='sub_{{$service->id}}'  enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="service_id" value="{{$service->id}}">
                                                    <a onclick="$('#sub_{{$service->id}}').submit();">
                                                        <button class="btn btn-sm btn-icon btn-pure btn-default on-default m-r-5 button-edit" data-toggle="tooltip" data-original-title="تعديل">
                                                        <i class="icon-pencil" aria-hidden="true"></i>
                                                        </button>                                      
                                                    </a>
                                                </form>

                                            </td>
                                            <td>
                                               
                                          
                                               <form action="/delete_service" method="GET"  id="sub_{{$service->id}}" enctype="multipart/form-data">
                                   
                                                   {{ csrf_field() }}
                               
                                                   <input type="hidden" name="service_id" value="{{$service->id}}">
                                      
                                                   <a  onclick="$('#sub_{{$service->id}}').submit();">

                                                    <button class="btn btn-sm btn-icon btn-pure btn-default on-default button-remove" data-toggle="tooltip" data-original-title="مسح">
                                                            <i class="icon-trash" aria-hidden="true"></i>
                                                    </button>  
                                                   </a>      
                           
                                               </form>
                                                
                                                
                                            </td> 
                                        </tr>
                                      @endforeach                                    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>            
        </div>       
    </div>
</div>



       



@endsection