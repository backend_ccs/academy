@extends('layouts.admin_const')

@section('content')

<div class="wrapper">
    
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12"style="float: right;margin-left: auto;">
                        <h2 style="font-size: xx-large;" > اضافة كوبون</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li style="float: right;margin-left: auto; "><a href="/show_allcoupons" class="btn btn-success"style="font-size: x-large; background-color: teal;"> الكوبونات العامة</a></li>&nbsp;&nbsp;
                            <li class="breadcrumb-item">اضافة كوبون </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            
             <div class="body">
                 <form id="basic-form" method="post"action="/addCoupons" novalidate enctype="multipart/form-data"  class="text-right">
                                           {{ csrf_field() }}

                        
                        <div class="form-group">
                            <label style="    font-size: x-large;">  أختر الخدمة</label>
                            <select class="form-control text-left " id="cproject_id" name="service_id" dir ="rtl" required>

                                <option value="0">جميع الدورات</option>

                                <?php 
                                     $get_service = \App\Service::select('id','name')->get();
                                ?>
                                @foreach($get_service as $service)
                                    <option value="{{$service->id}}">{{$service->name}}</option>
                                @endForeach
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label style="    font-size: x-large;"> أختر المسوق</label>
                            <select class="form-control text-left " id="cproject_id" name="affiliate_id" dir ="rtl" required>
                                <option >  أضف كوبون للمسوق او كوبون عام</option>

                                <option value="0">كوبون عام</option>

                                <?php 
                                     $get_affiliators = \App\affiliate::select('id','first_name','last_name')->get();
                                ?>
                                @foreach($get_affiliators as $affiliate)
                                    <option value="{{$affiliate->id}}">{{$affiliate->first_name}} {{$affiliate->last_name}}</option>
                                @endForeach
                            </select>
                        </div>
                        
                         <div class="form-group">
                            <label style="    font-size: x-large;">الكود </label>
                           
                            <input type="text" name="code"  class="form-control" required>
                        </div>
                        
                        
                          <div class="form-group">
                            <label style="    font-size: x-large;">% النسبه </label>
                           
                            <input type="number" name="value"  class="form-control" required>
                        </div>
                         
                        
                        <br>    
                        <button type="submit" name="Add_review"class="btn btn-primary" style="margin-right: 556px;font-size: 22px;">حفظ</button>
                          
                    </form>
             </div>
            
            
        </div>
</div>
</div>

@endsection