<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function () {
    return redirect('index');
});

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

Route::get('/our_backup_database', 'serviceController@our_backup_database')->name('our_backup_database');

Route::get("reservation/{id}", "reserveController@reservation");

// Route::post("paypals","reservePayPalController@payWithpaypal");
// Route::get("status","reservePayPalController@getPaymentStatus")->name('paypals.status');


Route::post("payProcess","payController@payProcess");

Route::post("reservePay","payController@reservePay");

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get("show_reservePage/{id}", "reserveController@show_reservePage");
Route::get("reserveService", "reserveController@reserveService");


Route::post("paypal","PaymentController@payWithpaypal");
Route::get("status","PaymentController@getPaymentStatus")->name('paypal.status');



Route::post("doctorContact","contactController@doctorContact");

Route::get("/index","contactController@show_home");

Route::get("show_services","contactController@show_services");

Route::post("sendmessage","contactController@sendmessage");

Route::post("add_msg","imageController@add_msg");
//Route::get('/','contactController@count_visit');


Route::get('/ajax_get_imgs','contactController@ajax_get_imgs');

Route::get('/id={affiliate}', "contactController@index");


Route::get("get_chartData","affiliateController@get_chartData");

Route::get("get_allregisterData","affiliateController@get_allregisterData");

Route::get("get_allpayedData","affiliateController@get_allpayedData");


Route::post("add_whatsappUser", "contactController@add_whatsappUser");

 //*****************************==> admin pannel:


Route::get("/dashboard", function () {
    return redirect('login');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'contactController@homeDate');

Route::post('/login','Auth\LoginController@postLogin');

// users::
Route::get("show_adminusers","userController@show_users");

Route::get("show_adminusers","userController@users_data");

Route::get("delete_user","userController@delete_user");

Route::get("edit_User","userController@show_editUser");

Route::get("edit_User","userController@userData");

Route::post('update_user',"userController@update_user");

Route::get("show_addUser","userController@show_addUser");

Route::post("add_user","userController@add_user");



// contact us::

Route::get("show_contactPage","contactController@show_contactPage");

Route::get("show_contactPage","contactController@contact_data");

Route::get("delete_contactus","contactController@delete_contactus");

Route::get('show_contactPage/excel','contactController@contact_excel')->name('show_contactPage.excel');

// doctor contact::

Route::get("show_doctorcontact","contactController@show_doctorcontact");

Route::get("show_doctorcontact","contactController@doctorcontact_data");

Route::get("delete_doctorcontact","contactController@delete_doctorcontact");

Route::get('show_doctorcontact/excel','contactController@excel')->name('show_doctorcontact.excel');

// servicess::

Route::get("show_services","serviceController@show_services");

Route::get("show_services","serviceController@service_data");


Route::get("edit_servicedata","serviceController@edit_servicedata");

Route::post("update_service","serviceController@update_service");

Route::get("delete_service","serviceController@delete_service");

Route::get("show_addservice","serviceController@show_addservice");

Route::post("add_service","serviceController@insert_service");


Route::get("show_images","serviceController@show_Page");

Route::get("show_images","serviceController@show_serviceImage");

Route::get("delete_image","serviceController@delete_image");

Route::get("show_addServiceImage","serviceController@show_addServiceImage");

Route::post("addServiceImage","serviceController@addServiceImages");

// about us::

Route::get("show_about","contactController@show_about");

Route::get("show_about","contactController@about_data");

Route::get("edit_about","contactController@show_editabout");
Route::get("edit_about","contactController@edit_about");

Route::post("update_about","contactController@update_about");

// call information ::

Route::get("show_call","contactController@show_call");

Route::get("show_call","contactController@call_Data");

Route::get("edit_call","contactController@edit_call");

Route::get("edit_call","contactController@edit_callData");

Route::post("update_call","contactController@upload_call");

//User Review  ::

Route::get('show_reivew',"imageController@show_reivewpage");

Route::get('show_reivew',"imageController@review_data");

Route::get("delete_reviews","imageController@delete_review");

Route::get('show_addreview','imageController@show_addreview');

Route::post("add_review","imageController@add_review");

// slider work   ::

Route::get('show_slider',"imageController@show_sliderpage");

Route::get('show_slider',"imageController@slider_data");

Route::get("delete_slider","imageController@deleteslider");

Route::get('show_addslider','imageController@show_addslider');

Route::post("add_slider","imageController@add_slider");

// web content  work   ::

Route::get('show_webcontent',"imageController@show_webcontentpage");

Route::get('show_webcontent',"imageController@webcontent_data");


Route::get("edit_webcontent","imageController@show_editwebcontent");

Route::get("edit_webcontent","imageController@edit_webcontentpage");

Route::post("update_content","imageController@update_content");


// show video::

Route::get("show_video","imageController@show_video");

Route::get("show_video","imageController@video_data");


Route::get("edit_video","imageController@show_editvideo");

Route::get("edit_video","imageController@editvideo_data");

Route::post("update_video","imageController@update_video");


// show show_sponser::



Route::get("show_sponser","imageController@show_sponser");

Route::get("show_sponser","imageController@sponser_data");


Route::get("delete_sponser","imageController@deletesponser");

Route::get('show_addsponser','imageController@show_addsponser');

Route::post("add_sponser","imageController@addsponser");

// change colors ::


Route::get("show_colorpage","imageController@show_color");

Route::get("show_colorpage","imageController@get_colors");

Route::post("update_color","imageController@update_color");


// social mediaa::


Route::get("show_social","imageController@show_social");

Route::get("show_social","imageController@social_data");


Route::get("edit_social","imageController@show_editSocail");

Route::get("edit_social","imageController@SocailData");

Route::post("update_social","imageController@update_social");




//affiliate users ::

Route::get("show_affiliators","affiliateController@show_affiliators");

Route::get("show_editAffiliator","affiliateController@show_editAffiliator");

Route::get("show_editAffiliator","affiliateController@show_AffiliatorData");

Route::post("update_affiliate","affiliateController@update_affiliate");

Route::get("show_addAffilitaor","affiliateController@show_addAffilitaor");

Route::post("add_affiliate","affiliateController@add_affiliate");

Route::get("delete_affiliator","affiliateController@delete_affiliator");






Route::get("update_state","affiliateController@update_state");

Route::get('show_affiliateContact/excel','affiliateController@affiliate_excel')->name('show_affiliateContact.excel');

Route::get("show_affiliatePage","affiliateController@show_affiliatePage");

Route::get("show_affiliatePage","affiliateController@affiliateData");

Route::get("update_reservationPage","reserveController@update_reservationPage");

// visits ::

Route::get('show_visits',"affiliateController@show_visits");

Route::get('show_visits',"affiliateController@show_visitsData");

Route::get('show_visitsData/excel','affiliateController@visits_excell')->name('visits_excell.excel');

Route::get("show_affiliateVists","affiliateController@show_affiliateVists");

Route::get("show_affiliateVists","affiliateController@show_affiliateVisits_Data");

Route::get('affiliators/excel','affiliateController@show_affiliators_excell')->name('affiliators.excel');


Route::get('affiliate_visits/excel','affiliateController@affiliate_visits_excell')->name('affiliate_visits.excel');

Route::get("follow_affiliate","affiliateController@showfollow_affiliate");

Route::get("follow_affiliate","affiliateController@follow_affiliate_data");

Route::get('follow/excel','affiliateController@follow_excell')->name('follow.excel');



// stages Data ::

Route::get("show_stages","serviceController@show_stages");

Route::get("show_stages","serviceController@stages_data");

Route::get('show_editStage',"serviceController@show_editStage");

Route::get('show_editStage',"serviceController@show_stageData");

Route::post('update_stage',"serviceController@update_stage");


// important documents ::

Route::get("show_docs" , "importDocController@show_docs");

Route::get("show_docs" , "importDocController@show_doc_data");

Route::get("show_edit_page","importDocController@show_edit_page");

Route::get("show_edit_page" , "importDocController@show_docData");

Route::post("update_doc","importDocController@update_doc");


// whats app users ::
Route::get("show_whatsUser_page", "contactController@show_whatsUser_page");

Route::get("show_whatsUser_page", "contactController@show_whatsUser_data");

Route::get('show_whatsContact/excel','contactController@whatsContact_excel')->name('show_whatsContact.excel');


Route::get("show_reservationPage","reserveController@show_reservationPage");

Route::get("show_reservationPage","reserveController@show_reservationData");

//*********************************affiliate coupons****************************/

Route::get("view_affiliateCoupon","couponController@view_affiliateCoupon");

Route::get("view_affiliateCoupon","couponController@show_all_affiliateCoupons");

Route::get("show_addCoupon","couponController@show_addCoupon");

Route::post("addCoupons","couponController@addCoupons");


Route::get("delete_coupon","couponController@delete_coupon");

Route::get("show_allcoupons","couponController@show_allcoupons");

Route::get("show_allcoupons","couponController@show_general_Coupons");



