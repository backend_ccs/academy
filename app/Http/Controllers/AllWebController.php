<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use carbon\carbon;
use Excel;

class AllWebController extends Controller{
    
    
    public $message = array();
    
    
    public function all_info(){
        try{
            
            $get_logoData = \App\video_link::select('id','link','logo','first_color','second_color')->where('id','1')->first();
                    
            $get_link = \App\video_link::where('id','1')->value('link');
            
            $social_media = \App\social_media::select('id','facebook','instagram','twitter','youtube','snapchat','Longitude','Latitude')->where('id' , '1')->first();
            
            $call_info = \App\call_info::select('id','address','phone','whatsapp','email')->where('id' , '1')->first();
            
            $web_content= \App\web_content::select('id', 'small_title','big_title','content','socialForm_title','service_title')->where('id', '1')->first();
            
            $about_us = \App\About::select('id', 'content')->where('id','1')->first();
            
            
            $message['link'] = $get_link;
            
            $message['data_1'] = $get_logoData;
            
            $message['data_2'] = $social_media;
            
            $message['data_3'] = $call_info;
            
            $message['data_4'] = $web_content;
            
            $message['data_5'] = $about_us;
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    public function send_message(Request $request){
        try{
            
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));

            $add_contact = new \App\contact_us;
            
            
            $add_contact->country = $request->input('country');
            $add_contact->email = $request->input('email');
            $add_contact->message = $request->input('message');
            $add_contact->created_at = $dateTime;
            $add_contact->updated_at = $dateTime;
            $add_contact->save();
            
            
            if($add_contact == true){
                $message['error'] = 0;
                $message['message'] = "the data is add successfully";
            }else{
                $message['error']= 1;
                $message['message'] = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    public function show_service(){
        try{
            
            $get_service = \App\Service::select('id','name','price')->get();
            
            
            if( count($get_service) >0){
                $message['data'] = $get_service;
                $message['error']  = 0;
                $message['message'] = "this is all the services";
            }else{
                $message['data'] = $get_service;
                $message['error'] = 1;
                $message['message'] = "there is no service, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    public function show_service_image(Request $request){
        try{
            
            $service_id = $request->input('service_id');
            
            $show_images = \App\image_services::select('id','image')->where('service_id',$service_id)->get();
            
            
            if(count($show_images)>0 ){
                $message['data'] = $show_images;
                $message['error'] = 0;
                $message['message'] = "this is all the images, please try again";
            }else{
                $message['data'] = $show_images;
                $message['error'] = 1;
                $message['message'] = "there is no data, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    public function show_serviceData(){
        try{
            $data = array();
            
            $get_data = \App\Service::select('id', 'name','description','price')->get();
            
            foreach($get_data as $each){
                
                $get_image = \App\image_services::select('id','image')->where('service_id',$each->id)->get();
                
                array_push($data, (object)array(
                    
                    "id" => $each->id,
                    "name" => $each->name,
                    "description"  => $each->description,
                    "price" => $each->price,
                    "images" => $get_image
                    
                    ));                
                
            }
            
            
            if( $data != NULL){
                $message['data'] = $data;
                $message['error'] = 0;
                $message['message'] = "this is all the services";
            }else{
                $message['data'] = $data;
                $message['error'] = 1;
                $message['message'] = "there is no data, please try again";
            }
            
            
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    public function show_sliders(){
        try{
            
            $show_review = \App\review_image::select('id', 'image')->get();
            
            $show_serve =  \App\service_image::select('id','image')->get();
            
            if( count($show_review)>0 ){
                $message['review'] = $show_review;
                $message['data']  = $show_serve;
                $message['error'] = 0;
                $message['message'] = "this is all the data";
            }else{
                $message['review'] = $show_review;
                $message['data']  = $show_serve;
                $message['error'] = 1;
                $message['message'] = "there is no data,please try again";
                
            }
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
     public function academy_contact(Request $request){
        try{
            $get_price = array();
            $g = 0;
            $total = 0;
            $all_service = $request->input('service');
            
           // $service = implode(" , ", $all_service);
            
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));

            $add_contact = new \App\doctor_contact;
            
            $add_contact->name = $request->input('name');
            $add_contact->phone = $request->input('phone');
            $add_contact->service_id = $all_service;
            $add_contact->affiliate_id = $request->input('affiliate_id');
            $add_contact->state = "wait";
            $add_contact->created_at = $dateTime;
            $add_contact->updated_at = $dateTime;
            $add_contact->save();
           
          /* 
            $get_service = \App\doctor_contact::where('id',  $add_contact->id)->value('service_id');
            
            
            
            foreach (explode(', ', $get_service) as $serve){
                array_push($get_price,  \App\Service::select('price')->where('name' , $serve )->first());

                $total += $get_price[$g]->price ;
                   
                $g++;
            }
            
            
            
            
           $update_contact = \App\doctor_contact::where([['id', $add_contact->id],['created_at',$dateTime]])
                                            ->update([
                                                'total_price' =>$total,
                                                'type' => 'cash',
                                            ]);
                                            
                                            
                                            */
    
            if($add_contact == true){
                $message['error'] = 0;
                $message['message'] = "the data is add successfully";
            }else{
                $message['error']= 1;
                $message['message'] = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    
    public function visits(Request $request){
        try{
            $affiliate_id = $request->input('affiliate_id');
                   
            $now_date = date("Y/m/d");

            if( $request->has('affiliate_id')){
                

                $get_affiliateVisit = DB::select("select number from affiliate_visits where date=? and affiliate_id=? ",[$now_date, $affiliate_id]);

                
                if( count($get_affiliateVisit)>0 ){
                    $affiliate_visit = $get_affiliateVisit[0]->number + 1;

                    $update_visit = DB::update("update  affiliate_visits set number=? where date=? and affiliate_id=?",[$affiliate_visit,$now_date , $affiliate_id]);

                }else{
                    $insert_affiliatevisit = DB::insert("insert into affiliate_visits (affiliate_id,number,date) values(?,?,?)",[$affiliate_id , '1',$now_date]);

                }
            }
            
            
            $get_num = \App\Visits::where('date' , $now_date)->value('number');

            if( count($get_num)>0 ){
                $visits = $get_num + 1;

               $get_sameDate = DB::update("update  visits set number=? where date=?",[$visits,$now_date]);

            }else{
               $insert_visit = DB::insert("insert into visits (number,date) values(?,?)",['1',$now_date]);

            }
            
            $message['error'] = 0;
            $message['message'] = "some one visits ur site";
        } catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    // dashboard:: **********************************************************************************************************
    
    
    public function todays_visit(){
        try{
            
            $now_date = date("Y/m/d");

            $get_visits = \App\Visits::where('date' , $now_date)->value('number');
            
            if( $get_visits != NULL){
                $message['data'] = $get_visits;
                $message['error'] = 0;
                $message['message'] = "this is the count of people that visits today";
            }else{
                $message['data'] = $get_visits;
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    public function show_socialContact(){
        try{
            
            $get_data = \App\social_media::select('id','facebook','instagram','twitter','youtube','snapchat','Longitude','Latitude')->first();
            
            
            if( $get_data  != NULL){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is social media contact";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no data, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    public function update_socailContacts(Request $request){
        try{
            
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));



            $update = \App\social_media::where('id' , '1')
                                       ->update([
                                           
                                           "facebook" => $request->input('facebook'),
                                           "instagram" => $request->input('instagram'),
                                           "twitter" => $request->input('twitter'),
                                           "youtube" => $request->input('youtube'),
                                           "snapchat" => $request->input('snapchat'),
                                           "Longitude" => $request->input('Longitude'),
                                           "Latitude" => $request->input('Latitude'),
                                           "updated_at" => $dateTime,
                                           ]);
            
            
            if( $update == true){
                $message['error'] = 0;
                $message['message'] = "Data is updated successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = " there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    public function count_visits(Request $request){
        try{
            
            $from_date = $request->input('from_date');
            $to_date = $request->input('to_date');
            
            $check = \App\Visits::whereBetween('date',[$from_date,$to_date])->sum('number');
            
            
            if( $check == true){
                $message['data'] = $check;
                $messagae['error'] = 0;
                $message['message'] = "this is the visits in this days";
            }else{
                $message['data'] = $check;
                $message['error']  =1;
                $message['message'] = "there is an error, please try again";
            }
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
     
    public function show_visitsDate_charts(){
        try{
            
            $all_Date = array();

            $dates = \App\Visits::select('date')->get();
            
            foreach ($dates as $eachDate){
                array_push($all_Date, $eachDate->date);
                           
            }
            $message['Date'] = $all_Date;
            
        }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
        }
        
         return response()->json($message);
    }
    
    
      public function show_visitsNumber_charts(){
        try{
            $data = array();
            $all_Num = array();
    
            $dates = \App\Visits::select('number')->get();
            
            foreach ($dates as $eachDate){
                
                array_push($data, $eachDate->number);
               
            }
            
             array_push($all_Num,  (object)array(
                                
                                "data" =>  $data
                                    ));
                        
            
            $message['all'] = $all_Num;
            
        }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
        }
        
         return response()->json($message);
    }
    
    
    
    public function show_registerDate_chart(){
        try{
              $all_Date = array();

            $dates = DB::select("SELECT COUNT(id) as number,date(created_at) as date FROM doctor_contact WHERE 1 GROUP BY DAY(created_at) ORDER BY created_at");
            
            foreach ($dates as $eachDate){
                array_push($all_Date, $eachDate->date);
                           
            }
            
            
            $message['Date'] = $all_Date;
            
            
        }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
        }
        
         return response()->json($message);
    }
    
    
    
      
    public function show_registerNumber_chart(){
        try{
            $data = array();
            $all_Num = array();
    
            $dates = DB::select("SELECT COUNT(id) as number,date(created_at) as date FROM doctor_contact WHERE 1 GROUP BY DAY(created_at) ORDER BY created_at");
            
            foreach ($dates as $eachDate){
                array_push($data , $eachDate->number);
                
              
    
            }
            
              array_push($all_Num, (object)array(
                    
                                "data" => $data
                                    ));
            
            
            $message['all'] =  $all_Num;
            
            
        }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
        }
        
         return response()->json($message);
    }
    
    
    
    
    
     public function show_payiedDate_chart(){
        try{
              $all_Date = array();

            $dates = DB::select("SELECT COUNT(id) as number,date(created_at) as date FROM doctor_contact WHERE state = 'done' GROUP BY DAY(created_at) ORDER BY created_at");
            
            foreach ($dates as $eachDate){
                array_push($all_Date, $eachDate->date);
                           
            }
            $message['Date'] =  $all_Date;
            
            
        }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
        }
        
         return response()->json($message);
    }
    
    
     public function show_payiedNumber_chart(){
        try{
            $data = array();
            $all_Num = array();
     
            $dates = DB::select("SELECT COUNT(id) as number,date(created_at) as date FROM doctor_contact WHERE state = 'done' GROUP BY DAY(created_at) ORDER BY created_at");
            
            foreach ($dates as $eachDate){
                
                array_push($data, $eachDate->number);
                
    
            }
            
            array_push($all_Num, (object)array(
                    
                            "data" => $data
                                ));
            
            $message['all'] =  $all_Num;
            
            
        }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
        }
        
         return response()->json($message);
    }
    
    
    
    public function  all_counts(){
        try{
            $visits = \App\Visits::select('id')->sum('number');
            
            $register = \App\doctor_contact::select('id')->count();
            
            $register_payed = \App\doctor_contact::select('id')->where('state', 'done')->count();
            
            
            $message['visits'] = $visits;
            
            $message['register'] = $register;
            
            $message['payied'] = $register_payed;
        }catch(Exception $ex){
               $message['error']=2;
               $message['message']='error'.$ex->getMessage();
        }
        
         return response()->json($message);
    }
    
    
    
}

   
   
   