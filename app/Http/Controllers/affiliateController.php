<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use carbon\carbon;
use Excel;
use Illuminate\Support\Facades\Session;

class affiliateController extends Controller{
    
    public $message = array();
    
    public function affiliate_login(Request $request){
        try{
            
            $email = $request->input('email');
            $password = $request->input('password');
            
            if( $request->has('email') && $request->has('password')){
                $check_login  = \App\affiliate::select('id', 'first_name','last_name','image','email','phone','password','card_no','stage','version_seen','reserve_page','created_at')
                                                ->where([['email',$email],['password', $password]])
                                                ->orwhere([['phone',$email],['password', $password]])->get();
                         
            }else{
                $message['data'] = [];
                $message['error'] = 1;
                $message['message'] = "error, please try again";
                return response()->json($message);
            }                   
                                            
            if( count($check_login)>0 ){
                $message['data'] = $check_login;
                $message['error'] = 0;
                $message['message'] = "welcome , you are logged in";
            }else{
                $message['data'] = $check_login;
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";    
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    
    public function affiliate_register(Request $request){
        try{
            
             $created_at = carbon::now()->toDateTimeString();
             $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));

            
            $image = $request->file('image');
             
             if(isset($image)){
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads/affiliates';
                    $image->move($destinationPath_id, $savedFileName);
        
                    $images = $savedFileName;
                }else{
                   
                     $images = NULL;
                }
                
                
                
            $add_affiliate = new \App\affiliate;
            
            $add_affiliate->first_name = $request->input('first_name');
            $add_affiliate->last_name = $request->input('last_name');
            $add_affiliate->image = $images;
            $add_affiliate->email = $request->input('email');
            $add_affiliate->phone = $request->input('phone');
            $add_affiliate->password = $request->input('password');
            $add_affiliate->card_no = $request->input('card_no');
            $add_affiliate->affiliate_id = $request->input('user_id');
            $add_affiliate->affiliate_present = 5;
            $add_affiliate->stage = 1;
            $add_affiliate->created_at = $dateTime;
            $add_affiliate->save();
              
            if( $add_affiliate == true ){
                $message['error'] = 0;
                $message['message'] = "A new user is inserted successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";    
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
     public function show_followUser_ByID(Request $request){
        try{
            $user_id = $request->input('user_id');
            
            $get_allUsers = \App\affiliate::select('id', 'first_name','last_name','image','email','phone','password','created_at')
                                            ->where('affiliate_id',$user_id )->get();
                                            
            if( count($get_allUsers)>0 ){
                $message['data'] = $get_allUsers;
                $message['error'] = 0;
                $message['message'] = "this is all the users that register by that user";
            }else{
                $message['data'] = $get_allUsers;
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";    
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    
    public function show_user_ById(Request $request){
        try{
            $user_id = $request->input('user_id');
            
            $get_allUsers = \App\affiliate::select('id', 'first_name','last_name','image','email','phone','password','card_no','version_seen','created_at')
                                            ->where('id',$user_id )->get();
                                            
            if( count($get_allUsers)>0 ){
                $message['data'] = $get_allUsers;
                $message['error'] = 0;
                $message['message'] = "this is all the users that register by that user";
            }else{
                $message['data'] = $get_allUsers;
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";    
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    public function update_profile(Request $request){
        try{
            
             $created_at = carbon::now()->toDateTimeString();
             $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));

            $affiliate_id = $request->input('user_id');
            
            
            $get_image = \App\affiliate::where('id', $affiliate_id)->value('image');
            
            $image = $request->file('image');
             
             if(isset($image)){
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads/affiliates';
                    $image->move($destinationPath_id, $savedFileName);
        
                    $images = $savedFileName;
                }else{
                   
                     $images = $get_image;
                }
                
                
                
            $update_affiliate =  \App\affiliate::where('id',$affiliate_id)
                                                 ->update([
                                                        'first_name' => $request->input('first_name'),
                                                        'last_name' => $request->input('last_name'),
                                                        'image' => $images,
                                                        'email' => $request->input('email'),
                                                        'phone' => $request->input('phone'),
                                                        'password' => $request->input('password'),
                                                        'card_no' => $request->input('card_no'),
                                                        'updated_at' => $dateTime,
                                                    ]);
            
           

            if( $update_affiliate == true ){
                $message['error'] = 0;
                $message['message'] = "this user is updated successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";    
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
  public function show_contacts_Byaffiliator(Request $request){
      try{
          
          $user_id = $request->input('user_id');
          
          $get_contact = \App\doctor_contact::select('id', 'name', 'phone','service_id','state', 'created_at')
                                    ->where('affiliate_id',$user_id)->get();
                                    
            
          if( count($get_contact)>0 ){
              $message['data'] = $get_contact;
              $message['error'] = 0;
              $message['message'] = "this is all the contacts by this affiliator";
          }else{
              $message['data'] = $get_contact;
              $message['error'] = 1;
              $message['message'] = "there is no cotacts , please try again";
          }  
          
      }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
  }  
  
     public function show_affiliateVisits(Request $request){
        try{
            
            $user_id = $request->input('user_id');
            
            $getVisits = \App\affiliate_visits::select('id','affiliate_id', 'number', 'date')
                                                ->where('affiliate_id' ,$user_id )->get();
            
            if( count($getVisits)>0 ){
                $message['data'] = $getVisits;
                $message['error'] = 0;
                $message['message'] = "this is all the visitis blong to that user";
            }else{
                $message['data'] = $getVisits;
                $message['error'] = 1;
                $message['message'] = "there is no visits for taht user";
            }
            
        } catch (Exception $ex) {
            $message['error']=2;
            $message['message']=" error('DataBase Error :{$ex->getMessage()}')";
          }
            return Response()->json($message);
    }
    
    
    
    // Dashboard ::
    
    
    
        public function show_affiliatePage(){
            return view('affiliate_page');
        }

        
        public function  affiliateData(Request $request){
            $id =  $request->input('user_id');
            
            setcookie('userAffiliate_id', $id);
            $get_data =\App\affiliate::select('id', 'first_name', 'last_name','last_name','image','email','phone','password','balance')
                                       ->where('id',$id)->get();
          
            
            $get_contact = \App\doctor_contact::select('id', 'name', 'phone','service_id','state','total_price','type', 'created_at')
                                                ->where('affiliate_id',$id)->get();
           
            return view('affiliate_page', array('get_data'=> $get_data ,
                                                'get_contact'=> $get_contact,
                                                ));
        }
        
        
    public function update_state(Request $request){
        
        $doctor_id = $request->input('doctor_id');
        
         $select_service = \App\doctor_contact::select('service_id','affiliate_id')->where('id' , $doctor_id)->get();

         $get_price = array();
        $g = 0; 
        $total=0;
        $affiliate_money=0;
        
        if( count($select_service) >0){
            $affiliateID = $select_service[0]->affiliate_id;
              
            if($affiliateID == NULL){
                
            }else{
                
                $get_level = \App\affiliate::where('id', $select_service[0]->affiliate_id)->value('stage');

            
            
            
            foreach($select_service as $serve){

                foreach (explode(', ', $serve->service_id) as $serv){

                    array_push($get_price, \App\Service::select('price', 'level1','level2', 'level3')
                                                          ->where('name' , $serv)->get());

                    $total += $get_price[$g][0]->price ;

                    if( $get_level == 1){
                        $calculate_present = $get_price[$g][0]->level1 ;    
                        $affiliate_money += $calculate_present ;

                    }else if($get_level == 2){
                        $calculate_present = $get_price[$g][0]->level2 ;    
                        $affiliate_money += $calculate_present ;

                    }else{
                        $calculate_present = $get_price[$g][0]->level3 ;    
                        $affiliate_money += $calculate_present ;

                    $g++;
                }
                }
            }
        

      
            
            $get_balance = \App\affiliate::where('id', $affiliateID)->value('balance');
            
            $all_balance = $get_balance + $affiliate_money;
            
             $update_affiliate = \App\affiliate::where('id',$affiliateID)->update(['balance'=>$all_balance]);
  
        }
        
        
        
        $update_doctorcontact = \App\doctor_contact::where('id',$doctor_id)->update(["state" => "done"]);
         
        if( $update_doctorcontact == 1){
            echo "<script>alert('تم تاكيد الطلب بنجاح');"
                    . "window.location.replace('/show_doctorcontact')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                    . "window.location.replace('/show_doctorcontact')"
                    . "</script>";
        }
        
       
    }
    }
     
    
    
   public function affiliate_excel(Request $request){

      $count = 0;
        $fromDate = $request->input('fromDate');
        $toDate = $request->input('toDate');

        if( $request->has('fromDate') && $request->has('toDate')) {
            
        
            $get_data = \App\doctor_contact::select('id','name','phone','address','nationality','service_id','state','total_price','type','created_at')
                                                ->where('affiliate_id', $_COOKIE['userAffiliate_id'])
                                                ->whereDate('created_at' ,'>=' , $fromDate)
                                                ->whereDate('created_at' ,'<=' , $toDate)->get();
            
            $doctor_array[] = array('id','name','phone','services','state','total_price','type','created_at');
            
            foreach ($get_data as $doctorss){
                $doctor_array[] = array(
                    'id' =>  $get_data[$count]->id,
                    'name' => $get_data[$count]->name,
                    'phone' => $get_data[$count]->phone,
                    'services'=>  $get_data[$count]->service_id,
                    'state' =>  $get_data[$count]->state,
                    'total_price' => $get_data[$count]->total_price,
                    'type' => $get_data[$count]->type,
                    'created_at' =>  $get_data[$count]->created_at,
                );
                $count++;
            }
            
            Excel::create('Affiliate contact data',function($excel) use($doctor_array){
                $excel->setTitle('Affiliate contact data');
                $excel->sheet('Affiliate contact data',function($sheet) use($doctor_array){
                    $sheet->fromArray($doctor_array , null, 'A1', false, false);
                });
            })->download('xls');
            
        }else{
            echo "<script>alert('من فضلك اختار التاريخ من و الى ');"
                              . "window.location.replace('/show_affiliatePage');"
                              . "</script>";
        }
    }

   
    public function show_affiliators(){
        
        $get_affiliators = \App\affiliate::select('id', 'first_name', 'last_name','image','email','phone','password','card_no','balance','affiliate_id','stage','version_seen','reserve_page','created_at')->get();
        
        return view('affiliators',['get_affiliators' => $get_affiliators]);
        
    }
    
    public function  delete_affiliator(Request $request){
        
        $affiliator_id = $request->input('affiliator_id');
        
        $deleteAffiliator = \App\affiliate::where('id', $affiliator_id)->delete();
        
        if( $deleteAffiliator == 1){
            echo "<script>alert('تم مسح هذا المسوق بنجاح');"
                    . "window.location.replace('/show_affiliators')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                    . "window.location.replace('/show_affiliators')"
                    . "</script>";
        }
    }
    
    
    public function show_editAffiliator(Request $request){
        
        
      return view('edit_affiliatorPage');

    }
    
    
    public function show_AffiliatorData(Request $request){
         $affiliator_id = $request->input('affiliate_id');
        
        $get_affiliator = \App\affiliate::select('id', 'first_name', 'last_name','image','email','phone','password','card_no','balance','affiliate_id','stage','version_seen','created_at')
                                            ->where('id',$affiliator_id)->get();
        
      return view('edit_affiliatorPage',['get_affiliator' => $get_affiliator]);
    }
    
    public function update_affiliate(Request $request){
               
        $affiliate_id = $request->input('affiliate_id');
        
        $get_image = \App\affiliate::where('id', $affiliate_id)->value('image');
        
        $image = $request->file('image');
        
          if(isset($image)){
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                            $destinationPath_id = 'uploads/affiliates/';
                $image->move($destinationPath_id, $savedFileName);

                $images = $savedFileName;
            }else{
                
                if($get_image >0){
                    $images = $get_image;
                }else{
                   $images = NULL;
                }
            }
    
      
        $created_at = carbon::now()->toDateTimeString();
        $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));

        
        $update = \App\affiliate::where('id', $affiliate_id)
                                    ->update([
                                        
                                        'first_name' => $request->input('first_name'),
                                        'last_name' => $request->input('last_name'),
                                        'image' => $images,
                                        'email' => $request->input('email'),
                                        'phone' => $request->input('phone'),
                                        'password' => $request->input('password'),
                                        'card_no' => $request->input('card_no'),
                                        'balance' => $request->input('balance'),
                                        'stage' => $request->input('stage'),
                                        'updated_at' => $dateTime,

                                    ]);
        
        if( $update == true){
            echo "<script>alert('تم تعديل هذا المسوق بنجاح');"
                    . "window.location.replace('/show_affiliators')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                    . "window.location.replace('/edit_affiliatorPage')"
                    . "</script>";
        }
    }
    
    
    public function show_addAffilitaor(){
        return view('add_affiliator');
    }
    
    
    public function add_affiliate(Request $request){
        
        $image = $request->file('image');
        
          if(isset($image)){
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                            $destinationPath_id = 'uploads/affiliates/';
                $image->move($destinationPath_id, $savedFileName);

                $images = $savedFileName;
            }else{
                   $images = NULL;
                
            }
    
      
        $created_at = carbon::now()->toDateTimeString();
        $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));

        $insert = new \App\affiliate;
        
        $insert->first_name = $request->input('first_name');
        $insert->last_name = $request->input('last_name');
        $insert->image = $images;
        $insert->email = $request->input('email');
        $insert->phone = $request->input('phone');
        $insert->password = $request->input('password');
        $insert->card_no = $request->input('card_no');
        $insert->stage = 1;
        $insert->created_at = $dateTime;
        $insert->save();
        
        if( $insert == true){
            echo "<script>alert('تم أضافة هذا المسوق بنجاح');"
                    . "window.location.replace('/show_affiliators')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                    . "window.location.replace('/show_addAffilitaor')"
                    . "</script>";
        }
    }
    
    
    public function show_visits(){
        return view('admin_visits');
    }
    
    public function show_visitsData(){
        
        $get_data = \App\Visits::select('id','number','date')->get();
        
        return view('admin_visits',['get_data' => $get_data]);
    }
    
    
    
    public function visits_excell(Request $request){
      $count = 0;
        $fromDate = $request->input('fromDate');
        $toDate = $request->input('toDate');
    
        if( $request->has('fromDate') && $request->has('toDate')){

            $get_data = \App\Visits::select('id','number','date')
                                  ->whereDate('date' ,'>=' , $fromDate)
                                  ->whereDate('date' ,'<=' , $toDate)->get();
    
            
            $all_visits[] = array('id','number','date');
            
            foreach ($get_data as $contactData){
                $all_visits[] = array(
                    'id' =>  $contactData->id,
                    'number' => $contactData->number,
                    'date' => $contactData->date,
                    
                );
                $count++;
            }
            
            Excel::create('Academy_visits_data',function($excel) use($all_visits){
                $excel->setTitle('Academy visits data');
                $excel->sheet('Academy visits data',function($sheet) use($all_visits){
                    $sheet->fromArray($all_visits , null, 'A1', false, false);
                });
            })->download('xls');
            
        }else{
            echo "<script>alert('من فضلك اختار التاريخ من و الى ');"
                              . "window.location.replace('/show_visits');"
                              . "</script>";
        }
    }

    
    public function show_affiliateVisit(){
        return view('affiliate_visits');
    }    

    public function show_affiliateVisits_Data(Request $request){
        
        $user_id = $request->input('user_id');
              
         session::put('id' , $user_id);
         
        $get_data = \App\affiliate_visits::select('id','number','date')->where('affiliate_id',$user_id)->get();
        
        return view('affiliate_visits',['get_data'  => $get_data]);
    }
    
   
     
    public function affiliate_visits_excell(Request $request){
      $count = 0;
        $fromDate = $request->input('fromDate');
        $toDate = $request->input('toDate');

        if( $request->has('fromDate') && $request->has('toDate')){
                
             $user_id =  session::get('id');

            $get_data = \App\affiliate_visits::select('id','number','date')->where('affiliate_id',$user_id)
                                  ->whereDate('date' ,'>=' , $fromDate)
                                  ->whereDate('date' ,'<=' , $toDate)->get();
    
            
            $all_visits[] = array('id','number','date');
            
            foreach ($get_data as $contactData){
                $all_visits[] = array(
                    'id' =>  $contactData->id,
                    'number' => $contactData->number,
                    'date' => $contactData->date,
                    
                );
                $count++;
            }
            
            Excel::create('Affiliate_visits_data',function($excel) use($all_visits){
                $excel->setTitle('Affiliate visits data');
                $excel->sheet('Affiliate visits data',function($sheet) use($all_visits){
                    $sheet->fromArray($all_visits , null, 'A1', false, false);
                });
            })->download('xls');
            
        }else{
            echo "<script>alert('من فضلك اختار التاريخ من و الى ');"
                              . "window.location.replace('/affiliate_visits');"
                              . "</script>";
        }
    }
    
  
   public function show_affiliators_excell(Request $request){

      $count = 0;
        $fromDate = $request->input('fromDate');
        $toDate = $request->input('toDate');

        if( $request->has('fromDate') && $request->has('toDate')){
        
            $get_data = \App\affiliate::select( 'affiliate.id', 'affiliate.first_name', 'affiliate.last_name', 'affiliate.image', 'affiliate.email', 'affiliate.phone', 'affiliate.password', 'affiliate_follow.first_name as follow_firstName', 'affiliate_follow.last_name as follow_lastName' , 'affiliate.balance', 'affiliate.affiliate_present', 'affiliate.created_at')
                                        ->leftjoin( 'affiliate as affiliate_follow' , 'affiliate.affiliate_id', '=', 'affiliate_follow.id')
                                        ->whereDate('affiliate.created_at' ,'>=' , $fromDate)
                                        ->whereDate('affiliate.created_at' ,'<=' , $toDate)->get();
                                        
                                        
            $affiliators[] = array('id', 'first_name', 'last_name', 'image', 'email', 'phone', 'password', 'affiliate_follow', 'balance', 'affiliate_present', 'created_at');
            
            foreach ($get_data as $affiliate){
                $affiliators[] = array(
                    'id' =>  $affiliate->id,
                    'first_name' => $affiliate->first_name,
                    'last_name' => $affiliate->last_name,
                    'image'=>  $affiliate->image,
                    'email' =>  $affiliate->email,
                    'phone' => $affiliate->phone,
                    'password' => $affiliate->password,
                    'affiliate_follow' => $affiliate->follow_firstName." ".$affiliate->follow_lastName,
                    'balance' => $affiliate->balance,
                    'affiliate_present' => $affiliate->affiliate_present,
                    'created_at' =>  $affiliate->created_at,
                );
                $count++;
            }
            
            Excel::create('All Affiliate  data',function($excel) use($affiliators){
                $excel->setTitle('All Affiliate data');
                $excel->sheet('All Affiliate data',function($sheet) use($affiliators){
                    $sheet->fromArray($affiliators , null, 'A1', false, false);
                });
            })->download('xls');
            
        }else{
            echo "<script>alert('من فضلك اختار التاريخ من و الى ');"
                              . "window.location.replace('/show_affiliators');"
                              . "</script>";
        }
    }



    
    public function showfollow_affiliate(){
        return view('show_followAffiliate');
    }
    
    public function follow_affiliate_data(Request $request){
        
        
        $follow_id = $request->input('follow_id');
        
        setcookie("follow_id", $follow_id);
        $get_following = \App\affiliate::select('id', 'first_name', 'last_name','image','email','phone','password','affiliate_id','created_at')
                                        ->where('affiliate_id', $follow_id)->get();
        
        return view('show_followAffiliate',['get_following' => $get_following]);
    }
    
    
      
   public function follow_excell(Request $request){

       $followID =  $_COOKIE['follow_id'];

      $count = 0;
      
      
        $fromDate = $request->input('fromDate');
        $toDate = $request->input('toDate');

        if( $request->has('fromDate') && $request->has('toDate')){
        
            $get_data = \App\affiliate::select( 'id', 'first_name', 'last_name','image','email','phone','card_no','affiliate_id','created_at')
                                        ->where('affiliate_id', $followID)
                                        ->whereDate('created_at' ,'>=' , $fromDate)
                                        ->whereDate('created_at' ,'<=' , $toDate)->get();
    
            $affiliators[] = array('id', 'first_name', 'last_name', 'image', 'email', 'phone', 'balance',  'created_at');
            
            foreach ($get_data as $affiliate){
                $affiliators[] = array(
                    'id' =>  $get_data[$count]->id,
                    'first_name' => $get_data[$count]->first_name,
                    'last_name' => $get_data[$count]->last_name,
                    'image'=>  $get_data[$count]->image,
                    'email' =>  $get_data[$count]->email,
                    'phone' => $get_data[$count]->phone,
                    'card_no' => $get_data[$count]->card_no,
                    'balance' => $get_data[$count]->balance,
                    'created_at' =>  $get_data[$count]->created_at,
                );
                $count++;
            }
            
            Excel::create('Follow Affiliate data',function($excel) use($affiliators){
                $excel->setTitle('Follow Affiliate data');
                $excel->sheet('Follow Affiliate data',function($sheet) use($affiliators){
                    $sheet->fromArray($affiliators , null, 'A1', false, false);
                });
            })->download('xls');
        }else{
            echo "<script>alert('من فضلك اختار التاريخ من و الى ');"
                         . "window.location.replace('/follow_affiliate');"
                        . "</script>";
        }
    }





 
    
    public function get_chartData(){
        $all_Date = array();
        $all_Num = array();

        $dates = \App\Visits::select('number','date')->get();
        
        foreach ($dates as $eachDate){
            array_push($all_Date, $eachDate->date);
                       
            array_push($all_Num, $eachDate->number);

        }
        $mac_num = max($all_Num);
        
        $max = round(( $mac_num + 10/2)/10)*10;
        
        $all = array(
                        "date" => $all_Date,
                        "number" =>$all_Num,
                        "max" => $max,
                    );
        return $all;
    }
    
    public function get_allregisterData(){
         $all_Date = array();
        $all_Num = array();

        $dates = DB::select("SELECT COUNT(id) as number,date(created_at) as date FROM doctor_contact WHERE 1 GROUP BY DAY(created_at) ORDER BY created_at");
        
        foreach ($dates as $eachDate){
            array_push($all_Date, $eachDate->date);
                       
            array_push($all_Num, $eachDate->number);

        }
        $mac_num = max($all_Num);
        
        $max = round(( $mac_num + 10/2)/10)*10;
        
        $all = array(
                        "Date" => $all_Date,
                        "number" =>$all_Num,
                        "max" => $max,
                    );
        return $all;
    
    }
    
    
    
    
    public function get_allpayedData(){
         $all_Date = array();
        $all_Num = array();

        $dates = DB::select("SELECT COUNT(id) as number,date(created_at) as date FROM doctor_contact WHERE state = 'done' GROUP BY DAY(created_at) ORDER BY created_at");
        
        foreach ($dates as $eachDate){
            array_push($all_Date, $eachDate->date);
                       
            array_push($all_Num, $eachDate->number);

        }
        $mac_num = max($all_Num);
        
        $max = round(( $mac_num + 10/2)/10)*10;
        
        $all = array(
                        "Date" => $all_Date,
                        "number" =>$all_Num,
                        "max" => $max,
                    );
        return $all;
    
    }
    
    
    
    
    // dashboard :::
    
    
    public function show_all_affilitors(){
        try{
            
            $get_data = \App\affiliate::select('affiliate.id','affiliate.first_name','affiliate.last_name','affiliate.image','affiliate.email','affiliate.phone','affiliate.card_no',
                                               'follow.id as follow_id', 'follow.first_name', 'follow.last_name', 'affiliate.balance', 'affiliate.stage', 'affiliate.version_seen')
                                     ->leftjoin('affiliate as follow' , 'affiliate.affiliate_id' ,'='  ,'follow.id')->get();
                                     
                                     
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is all the affiliators";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    public function show_affiliator_ByID(Request $request){
        try{
            
            $affiliate_id = $request->input('affiliate_id');
            
            
            $get_data = \App\affiliate::select('affiliate.id','affiliate.first_name','affiliate.last_name','affiliate.image','affiliate.email','affiliate.phone','affiliate.card_no',
                                               'affiliate.balance', 'affiliate.stage')
                                    ->where('id', $affiliate_id)->first();
                                               
                                               
               if( $get_data  != NULL){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is all the affiliators";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    public function update_affiliators(Request $request){
        try{
            
            $affiliate_id = $request->input('affiliate_id');
            
            $get_image = \App\affiliate::where('id',$affiliate_id)->value('image');
            
            $image = $request->file('image');
            
            if(isset($image)){
                $new_name = $image->getClientOriginalName();
                $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                            $destinationPath_id = 'uploads/affiliates/';
                $image->move($destinationPath_id, $savedFileName);

                $images = $savedFileName;
            }else{
                if( $get_image != NULL){
                    $images = $get_image;
                }else{
                   $images = NULL;
                }
            }
    
      
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));

            $update =  \App\affiliate::where('id', $affiliate_id)
                                     ->update([
                                         "first_name" => $request->input('first_name'),
                                         "last_name" => $request->input('last_name'),
                                         "image" => $images,
                                             "email" => $request->input('email'),
                                         "phone" => $request->input('phone'),
                                         "card_no" => $request->input('card_no'),
                                         "balance" => $request->input('balance'),
                                         "stage" => $request->input('stage'),
                                         "updated_at" => $dateTime,
                                         ]);
                                         
            if($update == true){
                $message['error'] = 0;
                $message['message'] = "Data is updated successfully";
            }else{
                $message['error'] = 1;
                $message['message']  = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    public function  insert_affiliators(Request $request){
        try{
            
            
             $image = $request->file('image');
        
              if(isset($image)){
                    $new_name = $image->getClientOriginalName();
                    $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                                $destinationPath_id = 'uploads/affiliates/';
                    $image->move($destinationPath_id, $savedFileName);
    
                    $images = $savedFileName;
                }else{
                       $images = NULL;
                    
                }
        
          
            $created_at = carbon::now()->toDateTimeString();
            $dateTime = date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));
    
            $insert = new \App\affiliate;
            
            $insert->first_name = $request->input('first_name');
            $insert->last_name = $request->input('last_name');
            $insert->image = $images;
            $insert->email = $request->input('email');
            $insert->phone = $request->input('phone');
            $insert->password = $request->input('password');
            $insert->card_no = $request->input('card_no');
            $insert->stage = 1;
            $insert->created_at = $dateTime;
            $insert->save();
            
            if( $insert == true){
                $message['error'] = 0;
                $message['message'] = "A new affiliators is add successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
            
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    public function deleted_affiliators(Request $request){
        try{
            
            $affiliate_id = $request->input('affiliate_id');
            
            $delete =  \App\affiliate::where('id', $affiliate_id)->delete();
            
            if( $delete == true){
                $message['error'] = 0;
                $message['message'] = "this affiliator is deleted successfully";
            }else{
                $message['error'] = 1;
                $message['message'] = "there is an error, please try again";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    public function show_affiliate_visits(Request $request){
        try{
            $affiliate_id = $request->input('affiliate_id');
            
            $show_visits = \App\affiliate_visits::select('id', 'number', 'date')
                                                ->where('affiliate_id' , $affiliate_id)->get();
                                                
            if( count($show_visits) >0 ){
                $message['data'] = $show_visits;
                $message['error'] = 0;
                $message['message'] = "this is all the days  visits for that affiliator";
            }else{
                $message['data'] = $show_visits;
                $message['error'] = 1;
                $message['message'] = "there is no visits for that affiliator";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    public function show_affiliate_follow(Request $request){
        try{
            
            $affiliate_id = $request->input('affiliate_id');
            
            $show_follow = \App\affiliate::select('affiliate.id','affiliate.first_name','affiliate.last_name','affiliate.image','affiliate.email','affiliate.phone','affiliate.card_no','affiliate.balance')
                                        ->where('affiliate_id' ,  $affiliate_id)->get();
                                        
                                        
            if( count($show_follow) > 0){
                $message['data'] = $show_follow;
                $message['error'] = 0;
                $message['message'] = "this is all the affiliators that follow that affiliator";
            }else{
                $message['data'] = $show_follow;
                $message['error'] = 1;
                $message['message'] = "there is no affiliators follow this affiliator";
            }
            
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
    
    
    
    
}