<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use carbon\carbon;
use Excel;
use Illuminate\Support\Facades\Session;

class newController extends Controller{
    
    public $message = array();
   
   
   public function show_AcademyContacts(Request $request){
       try{
        


           $get_data = \App\doctor_contact::select( 'doctor_contact.id','doctor_contact.name','doctor_contact.phone','doctor_contact.service_id as service_name', 'doctor_contact.affiliate_id', 'affiliate.first_name as affiliate_firstName','affiliate.last_name as affiliate_lastName', 'doctor_contact.state','doctor_contact.total_price','doctor_contact.type','doctor_contact.created_at' )
                                        ->leftjoin('affiliate' , 'doctor_contact.affiliate_id' ,'=' ,'affiliate.id')->get();
                                        
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is all the academy contacts";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no academy contacts";
            }
       }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
   }
   
   
   public function show_AcademyContacts_fromID(Request $request){
       try{
        

            $from_id = $request->input('id');
            
            
            $count = \App\doctor_contact::orderBy('id' ,'DESC')->value('id');

            $get_data = \App\doctor_contact::select( 'doctor_contact.id','doctor_contact.name','doctor_contact.phone','doctor_contact.service_id as service_name', 'doctor_contact.affiliate_id', 'affiliate.first_name as affiliate_firstName','affiliate.last_name as affiliate_lastName', 'doctor_contact.state','doctor_contact.total_price','doctor_contact.type','doctor_contact.created_at' )
                                        ->leftjoin('affiliate' , 'doctor_contact.affiliate_id' ,'=' ,'affiliate.id')
                                        ->whereBetween('doctor_contact.id' , array($from_id,$count))->get();
                                        
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is all the academy contacts";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no academy contacts";
            }
       }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
   }
   
   
    public function show_AcademyContacts_byDate(Request $request){
       try{
           
           $date_from = $request->input('date_from');
           
           $date_to = $request->input('date_to');


           $get_data = \App\doctor_contact::select( 'doctor_contact.id','doctor_contact.name','doctor_contact.phone','doctor_contact.service_id as service_name', 'doctor_contact.affiliate_id', 'affiliate.first_name as affiliate_firstName','affiliate.last_name as affiliate_lastName', 'doctor_contact.state','doctor_contact.total_price','doctor_contact.type','doctor_contact.created_at' )
                                        ->leftjoin('affiliate' , 'doctor_contact.affiliate_id' ,'=' ,'affiliate.id')
                                        ->whereBetween('doctor_contact.created_at', [$date_from,$date_to ])->get();
                                        
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is all the academy contacts";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no academy contacts in this date";
            }
       }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
   }
   
   
    public function show_whatscontact(Request $request){
       try{

             

           $get_data = \App\Whatsapp_user::select( 'whatsapp_user.id', 'whatsapp_user.name', 'whatsapp_user.affiliate_id', 'affiliate.first_name as affiliate_firstName', 'affiliate.last_name as affiliate_lastName', 'whatsapp_user.created_at' )
                                        ->leftjoin('affiliate' , 'whatsapp_user.affiliate_id' ,'=' ,'affiliate.id')->get();
                                        
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is all the whats app contacts";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no whats app contacts";
            }
       }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
   }
   
    public function show_whatscontact_fromID(Request $request){
       try{
           
            $from_id = $request->input('id');
            
            $count = \App\Whatsapp_user::orderBy('id' ,'DESC')->value('id');

       

           $get_data = \App\Whatsapp_user::select( 'whatsapp_user.id', 'whatsapp_user.name', 'whatsapp_user.affiliate_id', 'affiliate.first_name as affiliate_firstName', 'affiliate.last_name as affiliate_lastName', 'whatsapp_user.created_at' )
                                        ->leftjoin('affiliate' , 'whatsapp_user.affiliate_id' ,'=' ,'affiliate.id')
                                        ->whereBetween('whatsapp_user.id' , array($from_id,$count))->get();
                                        
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is all the whats app contacts";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no whats app contacts";
            }
       }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
   }
   
   
   
    public function show_contactUs(Request $request){
       try{
           
           $get_data = \App\contact_us::select( 'id','country','email','message','created_at' )->get();
                                        
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is all contacts us";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no contacts";
            }
       }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
   }
   
    public function show_contactUs_fromID(Request $request){
       try{
           
            $from_id = $request->input('id');
            
            $count = \App\contact_us::orderBy('id' ,'DESC')->value('id');

           
           $get_data = \App\contact_us::select( 'id','country','email','message','created_at' )
                                   ->whereBetween('id' , array($from_id,$count))->get();
                                        
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is all contacts us";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no contacts";
            }
       }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
   }
   
   
    public function show_affiliate_data(Request $request){
       try{
           
           $get_data = \App\affiliate::select( 'affiliate.id','affiliate.first_name as affiliate_firstName' ,'affiliate.last_name','affiliate.image','affiliate.email','affiliate.phone','affiliate.password','affiliate.card_no','affiliate.balance', 
                                               'affiliate.stage', 'affiliate.version_seen' ,'affiliate.affiliate_id','follow.first_name as follow_firstname', 'follow.last_name as follow_lastname' ,'affiliate.created_at' )
                                     ->leftjoin( 'affiliate as follow' , 'affiliate.affiliate_id'  ,'=' ,'follow.id')->get();
                                        
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is all affiliate data";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no affiliates";
            }
       }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
   }
   
    public function show_affiliate_withFollowData(Request $request){
       try{
           
           $allData = array();
           
           $get_data = \App\affiliate::select( 'affiliate.id','affiliate.first_name as affiliate_firstName' ,'affiliate.last_name','affiliate.image','affiliate.email','affiliate.phone','affiliate.password','affiliate.card_no','affiliate.balance', 
                                               'affiliate.stage', 'affiliate.version_seen', 'affiliate.affiliate_id' ,'affiliate.created_at as date' )->whereNull('affiliate_id')->get();
                                               
           
            foreach($get_data  as $affiliate ){
                
                $Follow_data = \App\affiliate::select( 'affiliate.id as follow_id','affiliate.first_name as follow_firstName' ,'affiliate.last_name as follow_lastname','affiliate.image as follow_image','affiliate.email as follow_email','affiliate.phone as follow_phone','affiliate.card_no as follow_cardno','affiliate.balance as follow_balance', 
                                               'affiliate.stage as follow_stage', 'affiliate.version_seen' ,'affiliate.created_at as follow_created_at' )->where('affiliate_id' , $affiliate->id)->get();
                                               
                array_push( $allData , (object)array(
                    
                    "id" => $affiliate->id,
                    "first_name" => $affiliate->affiliate_firstName,
                    "last_name" => $affiliate->last_name,
                    "image" => $affiliate->image,
                    "email" => $affiliate->email,
                    "phone" => $affiliate->phone,
                    "card_no" => $affiliate->card_no,
                    "balance" => $affiliate->balance,
                    "stage" => $affiliate->stage,
                    "version_seen" => $affiliate->version_seen,
                    "created_at" => $affiliate->date,
                    "Follow_affiliate" => $Follow_data,
                    
                    ));
                    
           
            }
                                        
            if( count($allData)>0 ){
                $message['data'] = $allData;
                $message['error'] = 0;
                $message['message'] = "this is all affiliate data";
            }else{
                $message['data'] = $allData;
                $message['error'] = 1;
                $message['message'] = "there is no affiliates";
            }
       }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
   }
   
    
    public function show_affiliate_ByID(Request $request){
        try{
            $affiliate_id = $request->input('affiliate_id');
            
            $get_data = \App\affiliate::select('id', 'first_name','last_name','image','email','phone','card_no','balance','stage','version_seen','created_at')
                                     ->where('id' ,$affiliate_id )->first();
            
            
            if( $get_data != NULL ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is the data of that affiliate";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no data for that user";
            }
        }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
    }
   
   
    public function show_service_data(Request $request){
       try{
           
            $data = array();
           
            $get_data = \App\Service::select(  'id','name','description','price','level1','level2','level3','level3','created_at as create_date','updated_at as update_date')->get();
                        
            
            foreach($get_data as $service ){
                
                $get_images = \App\image_services::select('id', 'image')->where('service_id' , $service->id)->get();
                
                array_push($data , (object)array(
                    
                    "id" => $service->id,
                    "name" => $service->name,
                    "description" => $service->description,
                    "price" => $service->price,
                    "level1" => $service->level1,
                    "level2" => $service->level2,
                    "level3" => $service->level3,
                    "created_at" => $service->create_date,
                    "updated_at" => $service->update_date,
                    "images" => $get_images,

                    ));
                
            }            
                            
            if( count($data)>0 ){
                $message['data'] = $data;
                $message['error'] = 0;
                $message['message'] = "this is all service data";
            }else{
                $message['data'] = $data;
                $message['error'] = 1;
                $message['message'] = "there is no services";
            }
       }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
   }
   
   
    public function show_academy_visits(Request $request){
       try{
           
           $get_data = \App\Visits::select( 'id','number','date')->get();
                                        
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is all academy visits";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no data, please try again";
            }
       }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
   }
   
   
   
    public function show_academy_visits_byDate(Request $request){
       try{
           
           
           $date_from = $request->input('date_from');
           
           $date_to = $request->input('date_to');

           $get_data = \App\Visits::select( 'id','number','date')       
                                ->whereBetween('date', [$date_from,$date_to ])->get();

                                        
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is  academy visits by specific date";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no data, please try again";
            }
       }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
   }
    

   public function show_affiliate_visits_ByDate(Request $request){
       try{
           
           $affiliate_id = $request->input('affiliate_id');
           
           $date_from = $request->input('date_from');
           
           $date_to = $request->input('date_to');
           
           $get_data = \App\affiliate_visits::select( 'id','number','date')
                                            ->where('affiliate_id' , $affiliate_id)
                                            ->whereBetween('date', [$date_from,$date_to ])->get();
                                        
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is all affiliate visits by special date";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no data, please try again";
            }
       }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
   }  
   
   
   
   
   public function show_affiliate_visits(Request $request){
       try{
           
           $affiliate_id = $request->input('affiliate_id');
           
         
           
           $get_data = \App\affiliate_visits::select( 'id','number','date')
                                            ->where('affiliate_id' , $affiliate_id)->get();
                                        
            if( count($get_data)>0 ){
                $message['data'] = $get_data;
                $message['error'] = 0;
                $message['message'] = "this is all visits of affiliate ";
            }else{
                $message['data'] = $get_data;
                $message['error'] = 1;
                $message['message'] = "there is no data, please try again";
            }
       }catch(Exception $ex){
            $message['error']=2;
            $message['message']="error('DataBase Error :{$ex->getMessage()}')";
         }

      return response()->json($message);
   }  
   
   
   
    
   
}
?>