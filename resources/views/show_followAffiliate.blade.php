@extends('layouts.admin_const')

@section('content')



<div id="wrapper">

    

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                       <!--<h2>Jquery Datatable</h2>-->
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item">   المسوقين التابعين </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                             
                            <ul class="nav nav-tabs-new">
                                 <form id="basic-form" method="get"action="{{ route('follow.excel')}}" novalidate enctype="multipart/form-data"  class="text-right">
                                           {{ csrf_field() }}

                                            <div class="form-group " style="float: right; margin-left: 40px;">
                                                <label style="    font-size: x-large; margin-right: -60px;">من </label>
                                                <input type="date" name='fromDate' class="form-control text-right" style="margin-top: -30px;" required>
                                            
                                                <label style="    font-size: x-large; margin-right: -60px;">الى </label>
                                                <input type="date" name='toDate' class="form-control text-right" style="margin-top: -30px;" required>
                                            </div> 
                
                                    <li style="float: right;margin-left: auto;">
                                    <button class="btn btn-outline-success " style="margin-top: 30px;"> Export to Excel</button></li>


                                </form>
                                <li style="float: right;margin-left: auto;"><h2 style="font-size: xx-large;">: تقارير المسوقين التابعين له </h2> </li> &nbsp;&nbsp;
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover js-basic-example dataTable table-custom text-center">
                                    
                                       <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>الاسم الاول </th>
                                            <th>الاسم الثانى </th>
                                            <th>الصورة</th>
                                            <th>البريد الالكترونى</th>
                                            <th>رقم الهاتف</th>
                                            <th>الرصيد</th>
                                            <th>تاريخ</th>

                                            
                                            <!--<th>Edit</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($get_following as $follow)
                                        <tr>
                                            <td>{{$follow->id}}</td>
                                            <td>{{$follow->first_name}}</td>
                                            <td>{{$follow->last_name}}</td>
                                            <td>{{$follow->image}}</td>
                                            <td>{{$follow->email}}</td>
                                            <td>{{$follow->phone}}</td>
                                            <td>{{$follow->balance}}</td>
                                            <td>{{$follow->created_at}}</td>
                                            
                                        </tr>
                                      @endforeach                                    

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>            
        </div>       
    </div>
</div>

         



@endsection