@extends('layouts.admin_const')

@section('content')

<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12" style="float: right;margin-left: auto;">
                        <h2 style="font-size: xx-large;"> :تعديل محتوى الموقع </h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item"> محتوى الموقع</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            
            
             <div class="body">
                 <form id="basic-form" method="post"action="/update_content" novalidate enctype="multipart/form-data" class="text-right">
                                           {{ csrf_field() }}

                        @foreach($get_data as $web)
                        
                        <input type="hidden" name="content_id" value="{{$web->id}}">

                        <div class="form-group">
                            <label style="    font-size: x-large;">العنوان الصغير </label>
                            <input type="text" name='small_title' value="{{$web->small_title}}" class="form-control text-right" required>
                        </div>
                        
                       
                        
                        <div class="form-group">
                            <label style="    font-size: x-large;">العنوان الكبير</label>
                            <input type="text" name='big_title' value="{{$web->big_title}}" class="form-control text-right" required>
                        </div>
                        
                         <div class="form-group">
                            <label style="    font-size: x-large;">الفقره </label>
                            <input type="text" name='content' value="{{$web->content}}" class="form-control text-right" required>
                        </div>
                        
                          <div class="form-group">
                            <label style="    font-size: x-large;">عنوان فورمه التواصل </label>
                            <input type="text" name='socialForm_title' value="{{$web->socialForm_title}}" class="form-control text-right" required>
                        </div>
                         <div class="form-group">
                            <label style="    font-size: x-large;">عنوان الخدمات </label>
                            <input type="text" name='service_title' value="{{$web->service_title}}" class="form-control text-right" required>
                        </div>
                        
                  
                        
                        
                        
                          
                        <br>    
                        <button type="submit" name="edit_category"class="btn btn-primary" style="margin-right: 556px;font-size: 22px;">حفظ</button>
                          @endforeach
                    </form>
             </div>
            
            
        </div>
</div>

@endsection