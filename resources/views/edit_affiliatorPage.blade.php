@extends('layouts.admin_const')

@section('content')

<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12" style="float: right;margin-left: auto;">
                        <h2 style="font-size: xx-large;"> :تعديل بيانات المسوق </h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">تعديل بيانات المسوق</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            
            
             <div class="body">
                 <form id="basic-form" method="post"action="/update_affiliate" novalidate enctype="multipart/form-data" class="text-right">
                                           {{ csrf_field() }}

                        @foreach($get_affiliator as $data)
                        
                        <input type="hidden" name="affiliate_id" value="{{$data->id}}">

                        <div class="form-group">
                            <label style="    font-size: x-large;">الاسم الاول </label>
                            <input type="text" name='first_name' value="{{$data->first_name}}" class="form-control text-right" required>
                        </div>
                        
                       
                        
                        <div class="form-group">
                            <label style="    font-size: x-large;">الاسم الاخير </label>
                            <input type="text" name='last_name' value="{{$data->last_name}}" class="form-control text-right" required>
                        </div>
                        
                         <div class="form-group">
                            <label style="    font-size: x-large;">الصورة</label><br>
                           <?php
                           $image = $data->image;
                                                
                                if( $image == ''){

                                }else{?>
                            &nbsp;  &nbsp;<img src="uploads/affiliates/{{$data->image}}" width="100" height="80" style="margin-right: 90px;"><br><br><br>
                                <?php } ?>
                            <input type="file" name="image"  class="form-control text-right" required>
                        </div>
                        
                  
                           <div class="form-group">
                            <label style="    font-size: x-large;">البريد الالكترونى </label>
                            <input type="text" name='email' value="{{$data->email}}" class="form-control text-right" required>
                        </div>
                        
                           <div class="form-group">
                            <label style="    font-size: x-large;">رقم الهاتف </label>
                            <input type="text" name='phone' value="{{$data->phone}}" class="form-control text-right" required>
                        </div>
                        
                        
                        <div class="form-group">
                            <label style="    font-size: x-large;">كلمه السر </label>
                            <input type="text" name='password' value="{{$data->password}}" class="form-control text-right" required>
                        </div>
                        
                          <div class="form-group">
                            <label style="    font-size: x-large;">رقم الهوية</label>
                            <input type="text" name='card_no' value="{{$data->card_no}}" class="form-control text-right" required>
                        </div>
                        
                        
                        <div class="form-group">
                            <label style="    font-size: x-large;">الرصيد </label>
                            <input type="text" name='balance' value="{{$data->balance}}" class="form-control text-right" required>
                        </div>
                        
                         <?php
                            $get_stages = \App\stage::where('id',$data->stage)->value('name');
                            
                            $get_all = \App\stage::all();
                         ?>
                      
                        <div class="form-group col-lg-12 ">
                            <label style="    font-size: x-large; float: right; margin-left: 20px;" >مرحلة </label> &nbsp; &nbsp; &nbsp; &nbsp;
                            <select name= "stage" value="{{$get_stages}}" class="form-control-sm " >
                                <option style="display: none;" value="{{$data->stage}}">{{$get_stages}}</option>
                                
                               @foreach($get_all as $each)
                                <option value=" {{$each->id}}">{{$each->name}}</option>
                               @endforeach 
                                
                            </select>
                        </div>
                        
                        
                          
                        <br>    
                        <button type="submit" name="edit_category"class="btn btn-primary" style="margin-right: 556px;font-size: 22px;">حفظ</button>
                        <br><br>
                          @endforeach
                    </form>
             </div>
            
            
        </div>
</div>

@endsection