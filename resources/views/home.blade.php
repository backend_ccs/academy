@extends('layouts.admin_const')

@section('content')
        
    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">
                        <h2>Dashboard</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item active">Dashboard</li>
                           
                            
                        </ul>
                    </div>
                    
                    <br><br>
                    
                       
                      <?php
          
                 
                      
                         $now_date = date("Y/m/d");

                      
                        $get_visitor = DB::select("select SUM(number) as number  from visits");
                        
                        $get_visitor_Day = \App\Visits::where('date',$now_date)->value('number');
                        
                        $get_countRegister = \App\doctor_contact::select('id')->count();
                        
                       $get_countpayed = \App\doctor_contact::select('id')->where('state','done')->count();


                        //$get_visitor_Day = DB::select("SELECT number as number FROM `visits` where date=?",[$now_date]);

                    ?>
              <!--     <div class="col-lg-4 col-12"> 
                        <div class="card border-0">
                            <div class="card-content" style=" background-color:  antiquewhite">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="media-body" style=" text-align: center">
                                    <p class="text-muted" style="font-size: xx-large; "><i class="fa fa-users"></i>  &nbsp; &nbsp;عدد الزيارات</p>
                                         <br>
                                        <h1 class="display-4">{{$get_visitor[0]->number}}</h1>
                                    </div>
                            </div>
                                                </div>

                    </div>  
                </div>
                    </div>
                      
                    
                    -->
<!--                        <div class="col-lg-3 col-12"> </div>-->
        <!--<form action="{{ route('our_backup_database') }}" method="get">-->
        <!--    <button style="submit" class="btn btn-primary"> download</button>-->
        <!--</form>-->
             <div class="col-lg-4 col-12"> 
                        <div class="card border-0">
                            <div class="card-content" style=" background-color:  antiquewhite">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="media-body" style=" text-align: center">
                                    <p class="text-muted" style="font-size: xx-large; "><i class="fa fa-users"></i>  &nbsp; &nbsp;عدد الزيارات اليوم</p>
                                        <br>
                                        <h1 class="display-4">{{$get_visitor_Day}}</h1>
                                    </div>
<!--                                <div class="align-self-center">
                                    <i class="icon-graph font-large-2 blue-grey lighten-3"></i>
                                </div>-->
                            </div>
                                              
                            </div>

                    </div>  
                </div>
                    </div>

                  <div class="col-lg-2 col-12"> </div>
                    
                <div class="col-lg-5 col-12"> 
                        <div class="card border-0">
                            <div class="card-content" style=" background-color:  antiquewhite">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="media-body" style=" text-align: center">
                                    <p class="text-muted" style="font-size: xx-large; "><i class="fa fa-users"></i>  &nbsp; &nbsp;عدد الزيارات بالتاريخ</p>
                            
                                      <form action="/home" method="get"   enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <lable style="float: right;     font-size: x-large;" >:من</lable><br>
                                            <input type="date" name="fromDate" style="    padding-right: 80px;" required><br><br>
                                            <lable style="float: right;     font-size: x-large;" >:الى</lable>
                                            <input type="date" name="toDate" style="    padding-right: 80px;" required>

                                        <br>
                                        <a >
                                            <button class="btn btn-sm btn-icon btn-pure btn-default on-default m-r-5 button-edit" data-toggle="tooltip" data-original-title="بحث" style='background-color: antiquewhite;'>
                                            <i class="fa fa-search fa-2x" aria-hidden="true"></i>
                                            </button>                                      
                                        </a>
                                    </form>
                                    
                                    

                                     
                                        <h1 class="display-4">{{$get_visitDay}}</h1>
                                    

                                    </div>
<!--                                <div class="align-self-center">
                                    <i class="icon-graph font-large-2 blue-grey lighten-3"></i>
                                </div>-->
                          </div>
                        </div>

                    </div>  
                </div>
                    </div>
                </div>
                
                <div class="row">
                   <div class="col-lg-12 col-md-9">
                    <div class="card">
                        <div class="header">
                            <p class="text-muted" style="font-size: xx-large; "><i class="fa fa-users"></i>  &nbsp; &nbsp;عدد الزيارات</p>
                               <br>
                            <h1 class="display-4">{{$get_visitor[0]->number}}</h1>
                                    
                        </div>
                        <div class="body">
                                <canvas id="lineChart" style="height: 400px;"></canvas>
                        </div>
                    </div>

                </div>
                </div>
                
                <div class="row">
                   <div class="col-lg-12 col-md-9">
                    <div class="card">
                        <div class="header">
                            <p class="text-muted" style="font-size: xx-large; "><i class="fa fa-users"></i>  &nbsp; &nbsp;عدد المهتمين </p>
                               <br>
                            <h1 class="display-4">{{$get_countRegister}}</h1>
                                    
                        </div>
                        <div class="body">
                                <canvas id="registerChart" style="height: 400px;"></canvas>
                        </div>
                    </div>

                </div>
                </div>
                
                 <div class="row">
                   <div class="col-lg-12 col-md-9">
                    <div class="card">
                        <div class="header">
                            <p class="text-muted" style="font-size: xx-large; "><i class="fa fa-users"></i>  &nbsp; &nbsp;عدد المسحلين </p>
                               <br>
                            <h1 class="display-4">{{$get_countpayed}}</h1>
                                    
                        </div>
                        <div class="body">
                                <canvas id="payed_num" style="height: 400px;"></canvas>
                        </div>
                    </div>

                </div>
                </div>
            </div>      
        </div>
    </div>

@endsection
