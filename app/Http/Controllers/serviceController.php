<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use carbon\carbon;
class serviceController extends Controller
{
  
    public function show_services(){
        return view('admin_service');
    }
    public function  service_data(){
        $get_servicedata = DB::select("select *  from services");
        
        return view('admin_service',['get_servicedata'=>$get_servicedata]);
    }
    
    public function delete_service(Request $request){
        
        $service_id = $request->input('service_id');
        
        $delete_service = DB::delete("delete from services where id=?",[$service_id]);
        
        if($delete_service == 1){
            echo "<script>alert('تم مسح الخدمة بنجاح');"
                    . "window.location.replace('/show_services')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ,  حاول مره اخرى');"
                    . "window.location.replace('/show_services')"
                    . "</script>";
        }
    }
    
    
   
    
    public function  edit_servicedata(Request $request){
        
        $service_id = $request->input('service_id');
        
        $get_servicedata = DB::select("select * from services where id=? ",[$service_id]);
        
        return view('admin_editservice',['get_servicedata'=>$get_servicedata]);
    }
    
    public function update_service(REquest $request){
        
        $service_id = $request->input('service_id');
        
        $get_image = DB::select("select *  from services where id=?",[$service_id]);
        
        $name = $request->input('name');
        $description = $request->input('description');
        $image = $request->file("image");
        $price = $request->input("price");
        $service_level1 = $request->input("service_level1");
        $service_level2 = $request->input("service_level2");
        $service_level3 = $request->input("service_level3");

         if(isset($image)){
            $new_name = $image->getClientOriginalName();
            $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads';
            $image->move($destinationPath_id, $savedFileName);

            $images = $savedFileName;
        }else{
           
             $images = $get_image[0]->image;
        }
        
        
        
        $update_service = DB::update("update services set name=? , description=?, image=? , price=? , level1= ?, level2 =?, level3 =? where id=? ",
                                [$name, $description, $images, $price, $service_level1, $service_level2, $service_level3, $service_id]);
        
        if($update_service == 1){
            echo "<script>alert('تم تعديل الخدمة بنجاح');"
                    . "window.location.replace('/show_services')"
                    . "</script>";
        }else{
            echo "<script>alert('لم يتم تغير البيانات');"
                    . "window.location.replace('/show_services')"
                    . "</script>";
        }
    }
    
    public function  show_addservice(){
        return view('show_addservice');
    }
    
    public function insert_service(Request $request){
        
        $service_name  = $request->input('service_name');
        $service_description = $request->input('service_description');
        $service_image = $request->file('service_image');
        $service_price = $request->input('service_price');
        $level1 = $request->input('service_level1');
        $level2 = $request->input('service_level2');
        $level3 = $request->input('service_level3');

        
        
        if( isset($service_name) && isset($service_description) && isset($service_image) && isset($service_price)){
        
        $created_at = carbon::now()->toDateTimeString();

        $dateTime= date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));

        if(isset($service_image)){
            $new_name = $service_image->getClientOriginalName();
            $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads';
            $service_image->move($destinationPath_id, $savedFileName);

            $images = $savedFileName;
        }else{
           
             $images = NULL;
        }
        
        
       $add_service = DB::insert("insert into services (name,description,image,price, level1, level2, level3,  created_at) values (?,?,?,?,?,?,?,?)",
                                [$service_name,$service_description,NULL,$service_price,$level1, $level2, $level3,$dateTime]);
        
        $get_service_id = DB::select("select id from services where name=? and description =? and created_at=?",[$service_name,$service_description,$dateTime]);
        
        $add_image = DB::insert("Insert into image_services (image,service_id,created_at) values (?,?,?)",[$images,$get_service_id[0]->id,$dateTime]);
     
       if( $add_service == 1){
            echo "<script>alert('تم أضافة خدمة جديدة بناح');"
                    . "window.location.replace('/show_services')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ,  حاول مره اخرى');"
                    . "window.location.replace('/show_addservice')"
                    . "</script>";
        }  
    
    }else{
        echo "<script>alert('برجاء ادخال البيانات كاملة');"
                    . "window.location.replace('/show_addservice')"
                    . "</script>";
    }
    }
    
    
    
    
    public function show_Page(){
        return redirect('showService_image');
    }
    public function show_serviceImage(Request $request){
       
        $service_id = $request->input('service_id');
        
        $show_allImages = DB::select("select * from image_services where service_id=?",[$service_id]);
        
        return view('showService_image',['show_allImages' => $show_allImages]);
    }
    
    public function delete_image(Request $request){
        
        $image_id = $request->input('image_id');
        
        $delete_image = DB::delete("delete from image_services where id=?",[$image_id]);
        
         if( $delete_image == 1){
            echo "<script>alert('تم مسح صورة  الخدمة بنجاح');"
                    . "window.location.replace('/show_services')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ,  حاول مره اخرى');"
                    . "window.location.replace('/addService_image')"
                    . "</script>";
        }  
    }
    
    public function show_addServiceImage(){
        return view('addService_image');
    }
    
    public function addServiceImages(Request $request){
       
       $service_id = $request->input('service_id');

        $image = $request->file('service_image');
        
        
        $created_at = carbon::now()->toDateTimeString();

        $dateTime= date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));

        if(isset($image)){
            $new_name = $image->getClientOriginalName();
            $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads';
            $image->move($destinationPath_id, $savedFileName);

            $images = $savedFileName;
        }else{
           
             $images = NULL;
        }
        
        $add_image = DB::insert("Insert into image_services (image,service_id,created_at) values (?,?,?)",[$images,$service_id,$dateTime]);

        if($add_image == 1){
            echo "<script>alert('تم اضافة صور الخدمة بنجاح');"
                    . "window.location.replace('/show_services')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ,  حاول مره اخرى');"
                    . "window.location.replace('/show_addServiceImage')"
                    . "</script>";
        }
    }
    
    
    
    //admin_stages ::
    
    public function show_stages(){
        return view('admin_stage');
    }
    
    public function stages_data(){
        
        $get_data = \App\stage::select('id','name','from_num','to_num','created_at')->get();
        
        return view('admin_stage',['get_data' => $get_data]);
    }
    
    public function show_editStage(){
        return view('edit_stage');
    }
    
    public function show_stageData(Request $request){
        
        $stage_id = $request->input('stage_id');
        
        $get_data = \App\stage::select('id','name','from_num','to_num')->where('id',$stage_id)->get();
        
        return view('edit_stage',['get_data' => $get_data]);
    }
    
    public function update_stage(Request $request){
        
        $created_at = carbon::now()->toDateTimeString();
        $dateTime= date('Y-m-d H:i:s',strtotime('+3 hours',strtotime($created_at)));

        $update = \App\stage::where('id',$request->input('stage_id'))
                            ->update([
                                'name' => $request->input('name'),
                                'from_num' => $request->input('from_num'),
                                'to_num' =>$request->input('to_num'),
                                'updated_at' => $dateTime
                            ]);
        
     if($update == 1){
            echo "<script>alert('تم تعديل المرحلة بنجاح');"
                    . "window.location.replace('/show_stages')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ,  حاول مره اخرى');"
                    . "window.location.replace('/show_editStage')"
                    . "</script>";
        }    
    }
    
    
    
    
    /************************************************************************************************/
    
    public function our_backup_database(){

        //ENTER THE RELEVANT INFO BELOW
        $mysqlHostName      = env('localhost');
        $mysqlUserName      = env('mariamAcademy');
        $mysqlPassword      = env('m123@academy');
        $DbName             = env('trainAcademy');
        $backup_name        = "academyDB.sql";
        $tables             = array("about_us","affiliate","affiliate_service","affiliate_visits","call_info","contact_us","doctor_contact","image_services"); //here your tables...

        $connect = new \PDO("mysql:host=$mysqlHostName;dbname=$DbName;charset=utf8", "$mysqlUserName", "$mysqlPassword",array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
        $get_all_table_query = "SHOW TABLES";
        $statement = $connect->prepare($get_all_table_query);
        $statement->execute();
        $result = $statement->fetchAll();


        $output = '';
        foreach($tables as $table)
        {
         $show_table_query = "SHOW CREATE TABLE " . $table . "";
         $statement = $connect->prepare($show_table_query);
         $statement->execute();
         $show_table_result = $statement->fetchAll();

         foreach($show_table_result as $show_table_row)
         {
          $output .= "\n\n" . $show_table_row["Create Table"] . ";\n\n";
         }
         $select_query = "SELECT * FROM " . $table . "";
         $statement = $connect->prepare($select_query);
         $statement->execute();
         $total_row = $statement->rowCount();

         for($count=0; $count<$total_row; $count++)
         {
          $single_result = $statement->fetch(\PDO::FETCH_ASSOC);
          $table_column_array = array_keys($single_result);
          $table_value_array = array_values($single_result);
          $output .= "\nINSERT INTO $table (";
          $output .= "" . implode(", ", $table_column_array) . ") VALUES (";
          $output .= "'" . implode("','", $table_value_array) . "');\n";
         }
        }
        $file_name = 'database_backup_on_' . date('y-m-d') . '.sql';
        $file_handle = fopen($file_name, 'w+');
        fwrite($file_handle, $output);
        fclose($file_handle);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($file_name));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
           header('Pragma: public');
           header('Content-Length: ' . filesize($file_name));
           ob_clean();
           flush();
           readfile($file_name);
           unlink($file_name);


}
}