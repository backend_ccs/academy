@extends('layouts.admin_const')

@section('content')

<div class="wrapper">
    
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12"style="float: right;margin-left: auto;">
                        <h2 style="font-size: xx-large;" >:أضافة صورة للخدمة</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">اضافة صورة  للخدمة </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            
             <div class="body">
                 <form id="basic-form" method="post"action="/addServiceImage" novalidate enctype="multipart/form-data"  class="text-right">
                                           {{ csrf_field() }}
                          
                        <div class="form-group">
                                <select class="form-control text-left " id="cproject_id" name="service_id" dir ="rtl" required>

                                        <?php 
                                        $get_service = \App\Service::select('id','name')->get();
                                        ?>
                                        @foreach($get_service as $service)
                                          <option value="{{$service->id}}">{{$service->name}}</option>
                                        @endForeach
                                    </select>
                                           
                            </div><br>
                        <div class="form-group">
                            <label style="    font-size: x-large;"> صورة الخدمة</label>
                           <input type="file" name="service_image"  class="form-control text-right" required>

                        </div>
                        
                        
                            
                        <br>    
                        <button type="submit" name="add_image"class="btn btn-primary" style="margin-right: 556px;font-size: 22px;">حفظ</button>
                          
                    </form>
             </div>
            
            
        </div>
</div>
</div>

@endsection