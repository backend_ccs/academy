@extends('layouts.admin_const')

@section('content')

<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12" style="float: right;margin-left: auto;">
                        <h2 style="font-size: xx-large;"> : تعديل المرحلة </h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">تعديل المرحلة </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            
            
             <div class="body">
                 <form id="basic-form" method="post"action="/update_stage" novalidate enctype="multipart/form-data" class="text-right">
                                           {{ csrf_field() }}

                        @foreach($get_data as $each_stage)
                        
                        <input type="hidden" name="stage_id" value="{{$each_stage->id}}">

                        <div class="form-group">
                            <label style="    font-size: x-large;">اسم المرحلة</label>
                            <input type="text" name='name' value="{{$each_stage->name}}" class="form-control text-right" required>
                        </div>
                        
                       
                        
                        <div class="form-group">
                            <label style="    font-size: x-large;">من </label>
                            <input type="text" name='from_num' class="form-control text-right" required value="{{$each_stage->from_num}} " />
                        </div>
                        
                        
                  
                         <div class="form-group">
                            <label style="    font-size: x-large;">الى </label>
                            <input type="text" name='to_num' value="{{$each_stage->to_num}}" class="form-control text-right" required>
                        </div>
                                            
                        
                        
                          
                        <br>    
                        <button type="submit" name="edit_stages"class="btn btn-primary" style="margin-right: 556px;font-size: 22px;">حفظ</button>
                          @endforeach
                    </form>
             </div>
            
            
        </div>
</div>

@endsection