<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use carbon\carbon;
class userController extends Controller
{
    
    public function show_users(){
        return view('admin_users');
    }
    
    public function  users_data(){
        $get_users = DB::select("select * from users");
        return view('admin_users',['get_users'=>$get_users]);
    }
    
    public function delete_user(Request $request){
        $user_id = $request->input('user_id');
        $delete_user = DB::delete("delete from users where id=?",[$user_id]);
        
        if($delete_user == 1){
            echo "<script>alert('this user  is deleted successfully');"
            . "window.location.replace('/dashboard');</script>";
        }else{
             echo "<script>alert('يوجد خطأ حاول مرة اخرى ');"
            . "window.location.replace('/admin_users');</script>";
        }
    }
    
     public function show_editUser(){
        
        return view('admin_editUser');
    }
    
    public function userData(Request $request){
        
        $user_id = $request->input('user_id');
        
        $user_data = DB::select("select * from users where id=?",[$user_id]);
        
        return view('admin_editUser',['user_data' => $user_data]);
    }
    
    
    public function update_user(Request $request){
        
        $user_id = $request->input('user_id');
        
        $get_image = DB::select("select * from users where id=?",[$user_id]);
        
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $image = $request->file('image');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $password = $request->input('password');
        $status = $request->input('status');
        
        
         if(isset($image)){
            $new_name = $image->getClientOriginalName();
            $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads';
            $image->move($destinationPath_id, $savedFileName);

            $images = $savedFileName;
        }else{
           
             $images = $get_image[0]->image;
        }
        
        $updateUserData = DB::update('update users set first_name=?, last_name=?, image=?, email=?, phone=?, password=? ,status=? where id=?',
                                                [$first_name,$last_name,$images,$email,$phone, $password, $status ,$user_id]);
        
        
        if( $updateUserData == 1){
            echo "<script>alert('تم تغير بيانات المستخدم  بنجاح ');"
                    . "window.location.replace('/show_adminusers')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                    . "window.location.replace('/edit_User')"
                    . "</script>"; 
        }
    }
    
    
    public function show_addUser(){
        return view("admin_addUser");
    }
        
        
    public function add_user(Request $request){
        
         
        
        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $image = $request->file('image');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $password = $request->input('password');
        $status = $request->input('status');
        
        
         if(isset($image)){
            $new_name = $image->getClientOriginalName();
            $savedFileName = rand(100000,999999).time()."_".$new_name; // give a unique name to file to be saved
                        $destinationPath_id = 'uploads';
            $image->move($destinationPath_id, $savedFileName);

            $images = $savedFileName;
        }else{
           
             $images = NULL;
        }
        
        $insert_users = DB::insert("insert into users (first_name,last_name,image,email,phone,password,status) values (?,?,?,?,?,?,?)",
                                                [$first_name,$last_name,$images,$email,$phone,$password,$status]);        
        
        
         if( $insert_users == 1){
            echo "<script>alert('تم أضافة المستخدم  بنجاح ');"
                    . "window.location.replace('/show_adminusers')"
                    . "</script>";
        }else{
            echo "<script>alert('يوجد خطأ, حاول مره اخرى ');"
                    . "window.location.replace('/show_addUser')"
                    . "</script>"; 
        }
        
    }
}