@extends('layouts.admin_const')

@section('content')

<div class="wrapper">
    
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12"style="float: right;margin-left: auto;">
                        <h2 style="font-size: xx-large;" >:أضافة خدمة</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="/home"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">اضافة خدمة </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            
             <div class="body">
                 <form id="basic-form" method="post"action="/add_service" novalidate enctype="multipart/form-data"  class="text-right">
                                           {{ csrf_field() }}

                        

                        <div class="form-group">
                            <label style="    font-size: x-large;">اسم الخدمة</label>
                            <input type="text" name='service_name' class="form-control text-right" required>
                        </div>
                                           
                        <div class="form-group">
                            <label style="    font-size: x-large;"> تفاصيل الخدمة</label>
                            <input type="text" name='service_description' class="form-control text-right" required>
                        </div>
                                           
                        <div class="form-group">
                            <label style="    font-size: x-large;"> صورة الخدمة</label>
                           <input type="file" name="service_image"  class="form-control text-right" required>

                        </div>
                        
                           
                        <div class="form-group">
                            <label style="    font-size: x-large;"> سعر الخدمة</label>
                            <input type="text" name="service_price"  class="form-control text-right" required>

                        </div>
                                           
                                   
                        <div class="form-group">
                            <label style="    font-size: x-large;"> المرحلة الاولى </label>
                            <input type="text" name="service_level1"  class="form-control text-right" required>

                        </div>
                                        
                        <div class="form-group">
                            <label style="    font-size: x-large;"> المرحلة الثانية</label>
                            <input type="text" name="service_level2"  class="form-control text-right" required>

                        </div>
                                           
                        <div class="form-group">
                            <label style="    font-size: x-large;"> المرحلة الثالثة</label>
                            <input type="text" name="service_level3"  class="form-control text-right" required>

                        </div>

                            
                        <br>    
                        <button type="submit" name="add_service"class="btn btn-primary" style="margin-right: 556px;font-size: 22px;">حفظ</button>
                          
                    </form>
             </div>
            
            
        </div>
</div>
</div>

@endsection